#include <ros/ros.h>

// action for FollowJointTrajectoryAction
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <actionlib/client/simple_action_client.h>

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_kinematics_parser_node");
	ros::NodeHandle n;
	ROS_INFO("**********************************");
	ROS_INFO("Control ABB arm node running.");
	ROS_INFO("**********************************");
		
	actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> fol_jnt_traj_action_client("/joint_trajectory_action", true);
	while(!fol_jnt_traj_action_client.waitForServer(ros::Duration(5.0)))
        ROS_ERROR("Waiting for the joint_trajectory_action server on hardware");
    sleep(1.0);
	
	std::vector<double> ik_cfg(6, 0.0);

	// (flat)
	// ik_cfg[0] = 0.0; ik_cfg[1] = 1.16; ik_cfg[2] = -0.71; ik_cfg[3] = 0.0; ik_cfg[4] = -2.02; ik_cfg[5] = 0.0;
	
	/* 
		X-rotations
	 */
	// (rotate around x for -10 degs)
	// ik_cfg[0] = 0.0; ik_cfg[1] = 1.05; ik_cfg[2] = -0.51; ik_cfg[3] = 0.0; ik_cfg[4] = -1.94; ik_cfg[5] = 0.0;

	// (rotate around x for -20 degs)
	// ik_cfg[0] = 0.0; ik_cfg[1] = 0.95; ik_cfg[2] = -0.35; ik_cfg[3] = 0.0; ik_cfg[4] = -1.82; ik_cfg[5] = 0.0;

	// (rotate around x for -40 degs)
	// ik_cfg[0] = 0.0; ik_cfg[1] = 0.77; ik_cfg[2] = -0.12; ik_cfg[3] = 0.0; ik_cfg[4] = -1.52; ik_cfg[5] = 0.0;

	// (rotate around x for -90 degs) [0.50, 0.00, 0.45, -0.00, 0.00, -0.00, 1.00]
	// ik_cfg[0] = 0.0; ik_cfg[1] = 0.49; ik_cfg[2] = -0.01; ik_cfg[3] = 0.0; ik_cfg[4] = -0.48; ik_cfg[5] = 0.0;

	// x = 10 
	// ik_cfg[0] = 0.0; ik_cfg[1] = 1.33; ik_cfg[2] = -1.02; ik_cfg[3] = 0.0; ik_cfg[4] = -2.05; ik_cfg[5] = 0.0;

	/* 
		Y-rotations
	 */
	// (rotate around y for -20 degs) 
	// ik_cfg[0] = -0.08; ik_cfg[1] = 1.16; ik_cfg[2] = -0.73; ik_cfg[3] = -0.38; ik_cfg[4] = -2.00; ik_cfg[5] = -0.09;

	// (rotate around y for -10 degs) 
	// ik_cfg[0] = -0.04; ik_cfg[1] = 1.16; ik_cfg[2] = -0.71; ik_cfg[3] = -0.19; ik_cfg[4] = -2.02; ik_cfg[5] = -0.05;

	// (rotate around y for 5 degs) [0.55, 0.00, 0.40, 0.03, -0.71, -0.03, 0.71]
	// ik_cfg[0] = 0.02; ik_cfg[1] = 1.16; ik_cfg[2] = -0.71; ik_cfg[3] = 0.1; ik_cfg[4] = -2.02; ik_cfg[5] = 0.02;

	// (rotate around y for 10 degs) 
	// ik_cfg[0] = 0.04; ik_cfg[1] = 1.16; ik_cfg[2] = -0.71; ik_cfg[3] = 0.19; ik_cfg[4] = -2.02; ik_cfg[5] = 0.04;

	// (rotate around y for 20 degs) [0.55, 0.02, 0.40, 0.12, -0.70, -0.12, 0.70]
	// ik_cfg[0] = 0.08; ik_cfg[1] = 1.16; ik_cfg[2] = -0.73; ik_cfg[3] = 0.38; ik_cfg[4] = -2.0; ik_cfg[5] = 0.08;

	/* 
		XY-rotations
	 */
	// x = -10 and y = 10
	// ik_cfg[0] = 0.04; ik_cfg[1] = 1.05; ik_cfg[2] = -0.51; ik_cfg[3] = 0.19; ik_cfg[4] = -1.93; ik_cfg[5] = 0.03;

	// x = -10 and y = -10
	// ik_cfg[0] = -0.04; ik_cfg[1] = 1.05; ik_cfg[2] = -0.51; ik_cfg[3] = -0.19; ik_cfg[4] = -1.93; ik_cfg[5] = -0.03;

	// x = 10 and y = 10
	ik_cfg[0] = 0.04; ik_cfg[1] = 1.33; ik_cfg[2] = -1.02; ik_cfg[3] = 0.19; ik_cfg[4] = -2.05; ik_cfg[5] = 0.05;

	// x = 10 and y = -10
	// ik_cfg[0] = -0.04; ik_cfg[1] = 1.33; ik_cfg[2] = -1.02; ik_cfg[3] = -0.19; ik_cfg[4] = -2.05; ik_cfg[5] = -0.05;


	std::vector<double> arm_jnt_vels(6, 0.5);

	trajectory_msgs::JointTrajectory trajectory;
	std::vector<std::string> jnt_names{"joint_1", "joint_2", "joint_3", "joint_4", "joint_5", "joint_6"};
	trajectory.joint_names = jnt_names;
	trajectory_msgs::JointTrajectoryPoint point;
	point.positions = ik_cfg;
	point.velocities = arm_jnt_vels;
	point.time_from_start = ros::Duration(5.0);
	trajectory.points.push_back(point);

	control_msgs::FollowJointTrajectoryGoal traj_goal;
    traj_goal.trajectory = trajectory;
    fol_jnt_traj_action_client.sendGoal(traj_goal);

	ros::waitForShutdown();
	return 0;
}

