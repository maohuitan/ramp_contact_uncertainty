#include <ros/ros.h>
#include "ramp_setting/ramp_setting.h"
#include "ramp/ik.h"

rampParameters ramp_params;
rampSetting* kine_parser;
bool ikQuery(ramp::ik::Request  &req,
         	 ramp::ik::Response &res)
{
	armCfg ref_cfg(6, 0.0);
	ref_cfg[0] = 0.0; ref_cfg[1] = 1.16; ref_cfg[2] = -0.71; ref_cfg[3] = 0.0; ref_cfg[4] = -2.02; ref_cfg[5] = 0.0;
	double min_cfg_dist = 1e6;
	armCfg min_dist_cfg;

	ROS_INFO("Ik of pose received");
	rampUtil::printGeoMsgPose(req.pose);
	std::vector<armCfg> tmps;
	if (kine_parser->getAllIKJointConfigurations(tmps, req.pose))
	{
		// ROS_INFO("ABB IK sols are:");
		for(const auto& t : tmps)
		{
			// rampUtil::printArmCfg(t);
			double this_dist = rampUtil::getJntCfgDistance(ref_cfg, t);
			if ( this_dist < min_cfg_dist )
			{
				min_cfg_dist = this_dist;
				min_dist_cfg = t;
			}
		}
		res.arm_cfg.data = min_dist_cfg;
		return true;
	}
	else
		return false;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_kinematics_parser_node");
	ros::NodeHandle n;
	ROS_INFO("**********************************");
	ROS_INFO("RAMP-Kinematics Parser node running.");
	ROS_INFO("**********************************");
		
	std::string node_space = "/abbArm/";
	std::vector<std::string> jnt_names{"joint_1", "joint_2", "joint_3", "joint_4", "joint_5", "joint_6"};
	std::string jnt_group_name = "manipulator";
	std::string eef_link = "tool0";
	kine_parser = new rampSetting(node_space, jnt_names, jnt_group_name, eef_link);
	kine_parser->printJointBoundsInfo();

	ros::ServiceServer ik_srv = n.advertiseService("ik_query", ikQuery);
	ROS_INFO("Ready to take ik query.");
  	ros::AsyncSpinner spinner(4);
	spinner.start();

	ros::waitForShutdown();
	delete kine_parser;
	return 0;
}

