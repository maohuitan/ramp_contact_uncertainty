#include "ramp_setting/ramp_setting.h"

rampSetting::rampSetting(const std::string node_space)
:robot_model_loader(node_space + "robot_description")
{
	joint_names = ramp_params.arm_joint_names;
	joint_group_name = ramp_params.arm_joint_group_name;
	eef_link_name = ramp_params.arm_eef_link_name;

	loadRobotModel();
	setJointBoundsMap();
}

rampSetting::rampSetting(const std::string node_space, const std::vector<std::string>& jnt_names, 
						const std::string& jnt_group_name, const std::string& eef_link)
: robot_model_loader(node_space + "robot_description"), joint_names(jnt_names), joint_group_name(jnt_group_name), eef_link_name(eef_link)
{
	loadRobotModel();
	setJointBoundsMap();
}

void rampSetting::loadRobotModel()
{
	kinematic_model = robot_model_loader.getModel();
	kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));
	joint_model_group = kinematic_model->getJointModelGroup(joint_group_name);
	kine_metrics_ptr = boost::make_shared<kinematics_metrics::KinematicsMetrics>(kinematic_model);
}

void rampSetting::setDefaultJointConfiguration()
{
	kinematic_state->setToDefaultValues();
}

void rampSetting::setRandomJointConfiguration()
{
	kinematic_state->setToRandomPositions();
}

void rampSetting::setJointConfiguration(const armCfg &target_cfg)
{
	kinematic_state->setJointGroupPositions(joint_model_group, target_cfg);
	kinematic_state->update();
}

void rampSetting::getDefaultJointConfiguration(armCfg &joint_values)
{
	setDefaultJointConfiguration();
	getCurrentJointConfiguration(joint_values);
}

void rampSetting::getCurrentJointConfiguration(armCfg &joint_values)
{
	kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
}

void rampSetting::getEefPose(geometry_msgs::Pose &eef_pose)
{
	const Eigen::Affine3d &eef_tf = kinematic_state->getGlobalLinkTransform(eef_link_name);
	// convert eigen::affine3d to geometry_msgs::Pose
	rampUtil::convertAffine3dtoQuaternion(eef_tf, eef_pose);
}

void rampSetting::getEefPose(Eigen::Affine3d &eef_pose)
{
	eef_pose = kinematic_state->getGlobalLinkTransform(eef_link_name);
}

bool rampSetting::getIKJointConfiguration(armCfg &joint_values, const geometry_msgs::Pose &eef_pose)
{
	const kinematics::KinematicsBaseConstPtr& solver = joint_model_group->getSolverInstance();
	armCfg cur_cfg;
	getCurrentJointConfiguration(cur_cfg);
	moveit_msgs::MoveItErrorCodes error;
	if ( solver->getPositionIK(eef_pose, cur_cfg, joint_values, error) )
		return true;
	else 
		return false;
}

bool rampSetting::getAllIKJointConfigurations(std::vector<armCfg> &vec_joint_values, const geometry_msgs::Pose &eef_pose)
{
	const kinematics::KinematicsBaseConstPtr& solver = joint_model_group->getSolverInstance();
	int nd = 0;
	getNumberOfDofs(nd);
	armCfg dummy_seed_cfg(nd, 0.0);
	std::vector<geometry_msgs::Pose> ik_poses;
	ik_poses.push_back(eef_pose);
	kinematics::KinematicsResult result;
	vec_joint_values.clear();
	if ( solver->getPositionIK(ik_poses, dummy_seed_cfg, vec_joint_values, result,kinematics::KinematicsQueryOptions()) )
	{
		#ifdef DEBUG
		for (int i=0; i<vec_joint_values.size(); ++i)
		{
			std::cout << "IK solutions are " << i << std::endl;
			std::cout << "[ ";
			for (int j=0; j<vec_joint_values.at(i).size(); ++j)
			{
				std::cout << vec_joint_values.at(i).at(j) << " ";
			}
			std::cout << "]" << std::endl;
		}
		#endif
		return true;
	}
	else 
		return false;
}

bool rampSetting::getAllIKJointConfigurationsIncludingLastJointTurns(std::vector<armCfg> &vec_joint_values, const geometry_msgs::Pose &eef_pose)
{
	const kinematics::KinematicsBaseConstPtr& solver = joint_model_group->getSolverInstance();
	int nd = 0;
	getNumberOfDofs(nd);
	armCfg dummy_seed_cfg(nd, 0.0);
	std::vector<geometry_msgs::Pose> ik_poses;
	ik_poses.push_back(eef_pose);
	kinematics::KinematicsResult result;
	vec_joint_values.clear();
	if ( solver->getPositionIK(ik_poses, dummy_seed_cfg, vec_joint_values, result,kinematics::KinematicsQueryOptions()) )
	{
		/* 
			Augment solutions with last joint turns included
			This part is currently coded for 6-joint robot
			Turn solutions: theta_6 = theta_6 +/- 360
		*/
		std::vector<armCfg> turn_solutions;
		for (int i=0; i<vec_joint_values.size(); ++i)
		{
			/* option 1 */
			armCfg this_cfg = vec_joint_values.at(i);
			this_cfg[5] += 2.0*3.14;
			if(isInJointAngleRanges(this_cfg))
				turn_solutions.push_back(this_cfg);
			
			/* option 2 */
			this_cfg = vec_joint_values.at(i);
			this_cfg[5] -= 2.0*3.14;
			if(isInJointAngleRanges(this_cfg))
				turn_solutions.push_back(this_cfg);
		}
		ROS_INFO("IKFast found %zd solutions. Augment with %zd solutions with turns of last joint", vec_joint_values.size(), turn_solutions.size());
		if (!turn_solutions.empty())
		{
			vec_joint_values.insert(vec_joint_values.end(), turn_solutions.begin(), turn_solutions.end());
		}

		// #ifdef DEBUG
		for (int i=0; i<vec_joint_values.size(); ++i)
		{
			std::cout << "IK solutions are " << i << std::endl;
			std::cout << "[ ";
			for (int j=0; j<vec_joint_values.at(i).size(); ++j)
			{
				std::cout << vec_joint_values.at(i).at(j) << " ";
			}
			std::cout << "]" << std::endl;
		}
		// #endif

		return true;
	}
	else
	{	
		ROS_ERROR("No IK solution found.") ;
		return false;
	}
}

void rampSetting::getNumberOfDofs(int &numOfDofs)
{
	armCfg jv;
	kinematic_state->copyJointGroupPositions(joint_model_group, jv);
	numOfDofs = jv.size();
	#ifdef DEBUG
	ROS_INFO("This kinematic chain has %d DOFs", numOfDofs);
	#endif
}

robot_model::RobotModelPtr& rampSetting::getRobotModelPtr()
{
	return kinematic_model;
}

void rampSetting::setJointNames(const std::vector<std::string> &jnt_names)
{
	joint_names.clear();
	joint_names = jnt_names;
}

std::vector<std::string> rampSetting::getJointNames()
{
	return joint_names;
}

const std::string& rampSetting::getJointModelGroupName()
{
	return joint_group_name;
}

void rampSetting::setJointBoundsMap()
{
	for (int i=0; i<joint_names.size(); ++i)
	{
		joint_bounds_maps[joint_names.at(i)] = kinematic_model->getVariableBounds(joint_names.at(i));
	}
}

const moveit::core::VariableBounds& rampSetting::getJointBounds(const std::string &joint_name)
{
	return joint_bounds_maps.find(joint_name)->second;
}

void rampSetting::printJointBoundsInfo()
{
	for (int i=0; i<joint_names.size(); ++i)
	{
		ROS_INFO("Joint %s bounds are:", joint_names.at(i).c_str());
		const moveit::core::VariableBounds& tmp = getJointBounds(joint_names.at(i));
		if (tmp.position_bounded_)
			ROS_INFO("Position [%f, %f]", tmp.min_position_, tmp.max_position_);
		else
			ROS_WARN("Position is not bounded.");

		if (tmp.velocity_bounded_)
			ROS_INFO("Velocity [%f, %f]", tmp.min_velocity_, tmp.max_velocity_);
		else
			ROS_WARN("Velocity is not bounded.");

		if (tmp.acceleration_bounded_)
			ROS_INFO("Acceleration [%f, %f]", tmp.min_acceleration_, tmp.max_acceleration_);
		else
			ROS_WARN("Acceleration is not bounded.");
	}
}

bool rampSetting::isInJointAngleRanges(const armCfg &cfg)
{
	for (int i=0; i<joint_names.size(); ++i)
	{
		const moveit::core::VariableBounds& tmp = getJointBounds(joint_names.at(i));
		if (!tmp.position_bounded_)
			ROS_WARN("Joint position is not bounded.");
		if ( cfg.at(i) < tmp.min_position_ || cfg.at(i) > tmp.max_position_ )
			return false;
	}
	return true;
}

bool rampSetting::isRobotColliding(const armCfg &cfg, const boost::shared_ptr<planning_scene::PlanningScene>& ps)
{
	setJointConfiguration(cfg);
	bool isColliding = false;
	if (ps->isStateColliding(*kinematic_state))
		isColliding = true;
	return isColliding;
}

bool rampSetting::isRobotColliding(const armCfg &cfg, const planning_scene_monitor::LockedPlanningSceneRO &ps)
{
	/*	This step is useful since it updates class private 
		member kinematic_state, which will be passed in 
		when checking for collision
	*/
	setJointConfiguration(cfg);
	int method = 1;
	bool isColliding = false;
	switch (method)
	{
		// check full collision
		case 1:
		{
			if (ps->isStateColliding(*kinematic_state))
				isColliding = true;
			break;
		}
		// check full collision
		case 2:
		{
			collision_detection::CollisionResult::ContactMap contacts;
			ps->getCollidingPairs(contacts, *kinematic_state);
			if (contacts.size()!=0)
				isColliding = true;
			break;
		}
		// check self collision only
		case 3:
		{
			collision_detection::CollisionRequest collision_request;
			collision_detection::CollisionResult collision_result;
			ps->checkSelfCollision(collision_request, collision_result, *kinematic_state);
			if (collision_result.collision)
				isColliding = true;
			break;
		}
	}
	return isColliding;
}

double rampSetting::getManipulabilityIndex(const armCfg &cfg)
{
	setJointConfiguration(cfg);
	double manipulability_index = 0.0;
	bool status = false;

	int method = 2;
	// seems that method 1 and 2 have similar run time speeds
	switch (method)
	{
		case 1:
		{
			Eigen::MatrixXd jacobian = kinematic_state->getJacobian(joint_model_group);
			Eigen::MatrixXd matrix = jacobian*jacobian.transpose();
			manipulability_index = sqrt(matrix.determinant());
			status = true;
			break;
		}
		case 2:
		{
			// this is an expensive operation...
			kinematic_state->update();
			status = kine_metrics_ptr->getManipulabilityIndex(*kinematic_state,
												  		 	  joint_model_group,
													 		  manipulability_index);
			break;
		}
	}
	
	if (status)
		return manipulability_index;
	else
	{
		ROS_ERROR("Failed to compute manipulability index");
		return -1.0;
	}
}

Eigen::MatrixXd rampSetting::getJacobian(const armCfg &cfg)
{
	setJointConfiguration(cfg);
	return kinematic_state->getJacobian(joint_model_group);
}

void rampSetting::setTaskCstrPose(const geometry_msgs::Pose &tc_p)
{
	task_cstr_pose = tc_p;
}

double rampSetting::compareCurrentPose(const geometry_msgs::Pose &cur_p)
{
	/* 
		step 1
		task constrained frame 
	*/
	Eigen::Affine3d tc_affine3d;
	rampUtil::convertQuaterniontoAffine3d(task_cstr_pose, tc_affine3d);

	/* 
		step 2
		testing frame 
	*/
	Eigen::Affine3d cur_p_affine3d;
	rampUtil::convertQuaterniontoAffine3d(cur_p, cur_p_affine3d);

	/* 
		step 3
		compute affine difference 
	*/
	Eigen::Affine3d diff = tc_affine3d.inverse() * cur_p_affine3d;
	std::vector<double> diff_vec(6,0.);
	rampUtil::convertAffine3dtoXYZRPY(diff, diff_vec);

	/* compute angle difference wrt roll pitch yaw */
	diff_vec.at(0) = std::abs(diff_vec.at(0));
	diff_vec.at(1) = std::abs(diff_vec.at(1));
	diff_vec.at(2) = std::abs(diff_vec.at(2));
	diff_vec.at(3) = std::abs(diff_vec.at(3));
	diff_vec.at(4) = std::abs(diff_vec.at(4));
	diff_vec.at(5) = std::abs(diff_vec.at(5));
	/* 
		remove ambiguity in rpy 
		closer to 3.14 than 0.0
	*/
	if ( std::abs(diff_vec.at(3)-3.14) < std::abs(diff_vec.at(3)-0.0) )
		diff_vec.at(3) = std::abs(3.14 - diff_vec.at(3));
	if ( std::abs(diff_vec.at(4)-3.14) < std::abs(diff_vec.at(4)-0.0) )
		diff_vec.at(4) = std::abs(3.14 - diff_vec.at(4));
	if ( std::abs(diff_vec.at(5)-3.14) < std::abs(diff_vec.at(5)-0.0) )
		diff_vec.at(5) = std::abs(3.14 - diff_vec.at(5));

	if (ramp_params.task_constraint_axis=="XY-O")
	{
		return diff_vec[3] + diff_vec[4];
	}

	if (ramp_params.task_constraint_axis=="XZ-P-XYZ-O")
	{
		float orn_error = diff_vec.at(3) + diff_vec.at(4) + diff_vec[5];
		float pos_error = std::abs(cur_p.position.x-task_cstr_pose.position.x) + std::abs(cur_p.position.z-task_cstr_pose.position.z);
		return orn_error + pos_error;
	}
}

rampSetting::~rampSetting()
{

}

