#include "ramp_abb/graph.h" 

node::node()
{
	id = -1;
	motion_point_id = -1;
}

node::node(const int &index, const int &motion_pnt_id, const armCfg &jnt_cfg)
{
	id = index;
	motion_point_id = motion_pnt_id;
	joint_cfg = jnt_cfg;
}

edge::edge(const node &des_node, const double &weight_cost, const moveit_msgs::RobotTrajectory &trajectory, const motion_type type)
:target_node(des_node.id, des_node.motion_point_id, des_node.joint_cfg)
{
	weight = weight_cost;
	traj = trajectory;
	edge_type = type;
}

graph::graph()
{
	num_of_nodes = 0;
}

void graph::addNode(const node &a)
{	
	nodes.insert(a);
	num_of_nodes = nodes.size();
}

void graph::addEdge(const node &a, const edge &e)
{
	/* check if a is new node */
	std::unordered_map<int, std::vector<edge> >::iterator it = adj_list.find(a.id);
	if(it != adj_list.end())
	{
		it->second.push_back(e);
	}
	else
	{
		std::vector<edge> v;
		v.push_back(e);
		adj_list[a.id] = v;
	}
	ROS_INFO("Adding edge between node %d and node %d", a.id, e.target_node.id);
	nodes.insert(a);
	nodes.insert(e.target_node);
	num_of_nodes = nodes.size();
}

void graph::removeEdge(const node &a, const edge &e)
{
	/* check if a exists */
	std::unordered_map<int, std::vector<edge> >::iterator it = adj_list.find(a.id);
	if(it != adj_list.end())
	{
		std::vector<edge> ns = it->second;
		int remove_index = -1;
		for(int i = 0; i<ns.size(); ++i)
		{
			if (ns[i].target_node.id = e.target_node.id)
			{
				remove_index = i;
				break;
			}
		}
		it->second.erase(it->second.begin()+remove_index);

		/* if no edges incoming or outgoing, also remove this node */
		if(it->second.empty())
		{
			std::set<node>::iterator set_it;
			for(set_it=nodes.begin(); set_it!=nodes.end(); ++set_it)
			{
				if(set_it->id == a.id)
					break;
			}
			nodes.erase(set_it);
			num_of_nodes = nodes.size();
			adj_list.erase(it);
		}
	}
	else
		std::cout << "Not found node " << a.id << std::endl;
}

void graph::getEdgeTrajectory(const int a_id, const int b_id, moveit_msgs::RobotTrajectory &traj, bool only_key_cfgs)
{
	std::unordered_map<int, std::vector<edge> >::iterator it = adj_list.find(a_id);
	if(it != adj_list.end())
	{
		std::vector<edge> ns = it->second;
		for(int i = 0; i<ns.size(); ++i)
		{
			if (ns[i].target_node.id == b_id)
			{
				if (only_key_cfgs)
				{
					switch (ns[i].edge_type)
					{
						case FREE:
						{
							traj = ns[i].traj;
							break;
						}
						case LINEAR:
						{
							traj.joint_trajectory.joint_names = ns[i].traj.joint_trajectory.joint_names;
							traj.joint_trajectory.points.push_back( ns[i].traj.joint_trajectory.points.front() );
							traj.joint_trajectory.points.push_back( ns[i].traj.joint_trajectory.points.back() );
							break;
						}
						case ARC:
						{
							traj.joint_trajectory.joint_names = ns[i].traj.joint_trajectory.joint_names;
							traj.joint_trajectory.points.push_back( ns[i].traj.joint_trajectory.points.front() );
							traj.joint_trajectory.points.push_back( ns[i].traj.joint_trajectory.points.back() );
							break;
						}
					}
				}
				else
					traj = ns[i].traj;
				break;
			}
		}
	}
	else
		std::cout << "Not found node with id " << a_id << std::endl;
}

bool graph::hasNodeWithJntCfg(const armCfg &cfg, node &exist_node)
{
	std::set<node>::iterator set_it;
	std::set<node>::iterator last_same_jnt_it;
	bool found_repeat = false;
	for(set_it=nodes.begin(); set_it!=nodes.end(); ++set_it)
	{
		if(rampUtil::areSameJntCfgs(set_it->joint_cfg, cfg))
		{
			last_same_jnt_it = set_it;
			found_repeat = true;
		}
	}
	if (found_repeat && last_same_jnt_it != nodes.end())
	{
		exist_node = *last_same_jnt_it;
		return true;
	}
	else
		return false;
}

void graph::shortestPath(const int &src_id, const int &des_id, std::vector<int> &path)
{	
	std::priority_queue< iPair, std::vector <iPair> , std::greater<iPair> > pq;
	std::vector<double> dist(num_of_nodes, std::numeric_limits<double>::max());
	std::vector<int> parent(num_of_nodes);
	parent[0] = -1;
	
	pq.push(std::make_pair(0., src_id));
    dist[src_id] = 0.;
	while (!pq.empty())
    {
        int u_id = pq.top().second;
        pq.pop();

		std::unordered_map<int, std::vector<edge> >::const_iterator cit = adj_list.find(u_id);;
		if(cit != adj_list.end())
		{
			for( int i = 0; i < cit->second.size(); ++i ) 
			{
				int v_id = cit->second[i].target_node.id;
				double weight = cit->second[i].weight;

				/* If there is shorted path to v through u */
				if (dist[v_id] > dist[u_id] + weight)
				{
					/* Updating distance of v */
					parent[v_id] = u_id;
					dist[v_id] = dist[u_id] + weight;
					pq.push(std::make_pair(dist[v_id], v_id));
				}
			}
		}
	}

	/* fill in shortest path information */
	path.push_back(src_id);
	printPath(parent, des_id, path);

	/* print path from source to goal */
	printf("\nPath from source to goal\n");
	for(int i=0; i<path.size(); ++i)
		std::cout << path[i] << " ";
	std::cout << std::endl;
}

void graph::printPath(const std::vector<int> &parent, const int &des_node_id, std::vector<int> &path)
{
    if (parent[des_node_id] == -1)
        return;
 
    printPath(parent, parent[des_node_id], path);
	path.push_back(des_node_id);
}

void graph::printPath(const std::vector<int> &parent, const int &des_node_id)
{
    if (parent[des_node_id] == -1)
        return;
 
    printPath(parent, parent[des_node_id]);
    printf("%d ", des_node_id);
}

void graph::printSolution(const std::vector<double> &dist, const int &n, const std::vector<int> &parent)
{
    int src = 0;
    printf("Vertex\t Distance\tPath");
    for (int i = 1; i < n; i++)
    {
        printf("\n%d -> %d \t\t %f\t\t%d ", src, i, dist[i], src);
        printPath(parent, i);
    }
}
 
void graph::printAdjList() 
{
	std::cout << "Planning graph adj list. for " << num_of_nodes << " many nodes. " << std::endl;
	std::unordered_map<int, std::vector<edge> >::const_iterator cit;
	for( cit = adj_list.begin(); cit != adj_list.end(); ++cit )
	{
		std::cout << " for node with id " << cit->first << " :  ";
		for( int i = 0; i < cit->second.size(); ++i ) {
			std::cout << "[" << cit->second[i].target_node.id << "(" << cit->second[i].weight << ")] ";
		}
		std::cout << std::endl;
	}
}

void graph::printNodesInfo()
{
	std::set<node>::iterator set_it;
	for(set_it=nodes.begin(); set_it!=nodes.end(); ++set_it)
	{
		ROS_INFO("Node %d:", set_it->id);
		rampUtil::printArmCfg(set_it->joint_cfg);
	}
}

void graph::reset()
{
	num_of_nodes = 0;
	adj_list.clear();
	nodes.clear();
}

