#include "ramp_abb/arc_solver.h" 

arcSolver::arcSolver()
{
	Radius = 0.;
	Angle = 0.;
}

/* get arc position at t [0..1] */
Eigen::Vector3f arcSolver::arc(double t)
{
	double x = t*Angle;
	return Center + Radius *cos(x) * FDirP1 + Radius * sin(x) * FDirP2;
}

bool arcSolver::solveArc(const Eigen::Vector3f &P1, const Eigen::Vector3f &P2, const Eigen::Vector3f &P3)
{
	Eigen::Vector3f O = (P2 + P3) / 2.0;
	Eigen::Vector3f C = (P1 + P3) / 2.0;
	Eigen::Vector3f X = (P2 - P1) / -2.0;

	Eigen::Vector3f N = (P3 - P1).cross(P2 - P1);
	Eigen::Vector3f D = N.cross(P2 - O);
	D.normalize();
	Eigen::Vector3f V = P1 - C;
	V.normalize();

	double check = D.dot(V);
    Angle = M_PI;
    bool exist = false;

	if (std::abs(check) > 0.)
	{
		double t = X.dot(V) / check;
		Center = O + D*t;
		Radius = (Center - P1).norm();
		Eigen::Vector3f V1 = (P1 - Center);
		V1.normalize();

		//vector from center to P1
		FDirP1 = V1;
		Eigen::Vector3f V2 = (P3 - Center);
		V2.normalize();
		Angle = acos(V1.dot(V2));

		if (std::abs(Angle) > 0.)
		{
			exist = true;
			V1 = P2-P1;
			V2 = P2-P3;
			if ((V1.dot(V2)) > 0.)
			{
				Angle = M_PI * 2.0 - Angle;
			}
		}
	}

	printf("Arc center [%f, %f, %f] radius %f\n", Center[0], Center[1], Center[2], Radius);

	//vector from center to P2
	FDirP2 = -N.cross(P1 - Center);
	FDirP2.normalize();
	return exist;
}
