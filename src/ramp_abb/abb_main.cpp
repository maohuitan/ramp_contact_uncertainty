#include <ros/ros.h>
#include "ramp_abb/ramp_abb_test.h"

/* 
 * \brief global variables: ramp parameters and experiment stats
 */
rampParameters ramp_params;
statistics stats;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_abb_node");
	ros::NodeHandle n;
	ROS_INFO("Sleeping to wait for MoveIt and Rviz setup...");
	sleep(3.0);

	rampABBtest test;
	ROS_INFO("**********************************");
	ROS_INFO("RAMP-ABB motion node running.");
	ROS_INFO("**********************************");
	// test.testGraph();
	// test.testPointCloudLoading();
	// test.testCfgConversion();
	// test.testArcMotion(); 

	ros::waitForShutdown();
	return 0;
}

