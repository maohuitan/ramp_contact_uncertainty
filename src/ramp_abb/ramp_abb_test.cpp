#include "ramp_abb/ramp_abb_test.h" 

rampABBtest::rampABBtest()
:ramp_abb_mp("panda_arm", ""), g()
{
	ramp_abb_mp.initialize();
}

void rampABBtest::testCfgConversion()
{
	armCfg a(6, 0.), b(6, 0.), c(6, 0.), d(6, 0.), e(6, 0.);
	abbCfg abb_cfg;
	a[0] = -41.26; a[1] = -0.18; a[2] = 24.39; a[3] = -64.94; a[4] = -46.71; a[5] = 55.71;
	b[0] = -41.26; b[1] = -0.18; b[2] = 24.39; b[3] = -64.94; b[4] = -46.71; b[5] = -304.29;
	c[0] = -41.26; c[1] = -0.18; c[2] = 24.39; c[3] = 115.06; c[4] = 46.71; c[5] = -124.29;
	d[0] = -41.26; d[1] = -0.18; d[2] = 24.39; d[3] = 115.06; d[4] = 46.7; d[5] = 235.71;

	ramp_abb_mp.convertRobotCfgToABBCfg(a, abb_cfg);
	ramp_abb_mp.convertRobotCfgToABBCfg(b, abb_cfg);
	ramp_abb_mp.convertRobotCfgToABBCfg(c, abb_cfg);
	ramp_abb_mp.convertRobotCfgToABBCfg(d, abb_cfg);
}

void rampABBtest::testPointCloudLoading()
{
	std::string test("/home/abb-intern1/Dropbox/Development/cam_calib/transformation_estimation_displacedCam/baseline/scene_1_1.pcd");
	
	std::vector<double> pose12_vec(6,0.0);
	pose12_vec[0] = 0.1; pose12_vec[1] = -0.1;  pose12_vec[2] = 1.1;
	pose12_vec[3] = 0.0; pose12_vec[4] = 1.0; pose12_vec[5] = 0.0;
	Eigen::Affine3d tf_from_camera_to_robot_base;
	rampUtil::convertXYZRPYtoAffine3d(pose12_vec, tf_from_camera_to_robot_base);
	ramp_abb_mp.loadPointCloud(tf_from_camera_to_robot_base, test);
}

void rampABBtest::testGraph()
{
	armCfg tmp(6, 0.);
	moveit_msgs::RobotTrajectory traj;

	node n0(0, 0, tmp);
	node n1(1, 0, tmp);
	node n2(2, 0, tmp);
	node n3(3, 0, tmp);
	node n4(4, 0, tmp);
	
	edge e0(n1, 1.0, traj, FREE);
	edge e1(n2, 1.0, traj, FREE);
	edge e2(n3, 1.0, traj, FREE);
	edge e3(n3, 1.0, traj, FREE);
	edge e4(n4, 1.0, traj, FREE);

	g.addEdge(n0, e0);
	g.addEdge(n0, e1);
	g.addEdge(n1, e2);
	g.addEdge(n2, e3);
	g.addEdge(n3, e4);
	g.printAdjList();

	g.removeEdge(n0, e0);
	g.removeEdge(n1, e2);
	g.printAdjList();
}

// void rampABBtest::testArcMotion()
// {
// 	geometry_msgs::Pose pose1;
// 	pose1.orientation.w = 1.0;
// 	pose1.position.x = 0.5;
// 	pose1.position.y = 0.0;
// 	pose1.position.z = 0.5;
// 	via_type v1 = FLY_BY;
// 	motion_type m1 = FREE;
// 	motionViaPoint p1(pose1, v1, m1, true);

// 	geometry_msgs::Pose pose2;
// 	pose2.orientation.w = 1.0;
// 	pose2.position.x = 0.353;
// 	pose2.position.y = -0.353;
// 	pose2.position.z = 0.5;
// 	via_type v2 = FLY_BY;
// 	motion_type m2 = ARC;
// 	motionViaPoint p2(pose2, v2, m2, true);

// 	geometry_msgs::Pose pose3;
// 	pose3.orientation.w = 1.0;
// 	pose3.position.x = 0.0;
// 	pose3.position.y = -0.5;
// 	pose3.position.z = 0.5;
// 	via_type v3 = FLY_BY;
// 	motion_type m3 = ARC;
// 	motionViaPoint p3(pose3, v3, m3, true);

// 	// point 4
// 	geometry_msgs::Pose pose4;
// 	pose4.orientation.w = 1.0;
// 	pose4.position.x = 0.0; 
// 	pose4.position.y = -0.4; 
// 	pose4.position.z = 0.5;
// 	via_type v4 = FLY_BY;
// 	motion_type m4 = LINEAR;
// 	motionViaPoint p4(pose4, v4, m4, true);

// 	// point 5
// 	geometry_msgs::Pose pose5;
// 	pose5.orientation.w = 1.0;
// 	pose5.position.x = 0.3;
// 	pose5.position.y = 0.;
// 	pose5.position.z = 0.7;
// 	via_type v5 = STOP;
// 	motion_type m5 = FREE;
// 	motionViaPoint p5(pose5, v5, m5, true);

// 	// point 6
// 	geometry_msgs::Pose pose6;
// 	pose6.orientation.w = 1.0;
// 	pose6.position.x = 0.0; 
// 	pose6.position.y = -0.4; 
// 	pose6.position.z = 0.5;
// 	via_type v6 = FLY_BY;
// 	motion_type m6 = FREE;
// 	motionViaPoint p6(pose6, v6, m6, true);

// 	// point 7
// 	geometry_msgs::Pose pose7;
// 	pose7.orientation.w = 1.0;
// 	pose7.position.x = 0.0; 
// 	pose7.position.y = -0.5; 
// 	pose7.position.z = 0.5;
// 	via_type v7 = FLY_BY;
// 	motion_type m7 = LINEAR;
// 	motionViaPoint p7(pose7, v7, m7, true);

// 	// point 8
// 	geometry_msgs::Pose pose8;
// 	pose8.orientation.w = 1.0;
// 	pose8.position.x = 0.0; 
// 	pose8.position.y = 0.4; 
// 	pose8.position.z = 0.5;
// 	via_type v8 = FLY_BY;
// 	motion_type m8 = FREE;
// 	motionViaPoint p8(pose8, v8, m8, true);

// 	// point 9
// 	geometry_msgs::Pose pose9;
// 	pose9.orientation.w = 1.0;
// 	pose9.position.x = 0.0; 
// 	pose9.position.y = 0.5; 
// 	pose9.position.z = 0.5;
// 	via_type v9 = FLY_BY;
// 	motion_type m9 = LINEAR;
// 	motionViaPoint p9(pose9, v9, m9, true);

// 	// point 10
// 	geometry_msgs::Pose pose10;
// 	pose10.orientation.w = 1.0;
// 	pose10.position.x = 0.0; 
// 	pose10.position.y = -0.6; 
// 	pose10.position.z = 0.5;
// 	via_type v10 = FLY_BY;
// 	motion_type m10 = FREE;
// 	motionViaPoint p10(pose10, v10, m10, true);

// 	// point 11
// 	geometry_msgs::Pose pose11;
// 	pose11.orientation.w = 1.0;
// 	pose11.position.x = 0.0; 
// 	pose11.position.y = -0.6; 
// 	pose11.position.z = 0.5;
// 	via_type v11 = FLY_BY;
// 	motion_type m11 = LINEAR;
// 	motionViaPoint p11(pose11, v11, m11, true);

// 	/* 
// 		Caution!
// 		Please use euler angles to specify orientation first 
// 		then convert to quaternion using the helper function.
		
// 		Please do not specify quaternion directly even though 
// 		the numbers seem to be correct!
		
// 		IK solver seems to be not working well with manually
// 		specified quaternions.
// 	 */
// 	// point 12 (90 deg around Y axis(0,1,0))
// 	// auto angle = M_PI / 2;
//     // auto sinA = std::sin(angle / 2);
//     // auto cosA = std::cos(angle / 2);

// 	// Eigen::Quaterniond quat(cosA, 0.*sinA, 1.0*sinA, 0.*sinA);
// 	// quat.normalize();

// 	std::vector<double> pose12_vec(6,0.0);
// 	pose12_vec[0] = 0.4; pose12_vec[1] = 0.0;  pose12_vec[2] = 0.4;
// 	pose12_vec[3] = 0.0; pose12_vec[4] = 1.57; pose12_vec[5] = 0.0;
// 	geometry_msgs::Pose pose12;
// 	rampUtil::convertXYZRPYtoQuaternion(pose12_vec, pose12);

// 	// pose12.position.x = 0.4; 
// 	// pose12.position.y = 0.0; 
// 	// pose12.position.z = 0.4;
// 	// pose12.orientation.w = quat.w();
// 	// pose12.orientation.x = quat.x();
// 	// pose12.orientation.y = quat.y();
// 	// pose12.orientation.z = quat.z();
// 	via_type v12 = FLY_BY;
// 	motion_type m12 = FREE;
// 	motionViaPoint p12(pose12, v12, m12, true);

// 	// point 13
// 	std::vector<double> pose13_vec(6,0.0);
// 	pose13_vec[0] = 0.4*0.707; pose13_vec[1] = -0.4*0.707;  pose13_vec[2] = 0.4;
// 	pose13_vec[3] = 0.;      pose13_vec[4] = 1.57;        pose13_vec[5] = 0.;
// 	geometry_msgs::Pose pose13;
// 	rampUtil::convertXYZRPYtoQuaternion(pose13_vec, pose13);
// 	via_type v13 = FLY_BY;
// 	motion_type m13 = ARC;
// 	motionViaPoint p13(pose13, v13, m13, true);

// 	// point 14
// 	std::vector<double> pose14_vec(6,0.0);
// 	pose14_vec[0] = 0.0; pose14_vec[1] = -0.4;  pose14_vec[2] = 0.4;
// 	pose14_vec[3] = 0.; pose14_vec[4] = 1.57;  pose14_vec[5] = 0.;
// 	geometry_msgs::Pose pose14;
// 	rampUtil::convertXYZRPYtoQuaternion(pose14_vec, pose14);
// 	via_type v14 = FLY_BY;
// 	motion_type m14 = ARC;
// 	motionViaPoint p14(pose14, v14, m14, true);

// 	// point 15
// 	std::vector<double> pose15_vec(6,0.0);
// 	pose15_vec[0] = -0.1; pose15_vec[1] = -0.5;  pose15_vec[2] = 0.4;
// 	pose15_vec[3] = 0.; pose15_vec[4] = 1.57;  pose15_vec[5] = 0.;
// 	geometry_msgs::Pose pose15;
// 	rampUtil::convertXYZRPYtoQuaternion(pose15_vec, pose15);
// 	via_type v15 = FLY_BY;
// 	motion_type m15 = LINEAR;
// 	motionViaPoint p15(pose15, v15, m15, true);

// 	// point 16
// 	std::vector<double> pose16_vec(6,0.0);
// 	pose16_vec[0] = 0.0; pose16_vec[1] = -0.6;  pose16_vec[2] = 0.4;
// 	pose16_vec[3] = 0.; pose16_vec[4] = 1.57; pose16_vec[5] = 0.;
// 	geometry_msgs::Pose pose16;
// 	rampUtil::convertXYZRPYtoQuaternion(pose16_vec, pose16);
// 	via_type v16 = FLY_BY;
// 	motion_type m16 = LINEAR;
// 	motionViaPoint p16(pose16, v16, m16, true);

// 	// point 17
// 	std::vector<double> pose17_vec(6,0.0);
// 	pose17_vec[0] = 0.0; pose17_vec[1] = 0.4;  pose17_vec[2] = 0.5;
// 	pose17_vec[3] = 0.0; pose17_vec[4] = 1.57; pose17_vec[5] = 0.0;
// 	geometry_msgs::Pose pose17;
// 	rampUtil::convertXYZRPYtoQuaternion(pose17_vec, pose17);
// 	via_type v17 = FLY_BY;
// 	motion_type m17 = FREE;
// 	motionViaPoint p17(pose17, v17, m17, true);

// 	// point 18
// 	std::vector<double> pose18_vec(6,0.0);
// 	pose18_vec[0] = 0.0; pose18_vec[1] = 0.5;  pose18_vec[2] = 0.5;
// 	pose18_vec[3] = 0.0; pose18_vec[4] = 1.57;  pose18_vec[5] = 0.0;
// 	geometry_msgs::Pose pose18;
// 	rampUtil::convertXYZRPYtoQuaternion(pose18_vec, pose18);
// 	via_type v18 = FLY_BY;
// 	motion_type m18 = LINEAR;
// 	motionViaPoint p18(pose18, v18, m18, true);

// 	/* arc */
// 	// point 19
// 	std::vector<double> pose19_vec(6,0.0);
// 	pose19_vec[0] = 0.2; pose19_vec[1] = 0.2; pose19_vec[2] = 0.3;
// 	pose19_vec[3] = 0.0; pose19_vec[4] = 0.0; pose19_vec[5] = 0.0;
// 	geometry_msgs::Pose pose19;
// 	rampUtil::convertXYZRPYtoQuaternion(pose19_vec, pose19);
// 	via_type v19 = FLY_BY;
// 	motion_type m19 = FREE;
// 	motionViaPoint p19(pose19, v19, m19, true);

// 	// point 20
// 	std::vector<double> pose20_vec(6,0.0);
// 	pose20_vec[0] = 0.2; pose20_vec[1] = 0.1;  pose20_vec[2] = 0.4;
// 	pose20_vec[3] = 0.0; pose20_vec[4] = 0.0;  pose20_vec[5] = 0.0;
// 	geometry_msgs::Pose pose20;
// 	rampUtil::convertXYZRPYtoQuaternion(pose20_vec, pose20);
// 	via_type v20 = FLY_BY;
// 	motion_type m20 = ARC;
// 	motionViaPoint p20(pose20, v20, m20, true);

// 	// point 21
// 	std::vector<double> pose21_vec(6,0.0);
// 	pose21_vec[0] = 0.2; pose21_vec[1] = 0.2; pose21_vec[2] = 0.5;
// 	pose21_vec[3] = 0.0; pose21_vec[4] = 0.0; pose21_vec[5] = 0.0;
// 	geometry_msgs::Pose pose21;
// 	rampUtil::convertXYZRPYtoQuaternion(pose21_vec, pose21);
// 	via_type v21 = FLY_BY;
// 	motion_type m21 = ARC;
// 	motionViaPoint p21(pose21, v21, m21, true);

// 	// point 22
// 	std::vector<double> pose22_vec(6,0.0);
// 	pose22_vec[0] = 0.3; pose22_vec[1] = 0.0;  pose22_vec[2] = 0.4;
// 	pose22_vec[3] = 0.0; pose22_vec[4] = 1.57; pose22_vec[5] = 0.0;
// 	geometry_msgs::Pose pose22;
// 	rampUtil::convertXYZRPYtoQuaternion(pose22_vec, pose22);
// 	via_type v22 = FLY_BY;
// 	motion_type m22 = FREE;
// 	motionViaPoint p22(pose22, v22, m22, true);

// 	std::vector<motionViaPoint> points;
// 	// points.push_back(p5);
// 	// points.push_back(p10);
// 	// points.push_back(p1);
// 	// points.push_back(p2);
// 	// points.push_back(p3);
// 	// points.push_back(p11);
// 	// points.push_back(p4);
// 	// points.push_back(p5);

// 	// points.push_back(p6);
// 	// points.push_back(p7);
// 	// points.push_back(p2);
// 	// points.push_back(p1);
// 	// points.push_back(p11);
// 	// points.push_back(p5);

// 	// points.push_back(p1);
// 	// points.push_back(p2);
// 	// points.push_back(p3);
// 	// points.push_back(p11);
// 	// points.push_back(p4);
// 	// points.push_back(p8);
// 	// points.push_back(p9);
// 	// points.push_back(p5);

// 	points.push_back(p22);
// 	points.push_back(p12);
// 	points.push_back(p13);
// 	points.push_back(p14);
// 	points.push_back(p15);
// 	points.push_back(p16);
// 	points.push_back(p17);
// 	points.push_back(p18);
// 	points.push_back(p19);
// 	points.push_back(p20);
// 	points.push_back(p21);
// 	points.push_back(p5);

// 	std::vector<double> tf_vec(6,0.0);
// 	tf_vec[0] = 0.1;
// 	tf_vec[4] = 1.57;
// 	Eigen::Affine3d tf_from_toolx_tool0;
// 	rampUtil::convertXYZRPYtoAffine3d(tf_vec, tf_from_toolx_tool0);

// 	Eigen::Affine3d tf_from_attached_obj_to_toolx;
// 	tf_from_attached_obj_to_toolx = Eigen::Matrix3d::Identity();
// 	tf_from_attached_obj_to_toolx.translation()[0] = 0.15;
// 	tf_from_attached_obj_to_toolx.translation()[2] = 0.1;

// 	std::string mesh_path("package://ramp/mesh/theboy_small_coarse.stl"); 
// 	std::string object_mesh_path("package://ramp/mesh/cylinder_2x12cm_DxH.obj"); 

// 	ramp_abb_mp.setFlagContinueOnUnreachable(true);
// 	ramp_abb_mp.setOutputKeyCfgsFlag(false); // true for key cfgs only

// 	ramp_abb_mp.addTool(tf_from_toolx_tool0, mesh_path);
// 	ramp_abb_mp.addAttachedObject(tf_from_attached_obj_to_toolx, object_mesh_path);
// 	// sleep(3.0);
// 	// ramp_abb_mp.removeTool();
// 	// sleep(3.0);
// 	// ramp_abb_mp.removeAttachedObject();

// 	ramp_abb_mp.setViaPoints(points);
// 	moveit_msgs::RobotTrajectory traj;
// 	std::vector<moveit_msgs::RobotTrajectory> traj_segs;
// 	std::vector<int> reachable_via_pnt_inds;
// 	ramp_abb_mp.generateMotionPlanViaGraphSearch(traj, reachable_via_pnt_inds, traj_segs); 
// 	// rampUtil::printTrajectory(traj);
// }

// void rampABBtest::testContactMotion()
// {
// 	int num_of_steps = 4;
// 	double y_res = -0.05;
// 	double x_range = 0.1;
// 	std::vector<double> start_pose_vec(6,0.0);
// 	start_pose_vec[0] = 0.23; start_pose_vec[1] = 0.15;  start_pose_vec[2] = 0.27;
// 	start_pose_vec[3] = 0.0; start_pose_vec[4] = 1.57; start_pose_vec[5] = 0.0;
// 	std::vector<motionViaPoint> points;
// 	for (int i=0; i<num_of_steps; ++i)
// 	{
// 		/* start pose */
// 		std::vector<double> p_start_vec = start_pose_vec;
// 		p_start_vec[1] += i*y_res; 
// 		geometry_msgs::Pose p_start;
// 		rampUtil::convertXYZRPYtoQuaternion(p_start_vec, p_start);
// 		points.push_back(motionViaPoint(p_start, FLY_BY, FREE, true));

// 		/* end pose */
// 		std::vector<double> p_end_vec = start_pose_vec;
// 		p_end_vec[0] += x_range; 
// 		p_end_vec[1] = p_start_vec[1]; 
// 		geometry_msgs::Pose p_end;
// 		rampUtil::convertXYZRPYtoQuaternion(p_end_vec, p_end);
// 		points.push_back(motionViaPoint(p_end, FLY_BY, LINEAR, true));
// 	}

// 	ramp_abb_mp.setFlagContinueOnUnreachable(true);
// 	ramp_abb_mp.setOutputKeyCfgsFlag(false); // true for key cfgs only

// 	/* add scene object */
// 	std::vector<double> pose_vec_scene_object_to_arm_base(6,0.0);
// 	pose_vec_scene_object_to_arm_base[0] = 0.5; pose_vec_scene_object_to_arm_base[1] = 0.05;  pose_vec_scene_object_to_arm_base[2] = 0.2;
// 	pose_vec_scene_object_to_arm_base[3] = 0.0; pose_vec_scene_object_to_arm_base[4] = 0.0; pose_vec_scene_object_to_arm_base[5] = -1.57;

// 	Eigen::Affine3d tf_scene_object_to_arm_base;
// 	rampUtil::convertXYZRPYtoAffine3d(pose_vec_scene_object_to_arm_base, tf_scene_object_to_arm_base);
// 	std::string object_mesh_path("package://ramp/mesh/contact-part-handle.stl"); 
// 	std::string object_id("scene_object");
// 	moveit_msgs::ObjectColor color;
// 	color.id = object_id;
// 	color.color.r = 0.6; color.color.g = 0.6;
// 	color.color.b = 0.6; color.color.a = 1.0;
// 	Eigen::Vector3d inch_to_m(1.0/39.37, 1.0/39.37, 1.0/39.37);
// 	ramp_abb_mp.addSceneObject("base_link", tf_scene_object_to_arm_base, object_mesh_path, inch_to_m, object_id, color);
// 	// ramp_abb_mp.removeSceneObject("scene_object");

// 	ramp_abb_mp.setViaPoints(points);
// 	moveit_msgs::RobotTrajectory traj;
// 	std::vector<moveit_msgs::RobotTrajectory> traj_segs;
// 	std::vector<int> reachable_via_pnt_inds;

// 	ramp_abb_mp.clearContactCfgContainer();
// 	ramp_abb_mp.generateMotionPlanViaGraphSearch(traj, reachable_via_pnt_inds, traj_segs); 
// 	// ramp_abb_mp.getContactCfgs();
// 	ramp_abb_mp.visualizeContactCfgs();
// 	// rampUtil::printTrajectory(traj);
// }

// void rampABBtest::testSphereTree()
// {
// 	std::string pkg_path = ros::package::getPath("ramp");
// 	std::string file_path = pkg_path+"/sphere_trees/aviation plug 4 pin - 2 pin aviation m plastic-1-spawn.sph";
// 	sphereTree spt(file_path);
// 	// spt.visualizeSpheresAtLevelX(1);
// 	// spt.visualizeSpheresAtLevelX(2);
// 	// spt.visualizeSpheresAtLevelX(3);
// 	// spt.visualizeSpheresAtLevelX(4);
// }



