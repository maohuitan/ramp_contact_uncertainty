#include <ros/ros.h>
#include <ramp/addTool.h>
#include <ramp/removeTool.h>
#include <ramp/addAttachedObject.h>
#include <ramp/removeAttachedObject.h>
#include <ramp/addPointCloudToScene.h>
#include <ramp/generateMotionPlan.h>
#include "ramp_util/ramp_util.h"

using namespace std;

ros::ServiceClient add_tool_client, remove_tool_client;
ros::ServiceClient add_object_client, remove_object_client;
ros::ServiceClient load_point_cloud_client, generate_mp_client;

int main(int argc,char **argv)
{
    ros::init(argc,argv,"bin_picking_test"); 
    ros::NodeHandle n;
    add_tool_client = n.serviceClient<ramp::addTool>("/abb_add_tool");
    remove_tool_client = n.serviceClient<ramp::removeTool>("/abb_remove_tool");
    add_object_client = n.serviceClient<ramp::addAttachedObject>("/abb_add_attached_object");
    remove_object_client = n.serviceClient<ramp::removeAttachedObject>("/abb_remove_attached_object");
    load_point_cloud_client = n.serviceClient<ramp::addPointCloudToScene>("/abb_load_point_cloud");
    generate_mp_client = n.serviceClient<ramp::generateMotionPlan>("/abb_generate_motion_plan");

    /* set tool object and pcl */
    std::vector<double> tf_vec(6,0.0);
	tf_vec[0] = 0.1;
	tf_vec[4] = 1.57;
	geometry_msgs::Pose quat_from_toolx_tool0;
	rampUtil::convertXYZRPYtoQuaternion(tf_vec, quat_from_toolx_tool0);

	geometry_msgs::Pose quat_from_attached_obj_to_toolx;
	quat_from_attached_obj_to_toolx.orientation.w = 1.0;
	quat_from_attached_obj_to_toolx.position.x = 0.15;
	quat_from_attached_obj_to_toolx.position.z = 0.1;

	std::string mesh_path("package://ramp/mesh/theboy_small_coarse.stl"); 
	std::string object_mesh_path("package://ramp/mesh/cylinder_2x12cm_DxH.obj"); 

    std::string pcl("/home/huitan/Dropbox/Development/cam_calib/transformation_estimation_displacedCam/baseline/scene_1_1.pcd");
	std::vector<double> pose_vec(6,0.0);
	pose_vec[0] = 0.1; pose_vec[1] = -0.1;  pose_vec[2] = 1.1;
	pose_vec[3] = 0.0; pose_vec[4] = 1.0; pose_vec[5] = 0.0;
	geometry_msgs::Pose quat_from_camera_to_robot_base;
	rampUtil::convertXYZRPYtoQuaternion(pose_vec, quat_from_camera_to_robot_base);

    std_msgs::String tool_path, obj_path, pcl_path;
    tool_path.data = mesh_path;
    obj_path.data = object_mesh_path;
    pcl_path.data = pcl;

    /* set via points */
    geometry_msgs::Pose pose5;
	pose5.orientation.w = 1.0;
	pose5.position.x = 0.3;
	pose5.position.y = 0.;
	pose5.position.z = 0.7;
    ramp::motionPlanRequest r5;
    r5.pose = pose5;
    r5.via_type.data = std::string("STOP");
    r5.motion_type.data = std::string("FREE");

    // point 12
    std::vector<double> pose12_vec(6,0.0);
	pose12_vec[0] = 0.4; pose12_vec[1] = 0.0;  pose12_vec[2] = 0.4;
	pose12_vec[3] = 0.0; pose12_vec[4] = 1.57; pose12_vec[5] = 0.0;
	geometry_msgs::Pose pose12;
	rampUtil::convertXYZRPYtoQuaternion(pose12_vec, pose12);
    ramp::motionPlanRequest r12;
    r12.pose = pose12;
    r12.via_type.data = std::string("FLY_BY");
    r12.motion_type.data = std::string("FREE");

	// point 13
	std::vector<double> pose13_vec(6,0.0);
	pose13_vec[0] = 0.4*0.707; pose13_vec[1] = -0.4*0.707;  pose13_vec[2] = 0.4;
	pose13_vec[3] = 0.;      pose13_vec[4] = 1.57;        pose13_vec[5] = 0.;
	geometry_msgs::Pose pose13;
	rampUtil::convertXYZRPYtoQuaternion(pose13_vec, pose13);
    ramp::motionPlanRequest r13;
    r13.pose = pose13;
    r13.via_type.data = std::string("FLY_BY");
    r13.motion_type.data = std::string("ARC");

	// point 14
	std::vector<double> pose14_vec(6,0.0);
	pose14_vec[0] = 0.0; pose14_vec[1] = -0.4;  pose14_vec[2] = 0.4;
	pose14_vec[3] = 0.; pose14_vec[4] = 1.57;  pose14_vec[5] = 0.;
	geometry_msgs::Pose pose14;
	rampUtil::convertXYZRPYtoQuaternion(pose14_vec, pose14);
    ramp::motionPlanRequest r14;
    r14.pose = pose14;
    r14.via_type.data = std::string("FLY_BY");
    r14.motion_type.data = std::string("ARC");

	// point 15
	std::vector<double> pose15_vec(6,0.0);
	pose15_vec[0] = -0.1; pose15_vec[1] = -0.5;  pose15_vec[2] = 0.4;
	pose15_vec[3] = 0.; pose15_vec[4] = 1.57;  pose15_vec[5] = 0.;
	geometry_msgs::Pose pose15;
	rampUtil::convertXYZRPYtoQuaternion(pose15_vec, pose15);
    ramp::motionPlanRequest r15;
    r15.pose = pose15;
    r15.via_type.data = std::string("FLY_BY");
    r15.motion_type.data = std::string("LINEAR");

	// point 16
	std::vector<double> pose16_vec(6,0.0);
	pose16_vec[0] = 0.0; pose16_vec[1] = -0.6;  pose16_vec[2] = 0.4;
	pose16_vec[3] = 0.; pose16_vec[4] = 1.57; pose16_vec[5] = 0.;
	geometry_msgs::Pose pose16;
	rampUtil::convertXYZRPYtoQuaternion(pose16_vec, pose16);
    ramp::motionPlanRequest r16;
    r16.pose = pose16;
    r16.via_type.data = std::string("FLY_BY");
    r16.motion_type.data = std::string("LINEAR");

	// point 17
	std::vector<double> pose17_vec(6,0.0);
	pose17_vec[0] = 0.0; pose17_vec[1] = 0.4;  pose17_vec[2] = 0.5;
	pose17_vec[3] = 0.0; pose17_vec[4] = 1.57; pose17_vec[5] = 0.0;
	geometry_msgs::Pose pose17;
	rampUtil::convertXYZRPYtoQuaternion(pose17_vec, pose17);
    ramp::motionPlanRequest r17;
    r17.pose = pose17;
    r17.via_type.data = std::string("FLY_BY");
    r17.motion_type.data = std::string("FREE");

	// point 18
	std::vector<double> pose18_vec(6,0.0);
	pose18_vec[0] = 0.0; pose18_vec[1] = 0.5;  pose18_vec[2] = 0.5;
	pose18_vec[3] = 0.0; pose18_vec[4] = 1.57;  pose18_vec[5] = 0.0;
	geometry_msgs::Pose pose18;
	rampUtil::convertXYZRPYtoQuaternion(pose18_vec, pose18);
    ramp::motionPlanRequest r18;
    r18.pose = pose18;
    r18.via_type.data = std::string("FLY_BY");
    r18.motion_type.data = std::string("LINEAR");;

	/* arc */
	// point 19
	std::vector<double> pose19_vec(6,0.0);
	pose19_vec[0] = 0.2; pose19_vec[1] = 0.2; pose19_vec[2] = 0.3;
	pose19_vec[3] = 0.0; pose19_vec[4] = 0.0; pose19_vec[5] = 0.0;
	geometry_msgs::Pose pose19;
	rampUtil::convertXYZRPYtoQuaternion(pose19_vec, pose19);
    ramp::motionPlanRequest r19;
    r19.pose = pose19;
    r19.via_type.data = std::string("FLY_BY");
    r19.motion_type.data = std::string("FREE");

	// point 20
	std::vector<double> pose20_vec(6,0.0);
	pose20_vec[0] = 0.2; pose20_vec[1] = 0.1;  pose20_vec[2] = 0.4;
	pose20_vec[3] = 0.0; pose20_vec[4] = 0.0;  pose20_vec[5] = 0.0;
	geometry_msgs::Pose pose20;
	rampUtil::convertXYZRPYtoQuaternion(pose20_vec, pose20);
    ramp::motionPlanRequest r20;
    r20.pose = pose20;
    r20.via_type.data = std::string("FLY_BY");
    r20.motion_type.data = std::string("ARC");

	// point 21
	std::vector<double> pose21_vec(6,0.0);
	pose21_vec[0] = 0.2; pose21_vec[1] = 0.2; pose21_vec[2] = 0.5;
	pose21_vec[3] = 0.0; pose21_vec[4] = 0.0; pose21_vec[5] = 0.0;
	geometry_msgs::Pose pose21;
	rampUtil::convertXYZRPYtoQuaternion(pose21_vec, pose21);
    ramp::motionPlanRequest r21;
    r21.pose = pose21;
    r21.via_type.data = std::string("FLY_BY");
    r21.motion_type.data = std::string("ARC");

	// point 22
	std::vector<double> pose22_vec(6,0.0);
	pose22_vec[0] = 0.3; pose22_vec[1] = 0.0;  pose22_vec[2] = 0.4;
	pose22_vec[3] = 0.0; pose22_vec[4] = 1.57; pose22_vec[5] = 0.0;
	geometry_msgs::Pose pose22;
	rampUtil::convertXYZRPYtoQuaternion(pose22_vec, pose22);
    ramp::motionPlanRequest r22;
    r22.pose = pose22;
    r22.via_type.data = std::string("FLY_BY");
    r22.motion_type.data = std::string("FREE");

    // point 23
	std::vector<double> pose23_vec(6,0.0);
	pose23_vec[0] = 0.7; pose23_vec[1] = 0.0;  pose23_vec[2] = 0.7;
	pose23_vec[3] = 0.0; pose23_vec[4] = 1.57; pose23_vec[5] = 0.0;
	geometry_msgs::Pose pose23;
	rampUtil::convertXYZRPYtoQuaternion(pose23_vec, pose23);
    ramp::motionPlanRequest r23;
    r23.pose = pose23;
    r23.via_type.data = std::string("FLY_BY");
    r23.motion_type.data = std::string("FREE");

    // point 24
    std::vector<double> pose24_vec(6,0.0);
	pose24_vec[0] = 0.7; pose24_vec[1] = 0.0;  pose24_vec[2] = 0.4;
	pose24_vec[3] = 0.0; pose24_vec[4] = 1.57; pose24_vec[5] = 0.0;
	geometry_msgs::Pose pose24;
	rampUtil::convertXYZRPYtoQuaternion(pose24_vec, pose24);
    ramp::motionPlanRequest r24;
    r24.pose = pose24;
    r24.via_type.data = std::string("FLY_BY");
    r24.motion_type.data = std::string("LINEAR");

    /* service tests */
    /* add tool */
    // ramp::addTool at;
    // at.request.toolx_pose_wrt_tool0 = quat_from_toolx_tool0;
    // at.request.path_to_toolx_mesh = tool_path;
    // add_tool_client.call(at);
    // sleep(2.0);

    // /* add object */
    // ramp::addAttachedObject ao;
    // ao.request.object_pose_wrt_toolx = quat_from_attached_obj_to_toolx;
    // ao.request.path_to_object_mesh = obj_path;
    // add_object_client.call(ao);
    // sleep(2.0);

    // // /* add point cloud */
    // ramp::addPointCloudToScene ap;
    // ap.request.camera_pose_wrt_robot_base = quat_from_camera_to_robot_base;
    // ap.request.path_to_point_cloud = pcl_path;
    // load_point_cloud_client.call(ap);
    // sleep(2.0);

    // /* remove object */
    // ramp::removeAttachedObject ro;
    // remove_object_client.call(ro);
    // sleep(2.0);

    // /* remove tool */
    // ramp::removeTool rt;
    // remove_tool_client.call(rt);
    // sleep(2.0);

    /* generate motion plan */
    int robot_ver = 1; // 1-irb120 2-irb1660
    ramp::motionPlanRequestArray pr;
    switch(robot_ver)
    {
        case 1:
        {
            pr.motion_plan_requests.push_back(r22);
            pr.motion_plan_requests.push_back(r12);
            pr.motion_plan_requests.push_back(r13);
            pr.motion_plan_requests.push_back(r14);
            pr.motion_plan_requests.push_back(r15);
            pr.motion_plan_requests.push_back(r16);
            pr.motion_plan_requests.push_back(r17);
            pr.motion_plan_requests.push_back(r18);
            pr.motion_plan_requests.push_back(r19);
            pr.motion_plan_requests.push_back(r20);
            pr.motion_plan_requests.push_back(r21);
            pr.motion_plan_requests.push_back(r5);
            break;
        }
        case 2:
        {
            pr.motion_plan_requests.push_back(r23);
            pr.motion_plan_requests.push_back(r24);
            break;
        }
    }
    ramp::generateMotionPlan gmp;
    gmp.request.plan_request = pr;
    gmp.request.continue_on_unreachable.data = true;
    gmp.request.output_key_cfgs.data = false;

    generate_mp_client.call(gmp);
    ROS_INFO("response: %zd via points reachable", gmp.response.reachable_via_pnts_inds.data.size());
    std::cout << "[";
	for (int i=0; i<gmp.response.reachable_via_pnts_inds.data.size(); ++i)
		std::cout << " " << gmp.response.reachable_via_pnts_inds.data[i];
	std::cout << "]" << std::endl;

    ROS_INFO("response: whole trajectory has %zd way points", gmp.response.trajectory.points.size());
    ROS_INFO("response: whole trajectory has %zd trajecoty segments", gmp.response.trajectory_segments.size());
    for (int i=0; i<gmp.response.trajectory_segments.size(); ++i)
        ROS_INFO("response: trajectory segment %d has %zd way points", i, gmp.response.trajectory_segments[i].points.size());

    return 0;
}