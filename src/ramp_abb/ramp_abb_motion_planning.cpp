#include "ramp_abb/ramp_abb_motion_planning.h" 
extern rampParameters ramp_params;
extern statistics stats;

motionViaPoint::motionViaPoint(const geometry_msgs::Pose &eef_pose, const via_type &point_via_type, 
							   const motion_type &point_motion_type, bool point_collison_check)
{
	pose = eef_pose;
	v_type = point_via_type;
	m_type = point_motion_type;
	collision_check = point_collison_check;
}

rampABBmotionPlanning::rampABBmotionPlanning(const std::string move_group_name, const std::string node_space)
: move_group_name_(move_group_name), move_group(move_group_name),node_space_(node_space) , spinner(4), planning_graph(), arc_solver(), tf_listener()
{
	ROS_INFO("*********************************************************");
	ROS_INFO("ABB Motion Planning Development at USCRC");
	ROS_INFO("*********************************************************");
	loadParameters();
	planning_scene_monitor = planning_scene_monitor::PlanningSceneMonitorPtr(new planning_scene_monitor::PlanningSceneMonitor("robot_description"));
	planning_scene_diff_publisher = nh.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
	object_octo_publisher = nh.advertise<pcl::PointCloud<pcl::PointXYZRGB> >("/move_group/depth_registered/points", 100);
 	display_pub = nh.advertise<moveit_msgs::DisplayTrajectory>("/move_group/abb/display_planned_path", 100, true);
 	joint_cfg_pub = nh.advertise<sensor_msgs::JointState>("/move_group/fake_controller_joint_states", 100);  
	visual_tools.reset(new moveit_visual_tools::MoveItVisualTools("panda_link0"));
	node_id = 0;
	continue_on_unreachble = true;
	is_tool_attached = false;
	tf_from_toolx_to_tool0 = Eigen::Matrix3d::Identity();
	is_object_attached = false;
	tf_from_attached_object_to_toolx = Eigen::Matrix3d::Identity();
	only_output_key_cfgs = false;

	/* perception based motion planning ros services */
	add_tool_srv = ros::NodeHandle().advertiseService("abb_add_tool", &rampABBmotionPlanning::addToolSrv, this);
	remove_tool_srv = ros::NodeHandle().advertiseService("abb_remove_tool", &rampABBmotionPlanning::removeToolSrv, this);
	add_attached_object_srv = ros::NodeHandle().advertiseService("abb_add_attached_object", &rampABBmotionPlanning::addAttachedObjectSrv, this);
	remove_attached_object_srv = ros::NodeHandle().advertiseService("abb_remove_attached_object", &rampABBmotionPlanning::removeAttachedObjectSrv, this);
	load_point_cloud_srv = ros::NodeHandle().advertiseService("abb_load_point_cloud", &rampABBmotionPlanning::addPointCloudToSceneSrv, this);
	generate_mp_srv = ros::NodeHandle().advertiseService("abb_generate_motion_plan", &rampABBmotionPlanning::generateMotionPlanSrv, this);

	initialize();
}

void rampABBmotionPlanning::loadParameters()
{
	if(nh.hasParam("/ramp/arm_joint_group_name")) 
	{
		nh.getParam("/ramp/arm_joint_group_name", ramp_params.arm_joint_group_name);
		std::cout <<"\narm_joint_group_name: "<< ramp_params.arm_joint_group_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_joint_group_name");

	if(nh.hasParam("/ramp/arm_joint_names")) 
	{
		nh.getParam("/ramp/arm_joint_names", ramp_params.arm_joint_names);
		std::cout <<"\narm_joint_names: ";
		for (int i=0; i<ramp_params.arm_joint_names.size(); i++)
			std::cout << ramp_params.arm_joint_names.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_joint_names");
	
	if(nh.hasParam("/ramp/arm_eef_link_name")) 
	{
		nh.getParam("/ramp/arm_eef_link_name", ramp_params.arm_eef_link_name);
		std::cout <<"\narm_eef_link_name: "<< ramp_params.arm_eef_link_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_eef_link_name");

	if(nh.hasParam("/ramp/verbose")) 
	{
		nh.getParam("/ramp/verbose", ramp_params.verbose);
		std::cout <<"\nverbose: "<< ramp_params.verbose;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/verbose");
}

void rampABBmotionPlanning::initialize()
{
	/*
		Initialize kinematic setting
		Load in robot kinematics 
	*/
	ROS_INFO("Loading robot kinematics...");
	ramp_setting_ptr = boost::make_shared<rampSetting>("");
	ramp_setting_ptr->printJointBoundsInfo();
  	spinner.start();
	joint_model_group = move_group.getCurrentState()->getJointModelGroup(move_group_name_);
}

void rampABBmotionPlanning::setFlagContinueOnUnreachable(bool cont_unreach)
{
	continue_on_unreachble = cont_unreach;
}

void rampABBmotionPlanning::setOutputKeyCfgsFlag(bool output_key_flag)
{
	only_output_key_cfgs = output_key_flag;
}

void rampABBmotionPlanning::setViaPoints(const std::vector<motionViaPoint> &via_points_specs)
{
	via_pnts_specified = via_points_specs;
}

void rampABBmotionPlanning::clearViaPoints()
{
	via_pnts_specified.clear();
}

void rampABBmotionPlanning::setTfFromToolXToTool0(const Eigen::Affine3d &tf)
{
	tf_from_toolx_to_tool0 = tf;
}

void rampABBmotionPlanning::setTfFromAttachedObjectToToolx(const Eigen::Affine3d &tf)
{
	tf_from_attached_object_to_toolx = tf;
}

bool rampABBmotionPlanning::linearPathEvaluation(const geometry_msgs::Pose &start_eef_pose, const geometry_msgs::Pose &target_eef_pose,
												 std::vector<moveit_msgs::RobotTrajectory> &trajectories, bool include_j6_ik)
{
	/* 
		Step 1
		Get all the IK solutions for the starting end effector pose
	*/
	std::vector<armCfg> ik_sols;
	if (include_j6_ik)
		ramp_setting_ptr->getAllIKJointConfigurationsIncludingLastJointTurns(ik_sols, start_eef_pose);
	else
		ramp_setting_ptr->getAllIKJointConfigurations(ik_sols, start_eef_pose);
	if (ramp_params.verbose)
	{
		ROS_INFO("Starting eef pose is:");
		rampUtil::printGeoMsgPose(start_eef_pose);
		ROS_INFO("%zd IK solutions found for the starting eef pose", ik_sols.size());
		ROS_INFO("These solutions are:");
		for (int i=0; i<ik_sols.size(); ++i)
		{
			ROS_INFO("IK solution %d:", i);
			rampUtil::printArmCfg(ik_sols.at(i));
		}
	}
	/* 
		Step 2
		Evaluate the starting configurations with Jacobian-based motion generation
	*/
	std::vector<bool> ik_sol_valid(ik_sols.size(), false);
	std::vector<int> error_codes(ik_sols.size(), -1);
	int num_of_succ = 0;
	for (int i=0; i<ik_sols.size(); ++i)
	{
		/* 
			Step 2A
			show arm at the start cfg
		*/
		ROS_INFO("Evaluating IK solution %d...", i);
		/* 
			Step 2B
			move eef on a straight line with selected direction
		*/
		ROS_INFO("Generating linear segment...");
		moveit_msgs::RobotTrajectory traj;
		if (jacobianBasedMotionGeneration(ik_sols.at(i), target_eef_pose, 1e2, error_codes.at(i), traj))
		{
			trajectories.push_back(traj);
			ik_sol_valid.at(i) = true;
			++num_of_succ;
		}
		else
		{
			ROS_WARN("IK solution %d failed to reach target eef pose with straight line motion", i);
			ik_sol_valid.at(i) = false;
		}
	}
	/* 
		Step 3
		Report results
	*/
	ROS_INFO("*****************************");
	ROS_INFO("Summary");
	ROS_INFO("*****************************");
	ROS_INFO("Start eef pose:");
	rampUtil::printGeoMsgPose(start_eef_pose);
	ROS_INFO("Target eef pose:");
	rampUtil::printGeoMsgPose(target_eef_pose);
	ROS_INFO("%d/%zd IK solutions are feasible to reach the target pose.", num_of_succ, ik_sols.size());
	ROS_INFO("Feasible solutions:");
	for (int i=0; i<ik_sols.size(); ++i)
	{
		if (ik_sol_valid.at(i))
		{
			ROS_INFO("IK solution %d:", i);
			rampUtil::printArmCfg(ik_sols.at(i));
		}
	}
	ROS_INFO("Infeasible solutions:");
	for (int i=0; i<ik_sols.size(); ++i)
	{
		if (!ik_sol_valid.at(i))
		{
			ROS_INFO("IK solution %d:", i);
			rampUtil::printArmCfg(ik_sols.at(i));
			ROS_INFO("Reason:");
			switch(error_codes.at(i))
			{
				case 1:
				{
					ROS_INFO("Close to singularity.");
					break;
				}
				case 2:
				{
					ROS_INFO("Joint limits reached.");
					break;
				}
			}
		}
	}
	if (num_of_succ != 0)
		return true;
	else
		return false;
}

bool rampABBmotionPlanning::jacobianBasedMotionGeneration(const armCfg &start_cfg, const tf::Vector3& eef_moving_dir_in_base_link,
														  const int &num_of_steps, int &error_code, moveit_msgs::RobotTrajectory &trajectory,
														  double acc_scale_factor, double vel_scale_factor)
{
	/* 
		Step 1
		Compute positional deviation w.r.t the target eef pose 
	*/
	geometry_msgs::Pose start_pose;
	ramp_setting_ptr->setJointConfiguration(start_cfg);
	ramp_setting_ptr->getEefPose(start_pose);
	Eigen::MatrixXd delta_e(6,1);
	delta_e << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

	/*
		Step 2
		determine Catesian motion step
	*/
	delta_e(0,0) = 0.001 * eef_moving_dir_in_base_link.x();
	delta_e(1,0) = 0.001 * eef_moving_dir_in_base_link.y();
	delta_e(2,0) = 0.001 * eef_moving_dir_in_base_link.z();

	/* 
		Step 4
		Motion generation
	 */
	armCfg arm_cur_cfg = start_cfg;
	Eigen::MatrixXd jacobian, delta_theta, pseudo_inv;
	geometry_msgs::Pose cur_pose;
	int time_step = 0;

	trajectory_msgs::JointTrajectoryPoint start_point;
	start_point.positions = start_cfg;
	trajectory.joint_trajectory.joint_names = ramp_setting_ptr->getJointNames();
	trajectory.joint_trajectory.points.push_back(start_point);

	while ( (time_step<num_of_steps) && ros::ok() )
	{
		jacobian = ramp_setting_ptr->getJacobian(arm_cur_cfg);
		/* check singularity */
		int method = 2;
		switch(method)
		{
			case 1:
			{
				Eigen::FullPivLU<Eigen::MatrixXd> luA(jacobian);
				int rank = luA.rank();
				if (rank != 6)
				{
					ROS_WARN("Singularity[Jacobian rank %d] occured at the following cfg", rank);
					rampUtil::printArmCfg(arm_cur_cfg);
					error_code = 1;
					return false;
				}
				break;
			}
			case 2:
			{
				double manipulability = ramp_setting_ptr->getManipulabilityIndex(arm_cur_cfg);
				if (manipulability < 0.001)
				{
					ROS_WARN("Singularity[Manipulability %f] occured at the following cfg", manipulability);
					rampUtil::printArmCfg(arm_cur_cfg);
					error_code = 1;
					return false;
				}
				break;
			}
		}

		/* generate joint motion step */
		pseudo_inv = (jacobian.transpose())*((jacobian*(jacobian.transpose())).inverse());
		delta_theta = pseudo_inv*delta_e;
		for(int i=0; i<arm_cur_cfg.size(); ++i)
			arm_cur_cfg[i] = arm_cur_cfg[i] + delta_theta(i,0);

		/* check joint limits */
		if (!ramp_setting_ptr->isInJointAngleRanges(arm_cur_cfg))
		{
			ROS_WARN("Joint out of the range at the following cfg");
			rampUtil::printArmCfg(arm_cur_cfg);
			error_code = 2;
			return false;
		}

		/* check collision with scene object */
		planning_scene_monitor->requestPlanningSceneState(); 
		planning_scene_monitor::LockedPlanningSceneRO ps(planning_scene_monitor);
		if (ramp_setting_ptr->isRobotColliding(arm_cur_cfg, ps))
		{
			ROS_WARN("Collision/In contact at the following cfg");
			rampUtil::printArmCfg(arm_cur_cfg);
			contact_cfgs.push_back(arm_cur_cfg);
			error_code = 3;
			break; // instead of "return false", we still return path until collision
		}
		
		/* update new configuration in rviz */
		// renderJointConfiguration(arm_cur_cfg, ramp_setting_ptr->getJointNames());
		
		/* push this cfg to trajecoty */
		trajectory_msgs::JointTrajectoryPoint point;
		point.positions = arm_cur_cfg;
		trajectory.joint_trajectory.points.push_back(point);
		++time_step;
	}

	/* Time parameterize the path */
	robot_state::RobotStatePtr kine_state = robot_state::RobotStatePtr(new robot_state::RobotState(ramp_setting_ptr->getRobotModelPtr()));
	kine_state->setToRandomPositions();

	robot_trajectory::RobotTrajectory rob_traj(ramp_setting_ptr->getRobotModelPtr(), ramp_setting_ptr->getJointModelGroupName());
	rob_traj.setRobotTrajectoryMsg(*kine_state, trajectory);

	trajectory_processing::IterativeParabolicTimeParameterization iptp;
	double max_vel_scaling = vel_scale_factor;
	double max_acc_scaling = acc_scale_factor;
	if (iptp.computeTimeStamps(rob_traj, max_vel_scaling, max_acc_scaling))
	{
		ROS_INFO("Time parameterization of the path is successful.");
		rob_traj.getRobotTrajectoryMsg(trajectory);
	}
	else
		ROS_ERROR("Failed to time parameterize the path.");

	// ramp_setting_ptr->setJointConfiguration(arm_cur_cfg);
	// ramp_setting_ptr->getEefPose(cur_pose);
	// ROS_INFO("Final reached pose is ");
	// rampUtil::printGeoMsgPose(cur_pose);
	// if (rampUtil::areSamePositions(cur_pose, target_eef_pose))
	// {
	// 	ROS_INFO("Target eef pose reached via Jacobian motion genereation.");
	// 	error_code = 0;
	// 	return true;
	// }
	// else
	// 	ROS_WARN("Number of motion steps has been exhausted but target eef pose is not reached.");
}

bool rampABBmotionPlanning::jacobianBasedMotionGeneration(const armCfg &start_cfg, const geometry_msgs::Pose &target_eef_pose, const int &num_of_steps, int &error_code, moveit_msgs::RobotTrajectory &trajectory)
{
	/* 
		Step 1
		Compute positional deviation w.r.t the target eef pose 
	*/
	geometry_msgs::Pose start_pose;
	ramp_setting_ptr->setJointConfiguration(start_cfg);
	ramp_setting_ptr->getEefPose(start_pose);
	Eigen::MatrixXd delta_e(6,1);
	delta_e << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

	/*
		Step 2
		determine Catesian motion step
	*/
	delta_e(0,0) = (target_eef_pose.position.x - start_pose.position.x) / (double) num_of_steps;
	delta_e(1,0) = (target_eef_pose.position.y - start_pose.position.y) / (double) num_of_steps;
	delta_e(2,0) = (target_eef_pose.position.z - start_pose.position.z) / (double) num_of_steps;

	/* 
		Step 3
		Check if start pose is already close to the target pose
	*/
	if (rampUtil::areSamePositions(start_pose, target_eef_pose))
	{
		ROS_WARN("Starting eef pose is already very close to the target eef pose.");
		error_code = 0;
		return true;
	}

	/* 
		Step 4
		Motion generation
	 */
	armCfg arm_cur_cfg = start_cfg;
	Eigen::MatrixXd jacobian, delta_theta, pseudo_inv;
	geometry_msgs::Pose cur_pose;
	int time_step = 0;

	trajectory_msgs::JointTrajectoryPoint start_point;
	start_point.positions = start_cfg;
	trajectory.joint_trajectory.points.push_back(start_point);

	while ( (time_step<num_of_steps) && ros::ok() )
	{
		jacobian = ramp_setting_ptr->getJacobian(arm_cur_cfg);
		/* check singularity */
		int method = 2;
		switch(method)
		{
			case 1:
			{
				Eigen::FullPivLU<Eigen::MatrixXd> luA(jacobian);
				int rank = luA.rank();
				if (rank != 6)
				{
					ROS_WARN("Singularity[Jacobian rank %d] occured at the following cfg", rank);
					rampUtil::printArmCfg(arm_cur_cfg);
					error_code = 1;
					return false;
				}
				break;
			}
			case 2:
			{
				double manipulability = ramp_setting_ptr->getManipulabilityIndex(arm_cur_cfg);
				if (manipulability < 0.001)
				{
					ROS_WARN("Singularity[Manipulability %f] occured at the following cfg", manipulability);
					rampUtil::printArmCfg(arm_cur_cfg);
					error_code = 1;
					return false;
				}
				break;
			}
		}

		/* generate joint motion step */
		pseudo_inv = (jacobian.transpose())*((jacobian*(jacobian.transpose())).inverse());
		delta_theta = pseudo_inv*delta_e;
		for(int i=0; i<arm_cur_cfg.size(); ++i)
			arm_cur_cfg[i] = arm_cur_cfg[i] + delta_theta(i,0);

		/* check joint limits */
		if (!ramp_setting_ptr->isInJointAngleRanges(arm_cur_cfg))
		{
			ROS_WARN("Joint out of the range at the following cfg");
			rampUtil::printArmCfg(arm_cur_cfg);
			error_code = 2;
			return false;
		}

		/* check collision with scene object */
		planning_scene_monitor->requestPlanningSceneState(); 
		planning_scene_monitor::LockedPlanningSceneRO ps(planning_scene_monitor);
		if (ramp_setting_ptr->isRobotColliding(arm_cur_cfg, ps))
		{
			ROS_WARN("Collision/In contact at the following cfg");
			rampUtil::printArmCfg(arm_cur_cfg);
			contact_cfgs.push_back(arm_cur_cfg);
			error_code = 3;
			break; // instead of "return false", we still return path until collision
		}
		
		/* update new configuration in rviz */
		// renderJointConfiguration(arm_cur_cfg, ramp_setting_ptr->getJointNames());
		
		/* push this cfg to trajecoty */
		trajectory_msgs::JointTrajectoryPoint point;
		point.positions = arm_cur_cfg;
		trajectory.joint_trajectory.points.push_back(point);
		++time_step;
	}
	ramp_setting_ptr->setJointConfiguration(arm_cur_cfg);
	ramp_setting_ptr->getEefPose(cur_pose);
	if (rampUtil::areSamePositions(cur_pose, target_eef_pose))
	{
		ROS_INFO("Target eef pose reached via Jacobian motion genereation.");
		error_code = 0;
		return true;
	}
	else
		ROS_WARN("Number of motion steps has been exhausted but target eef pose is not reached.");
}

bool rampABBmotionPlanning::arcPathEvaluation(const geometry_msgs::Pose &start_eef_pose,  const geometry_msgs::Pose &mid_eef_pose, const geometry_msgs::Pose &target_eef_pose,
							   				  std::vector<moveit_msgs::RobotTrajectory> &trajectories)
{
	/* 
		Step 1
		get all the IK solutions
	 */
	std::vector<armCfg> ik_sols;
	ramp_setting_ptr->getAllIKJointConfigurationsIncludingLastJointTurns(ik_sols, start_eef_pose);
	if (ramp_params.verbose)
	{
		ROS_INFO("%zd IK solutions found for the starting eef pose", ik_sols.size());
		ROS_INFO("These solutions are:");
		for (int i=0; i<ik_sols.size(); ++i)
		{
			ROS_INFO("IK solution %d:", i);
			rampUtil::printArmCfg(ik_sols.at(i));
		}
	}
	/* 
		Step 2
		Evaluate the starting configurations with Jacobian-based motion generation
	*/
	std::vector<bool> ik_sol_valid(ik_sols.size(), false);
	std::vector<int> error_codes(ik_sols.size(), -1);
	int num_of_succ = 0;
	for (int i=0; i<ik_sols.size(); ++i)
	{
		/* 
			Step 2A
			show arm at the start cfg
		*/
		ROS_INFO("Evaluating IK solution %d...", i);
		/* 
			Step 2B
			move eef on a straight line with selected direction
		*/
		moveit_msgs::RobotTrajectory traj;
		traj.joint_trajectory.joint_names = ramp_setting_ptr->getJointNames(); // remember to add names!
		if (moveAlongArc(ik_sols.at(i), mid_eef_pose, target_eef_pose, error_codes.at(i), traj))
		{
			ROS_INFO("IK solution %d reached target eef pose", i);
			trajectories.push_back(traj);
			ik_sol_valid.at(i) = true;
			++num_of_succ;
		}
		else
		{
			ROS_WARN("IK solution %d failed to reach target eef pose with an arc path", i);
			ik_sol_valid.at(i) = false;
		}
	}
	/* 
		Step 3
		Report results
	*/
	ROS_INFO("*****************************");
	ROS_INFO("Summary");
	ROS_INFO("*****************************");
	ROS_INFO("Start eef pose:");
	rampUtil::printGeoMsgPose(start_eef_pose);
	ROS_INFO("Target eef pose:");
	rampUtil::printGeoMsgPose(target_eef_pose);
	ROS_INFO("%d/%zd IK solutions are feasible to reach the target pose.", num_of_succ, ik_sols.size());
	ROS_INFO("Feasible solutions:");
	for (int i=0; i<ik_sols.size(); ++i)
	{
		if (ik_sol_valid.at(i))
		{
			ROS_INFO("IK solution %d:", i);
			rampUtil::printArmCfg(ik_sols.at(i));
		}
	}
	ROS_INFO("Infeasible solutions:");
	for (int i=0; i<ik_sols.size(); ++i)
	{
		if (!ik_sol_valid.at(i))
		{
			ROS_INFO("IK solution %d:", i);
			rampUtil::printArmCfg(ik_sols.at(i));
			ROS_INFO("Reason:");
			switch(error_codes.at(i))
			{
				case 1:
				{
					ROS_INFO("Close to singularity.");
					break;
				}
				case 2:
				{
					ROS_INFO("Joint limits reached.");
					break;
				}
			}
		}
	}
	if (num_of_succ != 0)
		return true;
	else
		return false;
}

bool rampABBmotionPlanning::moveAlongArc(const armCfg &start_cfg, const geometry_msgs::Pose &mid_pnt, const geometry_msgs::Pose &final_pnt, int &error_code, moveit_msgs::RobotTrajectory &trajectory)
{
	geometry_msgs::Pose start_pnt;
	ramp_setting_ptr->setJointConfiguration(start_cfg);
	ramp_setting_ptr->getEefPose(start_pnt);
	Eigen::MatrixXd delta_e(6,1);
	delta_e << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

	/*
		Step 1 
		determine circle center and radius
		interpolate to get intermediate points
	 */
	std::vector<Eigen::Vector3f> pnts_on_circle;
	Eigen::Vector3f s, m, f;
	s[0] = start_pnt.position.x; s[1] = start_pnt.position.y; s[2] = start_pnt.position.z;
	m[0] = mid_pnt.position.x;   m[1] = mid_pnt.position.y;   m[2] = mid_pnt.position.z;
	f[0] = final_pnt.position.x; f[1] = final_pnt.position.y; f[2] = final_pnt.position.z;
	if(arc_solver.solveArc(s, m, f))
	{
		/* 
			Step 2
			interpolate points on a circle
		 */
		double cur_t = 0.0;
		double t_incre = 0.1;
		while (cur_t < 1.0)
		{
			Eigen::Vector3f tmp = arc_solver.arc(cur_t);
			pnts_on_circle.push_back(tmp);
			cur_t += t_incre;
		}

		/* 
			Step 3
			generate joint path
		*/
		armCfg s_cfg = start_cfg;
		std::vector<moveit_msgs::RobotTrajectory> traj_segs;
		for(int i=1; i<pnts_on_circle.size(); ++i)
		{
			geometry_msgs::Pose tmp_tgt_pose;
			tmp_tgt_pose.orientation = mid_pnt.orientation;
			tmp_tgt_pose.position.x = pnts_on_circle[i][0];
			tmp_tgt_pose.position.y = pnts_on_circle[i][1];
			tmp_tgt_pose.position.z = pnts_on_circle[i][2];
			moveit_msgs::RobotTrajectory traj;
			ROS_INFO("Generating arc segment...");
			if (jacobianBasedMotionGeneration(s_cfg, tmp_tgt_pose, 1e1, error_code, traj))
			{
				traj_segs.push_back(traj);
				s_cfg = traj.joint_trajectory.points.back().positions;
			}
			else
			{
				ROS_WARN("Failed to generate arc segment to pnt %d", i);
				return false;
			}
		}
		appendTrajectories(trajectory, traj_segs);
		return true;
	}
	else
	{
		ROS_ERROR("Arc does not exist");
		return false;
	}
}

void rampABBmotionPlanning::convertRobotCfgToABBCfg(const armCfg &robot_cfg, abbCfg &abb_cfg)
{
	abb_cfg.clear();
	for (int i=0; i<robot_cfg.size(); ++i)
	{
		// for 6-axis robot only now
		if ( i==0 || i==3 || i==5)
			abb_cfg.push_back((int)std::floor(robot_cfg.at(i)/90.));
	}
	abb_cfg.push_back(0); //dummy for 4th value(external axis)
	ROS_INFO("abb cfg is [%d, %d, %d, %d]", abb_cfg[0], abb_cfg[1], abb_cfg[2], abb_cfg[3]);
}

void rampABBmotionPlanning::getRandomValidEefPose(geometry_msgs::Pose &eef_pose)
{
	ramp_setting_ptr->setRandomJointConfiguration();
	ramp_setting_ptr->getEefPose(eef_pose);
}

void rampABBmotionPlanning::loadPointCloud(const Eigen::Affine3d &tf_from_camera_to_robot_base, const std::string &path)
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcd1(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_pcd1(new pcl::PointCloud<pcl::PointXYZRGB>);
	pcl::io::loadPCDFile<pcl::PointXYZRGB>(path.c_str(), *pcd1);

	Eigen::Matrix4f tf_cameraLink_to_robot_base = tf_from_camera_to_robot_base.matrix().cast<float>();
	Eigen::Matrix4f tf_cameraLink_to_cameraOptical = Eigen::Matrix4f::Identity(); //fixed tf for ros kinetic
	// tf_cameraLink_to_cameraOptical << 	0, -1, 0, -0.02,
	// 									0, 0, -1, 0,
	// 									1, 0, 0, 0,
	// 									0, 0, 0, 1;
	Eigen::Matrix4f tf_cameraOptical_to_cameraLink = tf_cameraLink_to_cameraOptical.inverse();
	Eigen::Matrix4f tf_cameraOptical_to_robot_base = tf_cameraLink_to_robot_base * tf_cameraOptical_to_cameraLink;
	pcl::transformPointCloud(*pcd1,
							 *transformed_pcd1,
							 tf_cameraOptical_to_robot_base);
	transformed_pcd1->header.frame_id = "base_link";

	object_octo_publisher.publish (transformed_pcd1);
	ROS_INFO("Point cloud published");
}

bool rampABBmotionPlanning::moveFromCurrentJntCfgToGivenPose(const armCfg& start_cfg, const geometry_msgs::Pose &goal_pose, moveit_msgs::RobotTrajectory &trajectory, 
															 double acc_scale_factor, double vel_scale_factor)
{
	move_group.setMaxAccelerationScalingFactor(acc_scale_factor);
	move_group.setMaxVelocityScalingFactor(vel_scale_factor);

	robot_state::RobotState start_state(*move_group.getCurrentState());
	joint_model_group = move_group.getCurrentState()->getJointModelGroup(move_group_name_);
	start_state.setJointGroupPositions(joint_model_group,start_cfg);
	move_group.setStartState(start_state);

	move_group.setPoseTarget(goal_pose);
	moveit::planning_interface::MoveGroupInterface::Plan plan_result;
	bool success = (move_group.plan(plan_result) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
	if (success)
	{
		// move_group.execute(plan_result);
		ROS_INFO("Planning successful.");
		trajectory = plan_result.trajectory_;
		// rampUtil::printTrajectory(trajectory);
		return true;
	}
	else
		return false;
}

void rampABBmotionPlanning::setToolBoundingSphere(const float radius)
{
	attached_tool_geometry.link_name = ramp_params.arm_eef_link_name;
	attached_tool_geometry.object.header.frame_id = ramp_params.arm_eef_link_name;
	attached_tool_geometry.object.id = "eef_bounding_sphere_1";

	geometry_msgs::Pose toolx_pose;
	rampUtil::convertAffine3dtoQuaternion(tf_from_toolx_to_tool0, toolx_pose);

	shape_msgs::SolidPrimitive primitive;
	primitive.type = primitive.SPHERE;
	primitive.dimensions.resize(1);
	primitive.dimensions[0] = radius;

	attached_tool_geometry.object.primitives.push_back(primitive);
	attached_tool_geometry.object.primitive_poses.push_back(toolx_pose);

	// Note that attaching an object to the robot requires
	// the corresponding operation to be specified as an ADD operation
	attached_tool_geometry.object.operation = attached_tool_geometry.object.ADD;
	
	// Since we are attaching the object to the robot hand to simulate picking up the object,
	// we want the collision checker to ignore collisions between the object and the robot hand
	attached_tool_geometry.touch_links = std::vector<std::string>{"tool0", "link_4", "link_5", "link_6"};
	is_tool_attached = true;
}

/* 
	parent_link_name: 
	the link that you want to attach object to, this link needs to be part of the robot with an active
	planning group in order to attach the object to this link. Otherwise it will just be a fixed connection
	and the object will not be moving as the robot moves.

	link_name: 
	the object local link name

	is_attached:
	whether you want to attach this object to the parent_link_name

	Other parameters are self-explaintory
 */
void rampABBmotionPlanning::addSceneObject( const std::string& parent_link_name, const std::string& link_name, bool is_attached,
											const std::string &path_to_mesh, const Eigen::Vector3d &mesh_scale,
											const std::string &object_id, const moveit_msgs::ObjectColor& color)
{
	scene_object_geometry.object.meshes.clear();
	scene_object_geometry.object.mesh_poses.clear();

	scene_object_geometry.link_name = parent_link_name; 
	scene_object_geometry.object.header.frame_id = parent_link_name; 
	scene_object_geometry.object.id = object_id;

	// geometry_msgs::Pose scene_object_pose;
	// rampUtil::convertAffine3dtoQuaternion(tf, scene_object_pose);

	tf::StampedTransform transform;
	tf_listener.lookupTransform(parent_link_name, link_name, ros::Time(0), transform);
	geometry_msgs::Pose scene_object_pose;
	scene_object_pose.position.x = transform.getOrigin().x();
	scene_object_pose.position.y = transform.getOrigin().y();
	scene_object_pose.position.z = transform.getOrigin().z();
	tf::quaternionTFToMsg(transform.getRotation(), scene_object_pose.orientation);

	/* load mesh */
	shapes::Mesh *mesh_shape = shapes::createMeshFromResource(path_to_mesh, mesh_scale);
	shapes::ShapeMsg mesh_msg;
	shapes::constructMsgFromShape(mesh_shape, mesh_msg);
	shape_msgs::Mesh mesh = boost::get<shape_msgs::Mesh>(mesh_msg);

	scene_object_geometry.object.meshes.push_back(mesh);
	scene_object_geometry.object.mesh_poses.push_back(scene_object_pose);
	scene_object_geometry.object.operation = scene_object_geometry.object.ADD;
	// scene_object_geometry.touch_links = std::vector<std::string>{"tool0", "link_4", "link_5", "link_6"};
	scene_object_geometry.touch_links = std::vector<std::string>{"panda_link8", "panda_link7", "panda_link6"};

	/*
		add scene object geometry/model to planning scene
	*/
	moveit_msgs::PlanningScene planning_scene;
	if (is_attached)
		planning_scene.robot_state.attached_collision_objects.push_back(scene_object_geometry);
	else
		planning_scene.world.collision_objects.push_back(scene_object_geometry.object);
	planning_scene.object_colors.push_back(color);
	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
}

void rampABBmotionPlanning::drawObjectInRviz(const std::string& parent_link_name, const std::vector<double>& pose_vec,
											const std::string &path_to_mesh, const double mesh_scale,
											const std::size_t object_id, const rviz_visual_tools::colors& color)
{
    geometry_msgs::Pose pose_quat;
    rampUtil::convertXYZRPYtoQuaternion(pose_vec, pose_quat);
    tf::Quaternion tf_quat;
    tf::quaternionMsgToTF(pose_quat.orientation, tf_quat);
    tf::Transform tf_goal_pose;
    tf_goal_pose.setOrigin(tf::Vector3(pose_quat.position.x, pose_quat.position.y, pose_quat.position.z));
    tf_goal_pose.setRotation(tf_quat);

    tf::StampedTransform tf_from_parent_link_to_base;
	tf_listener.lookupTransform("/panda_link0", parent_link_name, ros::Time(0), tf_from_parent_link_to_base);
    tf::Transform tf_goal_in_base_link = tf_from_parent_link_to_base * tf_goal_pose;

    geometry_msgs::Pose goal_pose_in_base_link;
    goal_pose_in_base_link.position.x = tf_goal_in_base_link.getOrigin().x();
	goal_pose_in_base_link.position.y = tf_goal_in_base_link.getOrigin().y();
	goal_pose_in_base_link.position.z = tf_goal_in_base_link.getOrigin().z();
    tf::quaternionTFToMsg(tf_goal_in_base_link.getRotation(), goal_pose_in_base_link.orientation);

	visual_tools->publishMesh(goal_pose_in_base_link, path_to_mesh, color, mesh_scale, "mesh", object_id);
}

void rampABBmotionPlanning::drawFrameInRviz(const std::string& parent_link_name, const std::vector<double>& pose_vec, const std::string& label)
{
	geometry_msgs::Pose pose_quat;
    rampUtil::convertXYZRPYtoQuaternion(pose_vec, pose_quat);
    tf::Quaternion tf_quat;
    tf::quaternionMsgToTF(pose_quat.orientation, tf_quat);
    tf::Transform tf_goal_pose;
    tf_goal_pose.setOrigin(tf::Vector3(pose_quat.position.x, pose_quat.position.y, pose_quat.position.z));
    tf_goal_pose.setRotation(tf_quat);

    tf::StampedTransform tf_from_parent_link_to_base;
	tf_listener.lookupTransform("/panda_link0", parent_link_name, ros::Time(0), tf_from_parent_link_to_base);
    tf::Transform tf_goal_in_base_link = tf_from_parent_link_to_base * tf_goal_pose;

    geometry_msgs::Pose goal_pose_in_base_link;
    goal_pose_in_base_link.position.x = tf_goal_in_base_link.getOrigin().x();
	goal_pose_in_base_link.position.y = tf_goal_in_base_link.getOrigin().y();
	goal_pose_in_base_link.position.z = tf_goal_in_base_link.getOrigin().z();
    tf::quaternionTFToMsg(tf_goal_in_base_link.getRotation(), goal_pose_in_base_link.orientation);

	visual_tools->publishAxis(goal_pose_in_base_link);
	// visual_tools->publishAxisLabeled(goal_pose_in_base_link, label);
}

void rampABBmotionPlanning::drawRobotArmInRviz()
{
	std::vector<double> pose_vec(6, 0.0); //identity transformation
	drawObjectInRviz("base_link", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/base_link.stl", 1.0, 10, rviz_visual_tools::colors::WHITE);
	drawObjectInRviz("link_1", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/link_1.stl", 1.0, 11, rviz_visual_tools::colors::WHITE);
	drawObjectInRviz("link_2", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/link_2.stl", 1.0, 12, rviz_visual_tools::colors::WHITE);
	drawObjectInRviz("link_3", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/link_3.stl", 1.0, 13, rviz_visual_tools::colors::WHITE);
	drawObjectInRviz("link_4", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/link_4.stl", 1.0, 14, rviz_visual_tools::colors::WHITE);
	drawObjectInRviz("link_5", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/link_5.stl", 1.0, 15, rviz_visual_tools::colors::WHITE);
	drawObjectInRviz("link_6", pose_vec, "package://abb_irb120_support/meshes/irb120_3_58/visual/link_6.stl", 1.0, 16, rviz_visual_tools::colors::WHITE);
}

void rampABBmotionPlanning::rvizVisualTrigger()
{
	visual_tools->trigger();
}

void rampABBmotionPlanning::addSceneRobotArm(const std::string &object_id)
{
	scene_object_geometry.object.meshes.clear();
	scene_object_geometry.object.mesh_poses.clear();

	scene_object_geometry.link_name = "panda_link0";
	scene_object_geometry.object.header.frame_id = "panda_link0";
	scene_object_geometry.object.id = object_id;

	createSceneObject("base_link", "package://abb_irb120_support/meshes/irb120_3_58/visual/base_link.stl");
	createSceneObject("link_1", "package://abb_irb120_support/meshes/irb120_3_58/visual/link_1.stl");
	createSceneObject("link_2", "package://abb_irb120_support/meshes/irb120_3_58/visual/link_2.stl");
	createSceneObject("link_3", "package://abb_irb120_support/meshes/irb120_3_58/visual/link_3.stl");
	createSceneObject("link_4", "package://abb_irb120_support/meshes/irb120_3_58/visual/link_4.stl");
	createSceneObject("link_5", "package://abb_irb120_support/meshes/irb120_3_58/visual/link_5.stl");
	createSceneObject("link_6", "package://abb_irb120_support/meshes/irb120_3_58/visual/link_6.stl");
	
	scene_object_geometry.object.operation = scene_object_geometry.object.ADD;
	/*
		add scene object geometry/model to planning scene
	*/
	moveit_msgs::PlanningScene planning_scene;
	planning_scene.world.collision_objects.push_back(scene_object_geometry.object);

	moveit_msgs::ObjectColor arm_color;
	arm_color.id = object_id;
	arm_color.color.r = 1.0; arm_color.color.g = 1.0;
	arm_color.color.b = 1.0; arm_color.color.a = 1.0;
	planning_scene.object_colors.push_back(arm_color);

	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
}

void rampABBmotionPlanning::createSceneObject(const std::string& link_name, const std::string& path_to_mesh)
{
	tf::StampedTransform transform;
	tf_listener.lookupTransform("/panda_link0", link_name, ros::Time(0), transform);
	geometry_msgs::Pose scene_object_pose;
	scene_object_pose.position.x = transform.getOrigin().x();
	scene_object_pose.position.y = transform.getOrigin().y();
	scene_object_pose.position.z = transform.getOrigin().z();
	tf::quaternionTFToMsg(transform.getRotation(), scene_object_pose.orientation);

	shapes::Mesh *mesh_shape = shapes::createMeshFromResource(path_to_mesh);
	shapes::ShapeMsg mesh_msg;
	shapes::constructMsgFromShape(mesh_shape, mesh_msg);
	shape_msgs::Mesh mesh = boost::get<shape_msgs::Mesh>(mesh_msg);

	scene_object_geometry.object.meshes.push_back(mesh);
	scene_object_geometry.object.mesh_poses.push_back(scene_object_pose);
}

void rampABBmotionPlanning::removeSceneObject(const std::string &object_id)
{
	if (scene_object_geometry.object.id != object_id)
	{
		ROS_ERROR("%s not exist in planning scene. Unable to remove", object_id.c_str());
		return;
	}
	scene_object_geometry.object.operation = scene_object_geometry.object.REMOVE;

	moveit_msgs::PlanningScene planning_scene;
	planning_scene.world.collision_objects.push_back(scene_object_geometry.object);
	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
	ROS_INFO("Scene object %s removed.", object_id.c_str());
}

void rampABBmotionPlanning::addTool(const Eigen::Affine3d &tf, const std::string &path_to_mesh)
{
	setTfFromToolXToTool0(tf);
	addToolMesh(path_to_mesh);
	is_tool_attached = true;
	ROS_INFO("Added toolx");
}	

void rampABBmotionPlanning::removeTool()
{
	tf_from_toolx_to_tool0 = Eigen::Affine3d::Identity();
	tf_from_toolx_to_tool0.translation() = Eigen::Vector3d(0.0,0.0,0.0);
	attached_tool_geometry.object.operation = attached_tool_geometry.object.REMOVE;

	moveit_msgs::PlanningScene planning_scene;
	planning_scene.robot_state.attached_collision_objects.push_back(attached_tool_geometry);
	planning_scene.robot_state.is_diff = true;
	planning_scene.world.collision_objects.push_back(attached_tool_geometry.object);
	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
	ROS_INFO("Attached toolx removed.");
	is_tool_attached = false;
}

void rampABBmotionPlanning::addAttachedObject(const Eigen::Affine3d &tf, const std::string &path_to_mesh)
{
	setTfFromAttachedObjectToToolx(tf);
	addAttachedObjectMesh(path_to_mesh);
	is_object_attached = true;
	ROS_INFO("Added attached object");
}

void rampABBmotionPlanning::removeAttachedObject()
{
	tf_from_attached_object_to_toolx = Eigen::Affine3d::Identity();
	tf_from_attached_object_to_toolx.translation() = Eigen::Vector3d(0.0,0.0,0.0);
	attached_object_geometry.object.operation = attached_object_geometry.object.REMOVE;

	moveit_msgs::PlanningScene planning_scene;
	planning_scene.robot_state.attached_collision_objects.push_back(attached_object_geometry);
	planning_scene.robot_state.is_diff = true;
	planning_scene.world.collision_objects.push_back(attached_object_geometry.object);
	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
	ROS_INFO("Attached object removed.");
	is_object_attached = false;
}

void rampABBmotionPlanning::addToolMesh(const std::string &path_to_mesh)
{
	attached_tool_geometry.link_name = ramp_params.arm_eef_link_name;
	attached_tool_geometry.object.header.frame_id = ramp_params.arm_eef_link_name;
	attached_tool_geometry.object.id = "tool_mesh";

	geometry_msgs::Pose toolx_pose;
	rampUtil::convertAffine3dtoQuaternion(tf_from_toolx_to_tool0, toolx_pose);

	/* load mesh */
	shapes::Mesh *mesh_shape = shapes::createMeshFromResource(path_to_mesh);
	shapes::ShapeMsg mesh_msg;
	shapes::constructMsgFromShape(mesh_shape, mesh_msg);
	shape_msgs::Mesh mesh = boost::get<shape_msgs::Mesh>(mesh_msg);

	attached_tool_geometry.object.meshes.push_back(mesh);
	attached_tool_geometry.object.mesh_poses.push_back(toolx_pose);
	attached_tool_geometry.object.operation = attached_tool_geometry.object.ADD;
	attached_tool_geometry.touch_links = std::vector<std::string>{"tool0", "link_4", "link_5", "link_6"};

	/*
		add tool geometry/model to planning scene
	*/
	moveit_msgs::PlanningScene planning_scene;
	// planning_scene.world.collision_objects.push_back(attached_object.object);
	planning_scene.robot_state.attached_collision_objects.push_back(attached_tool_geometry);
	planning_scene.robot_state.is_diff = true;
	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
}


void rampABBmotionPlanning::addAttachedObjectMesh(const std::string &path_to_mesh)
{
	attached_object_geometry.link_name = ramp_params.arm_eef_link_name;
	attached_object_geometry.object.header.frame_id = ramp_params.arm_eef_link_name;
	attached_object_geometry.object.id = "attached_object_mesh";

	geometry_msgs::Pose attached_object_pose;
	Eigen::Affine3d tf_attached_obj_to_tool0 = tf_from_toolx_to_tool0*tf_from_attached_object_to_toolx;
	rampUtil::convertAffine3dtoQuaternion(tf_attached_obj_to_tool0, attached_object_pose);

	/* load mesh */
	shapes::Mesh *mesh_shape = shapes::createMeshFromResource(path_to_mesh);
	shapes::ShapeMsg mesh_msg;
	shapes::constructMsgFromShape(mesh_shape, mesh_msg);
	shape_msgs::Mesh mesh = boost::get<shape_msgs::Mesh>(mesh_msg);

	attached_object_geometry.object.meshes.push_back(mesh);
	attached_object_geometry.object.mesh_poses.push_back(attached_object_pose);
	attached_object_geometry.object.operation = attached_object_geometry.object.ADD;
	attached_object_geometry.touch_links = std::vector<std::string>{"tool0", "link_4", "link_5", "link_6"};

	/*
		add attached object mesh to planning scene
	*/
	moveit_msgs::PlanningScene planning_scene;
	// planning_scene.world.collision_objects.push_back(attached_object.object);
	planning_scene.robot_state.attached_collision_objects.push_back(attached_object_geometry);
	planning_scene.is_diff = true;
	planning_scene_diff_publisher.publish(planning_scene);
	sleep(2.0);
}

bool rampABBmotionPlanning::moveFromJntCfgToJntCfg(const armCfg &start_cfg, const armCfg &end_cfg, moveit_msgs::RobotTrajectory &trajectory)
{
	if (is_tool_attached)
	{
		sensor_msgs::JointState joint_state;
		joint_state.header.stamp = ros::Time::now();
		joint_state.name = ramp_setting_ptr->getJointNames();
		joint_state.position = start_cfg;

		moveit_msgs::RobotState robot_state;
		robot_state.joint_state = joint_state; 
		robot_state.attached_collision_objects.push_back(attached_tool_geometry);
		if (is_object_attached)
			robot_state.attached_collision_objects.push_back(attached_object_geometry);
		robot_state.is_diff = true;
		move_group.setStartState(robot_state);
	}
	else
	{
		robot_state::RobotState start_state(*move_group.getCurrentState());
		joint_model_group = move_group.getCurrentState()->getJointModelGroup(move_group_name_);
		start_state.setJointGroupPositions(joint_model_group,start_cfg);
		move_group.setStartState(start_state);
	}

	move_group.setJointValueTarget(end_cfg);
	moveit::planning_interface::MoveGroupInterface::Plan plan_result;
	bool success = (move_group.plan(plan_result) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
	if (success)
	{
		// move_group.execute(plan_result);
		ROS_INFO("Planning successful.");
		trajectory = plan_result.trajectory_;		
		return true;
	}
	else
		return false;
}

bool rampABBmotionPlanning::moveFromJntCfgToJntCfgByInterpolation(const armCfg &start_cfg, const armCfg &end_cfg, moveit_msgs::RobotTrajectory &trajectory, double vel_scale_factor, double acc_scale_factor, int interpolation_steps)
{
	trajectory.joint_trajectory.joint_names = ramp_setting_ptr->getJointNames();
	int num_of_steps = interpolation_steps;
	armCfg tmp(6, 0.0);
	for (int i=1; i<=num_of_steps; ++i)
	{
		for (int j=0; j<6; ++j)
		{
			double prop = double(i)/double(num_of_steps);
			double step_prop = 1.0/double(num_of_steps);
			double diff = end_cfg[j]-start_cfg[j];
			if (std::abs(step_prop*diff)>1.57)
			{
				ROS_ERROR("Large joint rotation encountered during interpolation.");
				return false;
			}
			tmp[j] = start_cfg[j] + prop*diff;
		}
		trajectory_msgs::JointTrajectoryPoint pnt;
		pnt.positions = tmp;
		trajectory.joint_trajectory.points.push_back(pnt);
	}

	/* Time parameterization */
	robot_state::RobotStatePtr kine_state = robot_state::RobotStatePtr(new robot_state::RobotState(ramp_setting_ptr->getRobotModelPtr()));
	kine_state->setToRandomPositions();

	robot_trajectory::RobotTrajectory rob_traj(ramp_setting_ptr->getRobotModelPtr(), ramp_setting_ptr->getJointModelGroupName());
	rob_traj.setRobotTrajectoryMsg(*kine_state, trajectory);

	trajectory_processing::IterativeParabolicTimeParameterization iptp;
	double max_vel_scaling = vel_scale_factor;
	double max_acc_scaling = acc_scale_factor;
	if (iptp.computeTimeStamps(rob_traj, max_vel_scaling, max_acc_scaling))
	{
		ROS_INFO("Time parameterization of the path is successful.");
		rob_traj.getRobotTrajectoryMsg(trajectory);
	}
	else
		ROS_ERROR("Failed to time parameterize the path.");

	return true;
}

bool rampABBmotionPlanning::moveFromJntCfgToJntCfgByJointMotion(const armCfg &start_cfg, const armCfg &end_cfg, moveit_msgs::RobotTrajectory &trajectory,
												 				double vel_scale_factor, double acc_scale_factor)
{
	trajectory.joint_trajectory.joint_names = ramp_setting_ptr->getJointNames();
	trajectory_msgs::JointTrajectoryPoint pnt;
	pnt.positions = start_cfg;
	trajectory.joint_trajectory.points.push_back(pnt);
	pnt.positions = end_cfg;
	trajectory.joint_trajectory.points.push_back(pnt);

	/* Time parameterization */
	robot_state::RobotStatePtr kine_state = robot_state::RobotStatePtr(new robot_state::RobotState(ramp_setting_ptr->getRobotModelPtr()));
	kine_state->setToRandomPositions();

	robot_trajectory::RobotTrajectory rob_traj(ramp_setting_ptr->getRobotModelPtr(), ramp_setting_ptr->getJointModelGroupName());
	rob_traj.setRobotTrajectoryMsg(*kine_state, trajectory);

	trajectory_processing::IterativeParabolicTimeParameterization iptp;
	double max_vel_scaling = vel_scale_factor;
	double max_acc_scaling = acc_scale_factor;
	if (iptp.computeTimeStamps(rob_traj, max_vel_scaling, max_acc_scaling))
	{
		ROS_INFO("Time parameterization of the path is successful.");
		rob_traj.getRobotTrajectoryMsg(trajectory);
	}
	else
		ROS_ERROR("Failed to time parameterize the path.");

	return true;
}

bool rampABBmotionPlanning::isTrajectoryEmpty(const moveit_msgs::RobotTrajectory &trajectory)
{
	if (trajectory.joint_trajectory.points.empty())
		return true;
	else
		return false;
}

void rampABBmotionPlanning::appendTrajectories(moveit_msgs::RobotTrajectory &trajectory1, const std::vector<moveit_msgs::RobotTrajectory> &trajectories)
{
	for (int i=0; i<trajectories.size(); ++i)
	{
		trajectory1.joint_trajectory.points.insert( trajectory1.joint_trajectory.points.end(),
													trajectories[i].joint_trajectory.points.begin(),
													trajectories[i].joint_trajectory.points.end());
	}
}

void rampABBmotionPlanning::convertRobotTrajectoryToMsg(const robot_trajectory::RobotTrajectory &rob_traj,
														trajectory_msgs::JointTrajectory &traj_msg)
{
	traj_msg.points.clear();
	traj_msg.header.stamp = ros::Time::now(); 
	traj_msg.joint_names = ramp_setting_ptr->getJointNames();
	for (std::size_t i=0; i<rob_traj.getWayPointCount(); ++i)
	{
		trajectory_msgs::JointTrajectoryPoint point;
		
		std::vector<double> tmp_pos, tmp_vel, tmp_acc;
		int nDofs = 6;
		for (int j=0; j<nDofs; ++j)
		{
			tmp_pos.push_back(rob_traj.getWayPoint(i).getVariablePosition(j));
			tmp_vel.push_back(rob_traj.getWayPoint(i).getVariableVelocity(j));
			tmp_acc.push_back(rob_traj.getWayPoint(i).getVariableAcceleration(j));
		}
		point.positions = tmp_pos;
		point.velocities = tmp_vel;
		point.accelerations = tmp_acc;
		traj_msg.points.push_back(point);
	}
	double time_from_beginning = 0.0;
	std::deque<double> durs = rob_traj.getWayPointDurations();
	for (int i=0; i<traj_msg.points.size(); ++i)
	{
		time_from_beginning += durs[i];
		traj_msg.points[i].time_from_start = ros::Duration(time_from_beginning);
	}
	ROS_INFO("Trajectory type conversion finished.");
}

void rampABBmotionPlanning::visualizeMotionViaPoints(const std::vector<motionViaPoint> &motion_via_points)
{
	for(int i=0; i<motion_via_points.size(); ++i)
	{
		std::stringstream ss;
		ss << "pose " << i;
		switch (motion_via_points[i].m_type)
		{
			case FREE:
			{
				ss << " free";
				break;
			}
			case LINEAR:
			{
				ss << " linear";
				break;
			}
			case ARC:
			{
				ss << " arc";
				break;
			}
		}
		visual_tools->publishAxisLabeled(motion_via_points[i].pose, ss.str());
	}
	visual_tools->trigger();
}

void rampABBmotionPlanning::renderJointConfiguration(const armCfg &cfg)
{
	sensor_msgs::JointState joint_state;
	joint_state.header.stamp = ros::Time::now();
	joint_state.name = ramp_setting_ptr->getJointNames();
	joint_state.position = cfg;
	joint_cfg_pub.publish(joint_state);
	ros::spinOnce();
	if (ramp_params.verbose)
	{
		std::cout << "Rendering joint configuration [ ";
		for (int i=0; i<cfg.size(); ++i)
		{
			std::cout << cfg.at(i) << " ";
		}
		std::cout << "]" << std::endl;
	}
	sleep(1.0);
}

void rampABBmotionPlanning::renderJointTrajectoryMsg(const trajectory_msgs::JointTrajectory &trajectory)
{
	renderJointTrajectoryMsg(trajectory, ramp_setting_ptr->getJointNames());
}

void rampABBmotionPlanning::renderJointTrajectoryMsg(const trajectory_msgs::JointTrajectory &trajectory,const std::vector<std::string> &jnt_names)
{
	moveit_msgs::RobotTrajectory traj_msg;
	traj_msg.joint_trajectory = trajectory;

	moveit_msgs::RobotState robot_state;
	if (is_tool_attached)
	{
		sensor_msgs::JointState joint_state;
		joint_state.header.stamp = ros::Time::now();
		joint_state.name = jnt_names;
		joint_state.position = trajectory.points.front().positions;

		robot_state.joint_state = joint_state; 
		robot_state.attached_collision_objects.push_back(attached_tool_geometry);
		if (is_object_attached)
			robot_state.attached_collision_objects.push_back(attached_object_geometry);
		robot_state.is_diff = true;
	}
	else
	{
		robot_state.joint_state.name = jnt_names;
		robot_state.joint_state.position = trajectory.points.front().positions;
	}

	moveit_msgs::DisplayTrajectory display_trajectory;
	display_trajectory.trajectory.push_back(traj_msg);
	display_trajectory.trajectory_start = robot_state;

	display_pub.publish(display_trajectory);
}

void rampABBmotionPlanning::generateMotionPlanViaGraphSearch(moveit_msgs::RobotTrajectory &trajectory, std::vector<int> &reachable_via_pnt_inds, std::vector<moveit_msgs::RobotTrajectory> &trajectory_segments)
{
	ROS_INFO("*****************************************");
	ROS_INFO("Motion Plan Generation via Graph Search");
	ROS_INFO("*****************************************");
	/* reset node id for graph nodes */
	node_id = 0;
	planning_graph.reset();
	/* 
		step 0 
		convert via point poses in tool_x to tool_0
	 */
	/* make a copy since we may need to transform poses */
	std::vector<motionViaPoint> via_points = via_pnts_specified;
	/* convert from tool x frame to tool0 frame */
	Eigen::Affine3d tf_toolx_to_base, tf_tool0_to_base;
	for(std::size_t i=0; i<via_points.size(); ++i)
	{
		rampUtil::convertQuaterniontoAffine3d(via_points[i].pose, tf_toolx_to_base);
		tf_tool0_to_base = tf_toolx_to_base * tf_from_toolx_to_tool0.inverse();
		rampUtil::convertAffine3dtoQuaternion(tf_tool0_to_base, via_points[i].pose);
	}

	visualizeMotionViaPoints(via_pnts_specified); // tool_x frame
	/* 
		Step 2
		Add nodes and edges in the planning graph based on planning requirements
	 */
	int src_node_id = 0; // always 0
	int goal_node_id = -1; // to be decided after all nodes in the graph are added
	std::vector<std::vector<armCfg> > level_cfgs; // arm cfgs corresponding to each motion via point
	level_cfgs.resize(via_points.size()+1); // size+1 to include start default cfg
	
	/* use default cfg as starting cfg */
	armCfg default_cfg;
	ramp_setting_ptr->getDefaultJointConfiguration(default_cfg);
	std::vector<armCfg> cfg_vec;
	cfg_vec.push_back(default_cfg);
	level_cfgs.front() = cfg_vec;

	/* add src node to the planning graph */
	node src_n = node(node_id, 0, cfg_vec.front());
	++node_id;
	planning_graph.addNode(src_n);

	int cur_reachable_motion_pnt_ind = 0;
	std::vector<int> reachable_motion_pnt_inds;
	for(std::size_t i=0; i<via_points.size(); ++i)
	{
		switch(via_points[i].m_type)
		{
			case FREE:
			{
				std::cout << std::endl;
				ROS_INFO("Generating free segment ending at via point %zd", i);
				std::vector<armCfg> seg_start_cfgs, seg_end_cfgs;
				if(!ramp_setting_ptr->getAllIKJointConfigurationsIncludingLastJointTurns(seg_end_cfgs, via_points[i].pose))
				{
					ROS_ERROR("No IK solutions found for via point %zd", i);
					if (continue_on_unreachble)
					{
						ROS_INFO("Continue with next via point");
						continue;
					}	
					else
					{
						ROS_ERROR("Program stopped at the unreachble point");
						return;
					}
				}
				seg_start_cfgs = level_cfgs[cur_reachable_motion_pnt_ind];
				/* check connectivity between each start and end cfg */
				bool is_reachable = false;
				for(int j=0; j<seg_start_cfgs.size(); ++j)
				{
					for(int k=0; k<seg_end_cfgs.size(); ++k)
					{
						moveit_msgs::RobotTrajectory tmp_traj;
						if(moveFromJntCfgToJntCfg(seg_start_cfgs[j], seg_end_cfgs[k], tmp_traj))
						{
							is_reachable = true;
							node src_n, des_n;
							/* make an edge in the graph */
							node tmp_exist_node;
							if (planning_graph.hasNodeWithJntCfg(seg_start_cfgs[j], tmp_exist_node) && (tmp_exist_node.motion_point_id == cur_reachable_motion_pnt_ind))
							{
								src_n = node(tmp_exist_node.id, tmp_exist_node.motion_point_id, seg_start_cfgs[j]);
								if ( planning_graph.hasNodeWithJntCfg(seg_end_cfgs[k], tmp_exist_node) && (tmp_exist_node.motion_point_id == i+1) )
									des_n = node(tmp_exist_node.id, tmp_exist_node.motion_point_id, seg_end_cfgs[k]);
								else
								{
									des_n = node(node_id, i+1, seg_end_cfgs[k]);
									++node_id;
								}

								double weight = rampUtil::getJointPathLength(tmp_traj);
								edge tmp_edge(des_n, weight, tmp_traj, FREE);
								planning_graph.addEdge(src_n, tmp_edge);							
								/* save seg_end_cfgs with successful connectivity this condition makes sure only save once */
								if (j==0)
									level_cfgs[i+1].push_back(seg_end_cfgs[k]);
							}
						}
					}
				}
				if(is_reachable)
				{
					/* note down current motion point as reachable */
					reachable_motion_pnt_inds.push_back(i);
					/* update next reachable motion point */
					cur_reachable_motion_pnt_ind = i+1; 
				}
				break;
			}
			case LINEAR:
			{
				std::cout << std::endl;
				std::vector<moveit_msgs::RobotTrajectory> tmp_linear_segs;
				ROS_INFO("Generating linear segment ending at via point %zd", i);
				bool linear_segment_success = linearPathEvaluation(via_points[cur_reachable_motion_pnt_ind-1].pose, via_points[i].pose, tmp_linear_segs, false);
				if (linear_segment_success)
				{	
					ROS_INFO("%zd trajectories for linear segment ending at via point %zd", tmp_linear_segs.size(), i);
					for (int j=0; j<tmp_linear_segs.size(); ++j)
					{
						/* make an edge in the graph */
						armCfg start_cfg = tmp_linear_segs[j].joint_trajectory.points.front().positions;
						armCfg end_cfg = tmp_linear_segs[j].joint_trajectory.points.back().positions;
						/* only make an edge if start_cfg already in the graph */
						node tmp_exist_node;
						node src_node, des_node;
						if (planning_graph.hasNodeWithJntCfg(start_cfg, tmp_exist_node) && (tmp_exist_node.motion_point_id == cur_reachable_motion_pnt_ind) )
						{
							src_node = node(tmp_exist_node.id, tmp_exist_node.motion_point_id, start_cfg);
							des_node = node(node_id, i+1, end_cfg);
							++node_id;
							double weight = rampUtil::getJointPathLength(tmp_linear_segs[j]);
							edge tmp_edge(des_node, weight, tmp_linear_segs[j], LINEAR);
							planning_graph.addEdge(src_node, tmp_edge);
							/* save seg_end_cfgs with successful connectivity */
							level_cfgs[i+1].push_back(end_cfg);
						}
					}
					/* note down current motion point as reachable */
					reachable_motion_pnt_inds.push_back(i);
					cur_reachable_motion_pnt_ind = i+1;
				}
				else
				{
					ROS_ERROR("Linear segment ending at via point %zd is not reachable", i);
					if (continue_on_unreachble)
					{
						ROS_INFO("Continue with next via point");
						continue;
					}	
					else
					{
						ROS_ERROR("Program stopped at the unreachble point");
						return;
					}
				}
				break;
			}
			case ARC:
			{
				std::cout << std::endl;
				std::vector<moveit_msgs::RobotTrajectory> tmp_arc_segs;
				ROS_INFO("Generating arc segment middling at via point %zd", i);
				bool arc_segment_success = arcPathEvaluation(via_points[i-1].pose, via_points[i].pose, via_points[i+1].pose, tmp_arc_segs);
				if (arc_segment_success)
				{
					ROS_INFO("%zd trajectories generated for arc segment middling at via point %zd", tmp_arc_segs.size(), i);
					for (int j=0; j<tmp_arc_segs.size(); ++j)
					{
						armCfg start_cfg = tmp_arc_segs[j].joint_trajectory.points.front().positions;
						armCfg end_cfg = tmp_arc_segs[j].joint_trajectory.points.back().positions;
						node tmp_exist_node;
						node src_node, des_node;
						if (planning_graph.hasNodeWithJntCfg(start_cfg, tmp_exist_node) && (tmp_exist_node.motion_point_id == i) )
						{
							src_node = node(tmp_exist_node.id, tmp_exist_node.motion_point_id, start_cfg);
							des_node = node(node_id, i+2, end_cfg);
							++node_id;
							double weight = rampUtil::getJointPathLength(tmp_arc_segs[j]);
							edge tmp_edge(des_node, weight, tmp_arc_segs[j], ARC);
							planning_graph.addEdge(src_node, tmp_edge);
							level_cfgs[i+2].push_back(end_cfg);
						}
					}
					/* note down reachable points */
					reachable_motion_pnt_inds.push_back(i);
					reachable_motion_pnt_inds.push_back(i+1);
					cur_reachable_motion_pnt_ind = i+2;
					/* fast forward to last arc pnt */
					i = i+1;
				}
				else
				{
					ROS_ERROR("Arc segment middling at via point %zd is not reachable", i);
					if (continue_on_unreachble)
					{
						ROS_INFO("Continue with next via point not on arc");
						i = i+1;
						continue;
					}	
					else
					{	
						ROS_ERROR("Program stopped at the unreachble point");
						return;
					}
				}
				break;
			}
		}
	}

	/*
		Step 2 
		Add final free segment back to default cfg 
	*/
	std::vector<armCfg> seg_start_cfgs = level_cfgs[cur_reachable_motion_pnt_ind];
	for(int j=0; j<seg_start_cfgs.size(); ++j)
	{
		moveit_msgs::RobotTrajectory tmp_traj;
		if(moveFromJntCfgToJntCfg(seg_start_cfgs[j], default_cfg, tmp_traj))
		{
			node src_n, des_n;
			// make an edge in the graph
			node tmp_exist_node;
			if (planning_graph.hasNodeWithJntCfg(seg_start_cfgs[j], tmp_exist_node) && (tmp_exist_node.motion_point_id == cur_reachable_motion_pnt_ind))
			{
				src_n = node(tmp_exist_node.id, tmp_exist_node.motion_point_id, seg_start_cfgs[j]);
				// always make a new and distinctive node for last default cfg
				des_n = node(node_id, level_cfgs.size(), default_cfg); 
				goal_node_id = des_n.id;
				double weight = rampUtil::getJointPathLength(tmp_traj);
				edge tmp_edge(des_n, weight, tmp_traj, FREE);
				planning_graph.addEdge(src_n, tmp_edge);
			}
		}
	}

	planning_graph.printAdjList();
	planning_graph.printNodesInfo();
	std::vector<int> path;
	planning_graph.shortestPath(src_node_id, goal_node_id, path);

	/* 
		Step 3
		Reconstruct whole path
	 */
	std::vector<moveit_msgs::RobotTrajectory> traj_segs;
	for (int i=0; i<(path.size()-1); ++i)
	{
		moveit_msgs::RobotTrajectory tmp_traj;
		planning_graph.getEdgeTrajectory(path[i], path[i+1], tmp_traj, only_output_key_cfgs);
		traj_segs.push_back(tmp_traj);
	}
	moveit_msgs::RobotTrajectory final_joint_path;
	final_joint_path.joint_trajectory.joint_names = ramp_setting_ptr->getJointNames(); // remember to add names!
	appendTrajectories(final_joint_path, traj_segs);

	/* 
		Step 4
		Time parameterization of the path
	 */
	robot_state::RobotState current_state(*move_group.getCurrentState());
	current_state.setToDefaultValues();
	robot_trajectory::RobotTrajectory rob_traj(ramp_setting_ptr->getRobotModelPtr(), ramp_setting_ptr->getJointModelGroupName());
	rob_traj.setRobotTrajectoryMsg(current_state, final_joint_path);

	trajectory_msgs::JointTrajectory final_joint_traj;
	trajectory_processing::IterativeParabolicTimeParameterization iptp;
	if(iptp.computeTimeStamps(rob_traj))
	{
		convertRobotTrajectoryToMsg(rob_traj, final_joint_traj);
	}
	/* 
		Step 5
		Send for visualization
	 */
	ROS_INFO("Reachable motion point indices are");
	std::cout << "[";
	for (int i=0; i<reachable_motion_pnt_inds.size(); ++i)
		std::cout << " " << reachable_motion_pnt_inds[i];
	std::cout << "]" << std::endl;

	ROS_INFO("Press Enter to display trajectory.");
	getchar();
	ROS_INFO("Displaying whole trajectory");
	renderJointTrajectoryMsg(final_joint_traj, ramp_setting_ptr->getJointNames());
	/* 
		Step 6
		collect result for output
	*/
	trajectory_segments = traj_segs;
	reachable_via_pnt_inds = reachable_motion_pnt_inds;
	trajectory.joint_trajectory = final_joint_traj;
}

bool rampABBmotionPlanning::addToolSrv(ramp::addTool::Request &req, ramp::addTool::Response &res)
{
	Eigen::Affine3d local_transform_toolx_to_tool0;
	rampUtil::convertQuaterniontoAffine3d(req.toolx_pose_wrt_tool0, local_transform_toolx_to_tool0);
	addTool(local_transform_toolx_to_tool0, req.path_to_toolx_mesh.data);
}
bool rampABBmotionPlanning::removeToolSrv(ramp::removeTool::Request &req, ramp::removeTool::Response &res)
{
	removeTool();
}

bool rampABBmotionPlanning::addAttachedObjectSrv(ramp::addAttachedObject::Request &req, ramp::addAttachedObject::Response &res)
{
	Eigen::Affine3d local_transform_object_to_toolx;
	rampUtil::convertQuaterniontoAffine3d(req.object_pose_wrt_toolx, local_transform_object_to_toolx);
	addAttachedObject(local_transform_object_to_toolx, req.path_to_object_mesh.data);
}

bool rampABBmotionPlanning::removeAttachedObjectSrv(ramp::removeAttachedObject::Request &req, ramp::removeAttachedObject::Response &res)
{
	removeAttachedObject();
}

bool rampABBmotionPlanning::addPointCloudToSceneSrv(ramp::addPointCloudToScene::Request &req, ramp::addPointCloudToScene::Response &res)
{
	Eigen::Affine3d tf_camera_to_base;
	rampUtil::convertQuaterniontoAffine3d(req.camera_pose_wrt_robot_base, tf_camera_to_base);
	loadPointCloud(tf_camera_to_base, req.path_to_point_cloud.data);
}

bool rampABBmotionPlanning::generateMotionPlanSrv(ramp::generateMotionPlan::Request &req, ramp::generateMotionPlan::Response &res)
{
	std::vector<motionViaPoint> points;
	for(int i=0; i<req.plan_request.motion_plan_requests.size(); ++i)
	{
		geometry_msgs::Pose p = req.plan_request.motion_plan_requests[i].pose;
		via_type v;
		motion_type m;
		if (req.plan_request.motion_plan_requests[i].via_type.data=="FLY_BY")
			v = FLY_BY;
		else if (req.plan_request.motion_plan_requests[i].via_type.data=="STOP")
			v = FLY_BY;
		else
			ROS_ERROR("Unrecognized via type at point %d", i);
		
		if (req.plan_request.motion_plan_requests[i].motion_type.data=="ARC")
			m = ARC;
		else if (req.plan_request.motion_plan_requests[i].motion_type.data=="LINEAR")
			m = LINEAR;
		else if (req.plan_request.motion_plan_requests[i].motion_type.data=="FREE")
			m = FREE;
		else
			ROS_ERROR("Unrecognized motion type at point %d", i);
		
		motionViaPoint vp(p, v, m, true);
		points.push_back(vp);
	}

	clearViaPoints();
	setViaPoints(points);
	setFlagContinueOnUnreachable(req.continue_on_unreachable.data);
	setOutputKeyCfgsFlag(req.output_key_cfgs.data); // true for key cfgs only

	moveit_msgs::RobotTrajectory traj;
	std::vector<moveit_msgs::RobotTrajectory> traj_segs;
	std::vector<int> reachable_via_pnt_inds;
	generateMotionPlanViaGraphSearch(traj, reachable_via_pnt_inds, traj_segs); 
	std::vector<trajectory_msgs::JointTrajectory> jnt_traj_segs;
	for (int i=0; i<traj_segs.size(); ++i)
		jnt_traj_segs.push_back(traj_segs[i].joint_trajectory);

	res.reachable_via_pnts_inds.data = reachable_via_pnt_inds;
	res.trajectory = traj.joint_trajectory;
	res.trajectory_segments = jnt_traj_segs;

}

void rampABBmotionPlanning::clearContactCfgContainer()
{
	contact_cfgs.clear();
}

std::vector<armCfg> rampABBmotionPlanning::getContactCfgs()
{
	return contact_cfgs;
}

void rampABBmotionPlanning::visualizeContactCfgs()
{
	if (contact_cfgs.empty())
	{
		ROS_WARN("No contact cfgs to visualize");
		return;
	}
	for (auto cfg : contact_cfgs)
	{
		renderJointConfiguration(cfg);
		sleep(1.0);
	}
	ROS_INFO("Visualized %zd contact arm cfgs", contact_cfgs.size());
}

void rampABBmotionPlanning::getEefPose(const armCfg& cfg, Eigen::Affine3d& tf_from_eef_link_to_arm_base)
{
	ramp_setting_ptr->setJointConfiguration(cfg);
	ramp_setting_ptr->getEefPose(tf_from_eef_link_to_arm_base);
}


