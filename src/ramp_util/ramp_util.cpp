#include "ramp_util/ramp_util.h"

void rampUtil::convertXYZRPYtoAffine3d(const std::vector<double> &pose_vec, Eigen::Affine3d &pose_tf)
{
	Eigen::Matrix3d rot;
	rot = Eigen::AngleAxisd(pose_vec[3], Eigen::Vector3d::UnitX())
		 *Eigen::AngleAxisd(pose_vec[4], Eigen::Vector3d::UnitY())
		 *Eigen::AngleAxisd(pose_vec[5], Eigen::Vector3d::UnitZ());
	pose_tf = rot;
	pose_tf.translation() = Eigen::Vector3d(pose_vec[0],pose_vec[1],pose_vec[2]);
}

void rampUtil::convertAffine3dtoQuaternion(const Eigen::Affine3d &pose_tf, geometry_msgs::Pose &pose_quat)
{
	pose_quat.position.x = pose_tf.translation()[0];
	pose_quat.position.y = pose_tf.translation()[1];
	pose_quat.position.z = pose_tf.translation()[2];
	Eigen::Quaterniond q(pose_tf.rotation());
	pose_quat.orientation.x = q.x();
	pose_quat.orientation.y = q.y();
	pose_quat.orientation.z = q.z();
	pose_quat.orientation.w = q.w();
}

void rampUtil::convertAffine3dtoTfTransform(const Eigen::Affine3d &pose_tf, tf::Transform& transform)
{
	geometry_msgs::Pose pose_msg;
	convertAffine3dtoQuaternion(pose_tf, pose_msg);
	tf::Quaternion tf_quat;
    tf::quaternionMsgToTF(pose_msg.orientation, tf_quat);
	transform.setOrigin(tf::Vector3(pose_msg.position.x, pose_msg.position.y, pose_msg.position.z));
    transform.setRotation(tf_quat);
}

void rampUtil::convertTfTransformtoMsg(const tf::Transform& transform, geometry_msgs::Pose &pose_msg)
{
    pose_msg.position.x = transform.getOrigin().x();
	pose_msg.position.y = transform.getOrigin().y();
	pose_msg.position.z = transform.getOrigin().z();
    tf::quaternionTFToMsg(transform.getRotation(), pose_msg.orientation);
}

void rampUtil::convertTfTransformtoAffine3d(const tf::Transform& transform, Eigen::Affine3d &pose_tf)
{
	geometry_msgs::Pose tmp;
	convertTfTransformtoMsg(transform, tmp);
	convertQuaterniontoAffine3d(tmp, pose_tf);
}

void rampUtil::convertXYZRPYtoQuaternion(const std::vector<double> &pose_vec, geometry_msgs::Pose &pose_quat)
{
	Eigen::Affine3d tmp;
	convertXYZRPYtoAffine3d(pose_vec, tmp);
	convertAffine3dtoQuaternion(tmp, pose_quat);
}

void rampUtil::convertXYZRPYtoTfTransform(const std::vector<double> &pose_vec, tf::Transform& transform)
{
	geometry_msgs::Pose pose_quat;
	convertXYZRPYtoQuaternion(pose_vec, pose_quat);
	tf::Quaternion tf_quat;
	tf::quaternionMsgToTF(pose_quat.orientation, tf_quat);
	transform.setOrigin(tf::Vector3(pose_quat.position.x, pose_quat.position.y, pose_quat.position.z));
	transform.setRotation(tf_quat);
}
void rampUtil::convertQuaterniontoXYZRPY(const geometry_msgs::Pose &pose_quat, std::vector<double> &pose_vec)
{
	pose_vec.resize(6, 0.0);
	pose_vec.at(0) = pose_quat.position.x;
	pose_vec.at(1) = pose_quat.position.y;
	pose_vec.at(2) = pose_quat.position.z;
	Eigen::Quaterniond q(pose_quat.orientation.w, pose_quat.orientation.x,
						 pose_quat.orientation.y, pose_quat.orientation.z); // w x y z 

	Eigen::Vector3d euler_ang = q.toRotationMatrix().eulerAngles(0, 1, 2);
	pose_vec.at(3) = euler_ang[0];
	pose_vec.at(4) = euler_ang[1];
	pose_vec.at(5) = euler_ang[2];
}

void rampUtil::convertAffine3dtoXYZRPY(const Eigen::Affine3d &pose_tf, std::vector<double> &pose_vec)
{
	pose_vec.resize(6,0.0);
	pose_vec.at(0) = pose_tf.translation()[0];
	pose_vec.at(1) = pose_tf.translation()[1];
	pose_vec.at(2) = pose_tf.translation()[2];

	Eigen::Vector3d euler_ang = pose_tf.rotation().eulerAngles(0,1,2);
	pose_vec.at(3) = euler_ang[0];
	pose_vec.at(4) = euler_ang[1];
	pose_vec.at(5) = euler_ang[2];
}

void rampUtil::convertTfTransformtoXYZRPY(const tf::Transform& transform, std::vector<double> &pose_vec)
{
	Eigen::Affine3d affine_tf;
	convertTfTransformtoAffine3d(transform, affine_tf);
	convertAffine3dtoXYZRPY(affine_tf, pose_vec);
}

void rampUtil::convertQuaterniontoAffine3d(const geometry_msgs::Pose &pose_quat, Eigen::Affine3d &pose_tf)
{
	Eigen::Quaterniond q(pose_quat.orientation.w, pose_quat.orientation.x,
						 pose_quat.orientation.y, pose_quat.orientation.z);
	pose_tf = q.toRotationMatrix();
	pose_tf.translation() = Eigen::Vector3d(pose_quat.position.x, pose_quat.position.y, pose_quat.position.z);
}

void rampUtil::printGeoMsgPose(const geometry_msgs::Pose &p)
{
	ROS_INFO("Pose is [%.4f, %.4f, %.4f, %.4f, %.4f, %.4f, %.4f]",p.position.x,
													p.position.y,
													p.position.z,
													p.orientation.x,
													p.orientation.y,
													p.orientation.z,
													p.orientation.w);
}

void rampUtil::printGeoMsgTwist(const geometry_msgs::Twist &t)
{
	ROS_INFO("Twist is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",t.linear.x,
												 t.linear.y,
												 t.linear.z,
												 t.angular.x,
												 t.angular.y,
												 t.angular.z);
}

void rampUtil::printArmCfg(const armCfg &jv)
{
	if (jv.size()==6)
		ROS_INFO("jnt vector is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",  jv[0],
																		jv[1],
																		jv[2],
																		jv[3],
																		jv[4],
																		jv[5]);
	if (jv.size()==7)
		ROS_INFO("jnt vector is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",jv[0],
																			jv[1],
																			jv[2],
																			jv[3],
																			jv[4],
																			jv[5],
																			jv[6]);
	if (jv.size()==8)
		ROS_INFO("jnt vector is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",jv[0],
																				  jv[1],
																				  jv[2],
																				  jv[3],
																				  jv[4],
																				  jv[5],
																				  jv[6],
																				  jv[7]);
}

void rampUtil::printArmCfgs(const std::vector<armCfg> &jvs)
{
	for (int i=0; i<jvs.size(); ++i)
		printArmCfg(jvs.at(i));
}

void rampUtil::printTfTransform(const tf::Transform& tf)
{
	geometry_msgs::Pose p;
	convertTfTransformtoMsg(tf, p);
	printGeoMsgPose(p);
}

float rampUtil::randomFloat(float a, float b) 
{
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

void rampUtil::saveCfgsToFile(std::vector<armCfg> &cfgs)
{
	const std::string file_path("/home/huitan/Dropbox/RAMP_DATA/sequentail_knowledge_transfer/with_skt/");
	std::string spec_path = file_path+"XZ-P-XYZ-O_exe_path_cfgs_.txt";
	ROS_INFO("Saving executed path knot cfgs to file %s", spec_path.c_str());
	if (!cfgs.empty())
	{
		int n_dofs = cfgs.front().size();
		std::ofstream save_file;
		save_file.open (spec_path);
		for(int i=0; i<cfgs.size(); ++i)
		{
			/* every n_dofs is one knot cfg */
			for (int j=0; j<n_dofs; ++j)
				save_file << cfgs.at(i).at(j) << "\n";
		}
		save_file.close();
	}
	else
		ROS_ERROR("Empty cfgs to save!");

}

bool rampUtil::areSamePoses(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2)
{
	float p1, p2, p3, p4, p5, p6, p7;
	p1 = 0.;
	p2 = 0.;
	p3 = 0.;
	p4 = 0.;
	p5 = 0.;
	p6 = 0.;
	p7 = 0.;

	// compare position difference
	p5 = std::abs(pose1.position.x - pose2.position.x);
	p6 = std::abs(pose1.position.y - pose2.position.y);
	p7 = std::abs(pose1.position.z - pose2.position.z);

	// compare orientation difference
	p1 = std::abs(pose1.orientation.x - pose2.orientation.x);
	p2 = std::abs(pose1.orientation.y - pose2.orientation.y);
	p3 = std::abs(pose1.orientation.z - pose2.orientation.z);
	p4 = std::abs(pose1.orientation.w - pose2.orientation.w);
	float diff = std::abs(sqrt(p1*p1+p2*p2+p3*p3+p4*p4+p5*p5+p6*p6+p7*p7));
	if (diff < 0.1)
		return true;
	else
		return false;
}

bool rampUtil::areSamePositions(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2)
{
	float p5, p6, p7;
	p5 = 0.;
	p6 = 0.;
	p7 = 0.;

	// compare position difference
	p5 = std::abs(pose1.position.x - pose2.position.x);
	p6 = std::abs(pose1.position.y - pose2.position.y);
	p7 = std::abs(pose1.position.z - pose2.position.z);

	float diff = std::abs(sqrt(p5*p5+p6*p6+p7*p7));
	if (diff < 0.01)
		return true;
	else
		return false;
}

bool rampUtil::areSameJntCfgs(const armCfg &c1, const armCfg &c2)
{
	// for 6-axis
	double d0 = std::abs(c1[0]-c2[0]);
	double d1 = std::abs(c1[1]-c2[1]);
	double d2 = std::abs(c1[2]-c2[2]);
	double d3 = std::abs(c1[3]-c2[3]);
	double d4 = std::abs(c1[4]-c2[4]);
	double d5 = std::abs(c1[5]-c2[5]);
	double sum = pow(d0,2)+pow(d1,2)+pow(d2,2)+pow(d3,2)+pow(d4,2)+pow(d5,2);
	if (sqrt(sum)<1e-1)
		return true;
	else
		return false;
}

double rampUtil::getJntCfgDistance(const armCfg &c1, const armCfg &c2)
{
	// for 6-axis
	double d0 = std::abs(c1[0]-c2[0]);
	double d1 = std::abs(c1[1]-c2[1]);
	double d2 = std::abs(c1[2]-c2[2]);
	double d3 = std::abs(c1[3]-c2[3]);
	double d4 = std::abs(c1[4]-c2[4]);
	double d5 = std::abs(c1[5]-c2[5]);
	double sum = pow(d0,2)+pow(d1,2)+pow(d2,2)+pow(d3,2)+pow(d4,2)+pow(d5,2);
	double result = sqrt(sum);
	if (std::abs(result)<1e-4)
		result = 1e-3;
	if (result > 0.)
		return result;
	else
	{	
		std::cout << "result is " << result << std::endl;
		ROS_ERROR("Returning large value");
		return 1e7;
	}
}

double rampUtil::getJointPathLength(const moveit_msgs::RobotTrajectory &traj)
{
	double res = 0.;
	for(int i=0; i<(traj.joint_trajectory.points.size()-1); ++i)
	{
		res += getJntCfgDistance(traj.joint_trajectory.points[i].positions, traj.joint_trajectory.points[i+1].positions);
	}
	return res;
}

void rampUtil::printTrajectory(const moveit_msgs::RobotTrajectory &traj)
{
	std::cout << std::endl;
	std::cout << "Trajectory has " << traj.joint_trajectory.points.size() << " points" << std::endl;
	for(int i=0; i<traj.joint_trajectory.points.size(); ++i)
	{	
		std::cout << "[Point " << i << "]:" << std::endl;
		
		std::cout << "   [Positions]:";
		std::cout << "   [ ";
		for (int j=0; j<traj.joint_trajectory.points[i].positions.size(); ++j)
		{
			std::cout << traj.joint_trajectory.points[i].positions.at(j) << " ";
		}
		std::cout << "]" << std::endl;

		std::cout << "   [Velocities]:";
		std::cout << "   [ ";
		for (int j=0; j<traj.joint_trajectory.points[i].velocities.size(); ++j)
		{
			std::cout << traj.joint_trajectory.points[i].velocities.at(j) << " ";
		}
		std::cout << "]" << std::endl;

		std::cout << "   [Accelerations]:";
		std::cout << "   [ ";
		for (int j=0; j<traj.joint_trajectory.points[i].accelerations.size(); ++j)
		{
			std::cout << traj.joint_trajectory.points[i].accelerations.at(j) << " ";
		}
		std::cout << "]" << std::endl;

		std::cout << "   [Time from start]: " << traj.joint_trajectory.points[i].time_from_start.toSec() << " secs" << std::endl;
	}
	std::cout << std::endl;
}

double rampUtil::get3dDistance(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2)
{
	// return sqrt(pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2) + pow(p1[2] - p2[2], 2));
	return (p1-p2).norm();
}

double rampUtil::convertDegToRad(double deg)
{
	return deg * 0.0174533;
}

double rampUtil::convertMtoMM(double len_in_meter)
{
	return len_in_meter * 1000.0;
}












 
