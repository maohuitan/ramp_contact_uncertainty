#include "ramp_contact/haptic_rendering_qp.h"

hapticRenderingQP::hapticRenderingQP()
: P_(6), q_(6)
{
    pre_x_g = 0.;
    pre_y_g = 0.;
    pre_z_g = 0.;
    pre_gamma_g = 0.;
    pre_beta_g = 0.;
    pre_alpha_g = 0.;

    // constants
    P_.setZero();

    qp_srv_client_ = ros::NodeHandle().serviceClient<ramp::qp>("/qp_query");
}

hapticRenderingQP::~hapticRenderingQP()
{

}

void hapticRenderingQP::setHapticCfg(const std::vector<double>& haptic_cfg)
{
    haptic_cfg_ = haptic_cfg;
    q_ << -haptic_cfg[0], -haptic_cfg[1], -haptic_cfg[2], -haptic_cfg[3], -haptic_cfg[4], -haptic_cfg[5];
}

void hapticRenderingQP::setGraphicCfgPrevious(const std::vector<double>& graphic_cfg_pre)
{
    pre_x_g = graphic_cfg_pre[0];
    pre_y_g = graphic_cfg_pre[1];
    pre_z_g = graphic_cfg_pre[2];
    pre_gamma_g = graphic_cfg_pre[3];
    pre_beta_g = graphic_cfg_pre[4];
    pre_alpha_g = graphic_cfg_pre[5];
}

void hapticRenderingQP::setCollidingSpherePairs(const std::vector<std::pair<sphere, sphere>>& colliding_pairs)
{
    // std::cout << "Making constraints" << std::endl;
    for (const auto& p : colliding_pairs)
    {
        sphere active_sp = p.first;
        sphere passive_sp = p.second;

        double x_l = active_sp.center[0];  double y_l = active_sp.center[1];  double z_l = active_sp.center[2];
        double x_o = passive_sp.center[0]; double y_o = passive_sp.center[1]; double z_o = passive_sp.center[2];

        makeOneConstraint(x_l, y_l, z_l, x_o, y_o, z_o, active_sp.radius, passive_sp.radius);
    }
    // std::cout << "Finished making constraints" << std::endl;
}

void hapticRenderingQP::makeOneConstraint(double x_l, double y_l, double z_l,
                                          double x_o, double y_o, double z_o,
                                          double act_sp_rad, double pas_sp_rad)
{
    double x_t = computeX_T(x_l, y_l, z_l);
    double y_t = computeY_T(x_l, y_l, z_l);
    double z_t = computeZ_T(x_l, y_l, z_l);

    double phi_x_T_over_phi_qg4 = compute_phi_x_T_over_phi_qg4(x_l, y_l, z_l);
    double phi_y_T_over_phi_qg4 = compute_phi_y_T_over_phi_qg4(x_l, y_l, z_l);
    double phi_z_T_over_phi_qg4 = compute_phi_z_T_over_phi_qg4(x_l, y_l, z_l);

    double phi_x_T_over_phi_qg5 = compute_phi_x_T_over_phi_qg5(x_l, y_l, z_l);
    double phi_y_T_over_phi_qg5 = compute_phi_y_T_over_phi_qg5(x_l, y_l, z_l);
    double phi_z_T_over_phi_qg5 = compute_phi_z_T_over_phi_qg5(x_l, y_l, z_l);

    double phi_x_T_over_phi_qg6 = compute_phi_x_T_over_phi_qg6(x_l, y_l, z_l);
    double phi_y_T_over_phi_qg6 = compute_phi_y_T_over_phi_qg6(x_l, y_l, z_l);
    double phi_z_T_over_phi_qg6 = compute_phi_z_T_over_phi_qg6(x_l, y_l, z_l);

    double T1 = 2.0*(x_o - x_t);
    double T2 = 2.0*(y_o - y_t);
    double T3 = 2.0*(z_o - z_t);
    double T4 = 2.0*(x_o - x_t)*phi_x_T_over_phi_qg4 + 2.0*(y_o - y_t)*phi_y_T_over_phi_qg4 + 2.0*(z_o - z_t)*phi_z_T_over_phi_qg4;
    double T5 = 2.0*(x_o - x_t)*phi_x_T_over_phi_qg5 + 2.0*(y_o - y_t)*phi_y_T_over_phi_qg5 + 2.0*(z_o - z_t)*phi_z_T_over_phi_qg5;
    double T6 = 2.0*(x_o - x_t)*phi_x_T_over_phi_qg6 + 2.0*(y_o - y_t)*phi_y_T_over_phi_qg6 + 2.0*(z_o - z_t)*phi_z_T_over_phi_qg6;

    double pre_c = pow((x_t - x_o), 2) + pow((y_t - y_o), 2) + pow((z_t - z_o), 2) - pow((act_sp_rad + pas_sp_rad), 2);
    // std::cout << "pre_c is " << pre_c << std::endl;
    double H1 = pre_c - 2.0*(x_t - x_o)*pre_x_g - 2.0*(y_t - y_o)*pre_y_g - 2.0*(z_t - z_o)*pre_z_g
                -(2.0*(x_t - x_o)*phi_x_T_over_phi_qg4 + 2.0*(y_t - y_o)*phi_y_T_over_phi_qg4 + 2.0*(z_t - z_o)*phi_z_T_over_phi_qg4)*pre_gamma_g
                -(2.0*(x_t - x_o)*phi_x_T_over_phi_qg5 + 2.0*(y_t - y_o)*phi_y_T_over_phi_qg5 + 2.0*(z_t - z_o)*phi_z_T_over_phi_qg5)*pre_beta_g
                -(2.0*(x_t - x_o)*phi_x_T_over_phi_qg6 + 2.0*(y_t - y_o)*phi_y_T_over_phi_qg6 + 2.0*(z_t - z_o)*phi_z_T_over_phi_qg6)*pre_alpha_g;

    Eigen::VectorXd G_row(6);
    G_row << T1, T2, T3, T4, T5, T6;
    // std::cout << G_row.transpose() << std::endl;
    G_.push_back(G_row);
    h_.push_back(H1);
}

double hapticRenderingQP::computeX_T(double x_l, double y_l, double z_l)
{
    double X_T = cos(pre_alpha_g)*cos(pre_beta_g)*x_l + (cos(pre_alpha_g)*sin(pre_beta_g)*sin(pre_gamma_g) - sin(pre_alpha_g)*cos(pre_gamma_g))*y_l
                + (cos(pre_alpha_g)*sin(pre_beta_g)*cos(pre_gamma_g) + sin(pre_alpha_g)*sin(pre_gamma_g))*z_l + pre_x_g;
    return X_T;
}

double hapticRenderingQP::computeY_T(double x_l, double y_l, double z_l)
{
    double Y_T = sin(pre_alpha_g)*cos(pre_beta_g)*x_l + (sin(pre_alpha_g)*sin(pre_beta_g)*sin(pre_gamma_g) + cos(pre_alpha_g)*cos(pre_gamma_g))*y_l
                + (sin(pre_alpha_g)*sin(pre_beta_g)*cos(pre_gamma_g) - cos(pre_alpha_g)*sin(pre_gamma_g))*z_l + pre_y_g;
    return Y_T;
}

double hapticRenderingQP::computeZ_T(double x_l, double y_l, double z_l)
{
    double Z_T = -sin(pre_beta_g)*x_l + cos(pre_beta_g)*sin(pre_gamma_g)*y_l + cos(pre_beta_g)*cos(pre_gamma_g)*z_l + pre_z_g;
    return Z_T;
}

double hapticRenderingQP::compute_phi_x_T_over_phi_qg4(double x_l, double y_l, double z_l)
{
    double res = (cos(pre_alpha_g)*sin(pre_beta_g)*cos(pre_gamma_g) + sin(pre_alpha_g)*sin(pre_gamma_g))*y_l 
                +(-cos(pre_alpha_g)*sin(pre_beta_g)*sin(pre_gamma_g) + sin(pre_alpha_g)*cos(pre_gamma_g))*z_l;
    return res;
}

double hapticRenderingQP::compute_phi_y_T_over_phi_qg4(double x_l, double y_l, double z_l)
{
    double res = (sin(pre_alpha_g)*sin(pre_beta_g)*cos(pre_gamma_g) - cos(pre_alpha_g)*sin(pre_gamma_g))*y_l 
                +(-sin(pre_alpha_g)*sin(pre_beta_g)*sin(pre_gamma_g) - cos(pre_alpha_g)*cos(pre_gamma_g))*z_l;
    return res;
}

double hapticRenderingQP::compute_phi_z_T_over_phi_qg4(double x_l, double y_l, double z_l)
{
    double res = cos(pre_beta_g)*cos(pre_gamma_g)*y_l - cos(pre_beta_g)*sin(pre_gamma_g)*z_l;
    return res;
}


double hapticRenderingQP::compute_phi_x_T_over_phi_qg5(double x_l, double y_l, double z_l)
{
    double res = -cos(pre_alpha_g)*sin(pre_beta_g)*x_l + cos(pre_alpha_g)*cos(pre_beta_g)*sin(pre_gamma_g)*y_l
                + cos(pre_alpha_g)*cos(pre_beta_g)*cos(pre_gamma_g)*z_l;
    return res;
}

double hapticRenderingQP::compute_phi_y_T_over_phi_qg5(double x_l, double y_l, double z_l)
{
    double res = -sin(pre_alpha_g)*sin(pre_beta_g)*x_l + sin(pre_alpha_g)*cos(pre_beta_g)*sin(pre_gamma_g)*y_l
                + sin(pre_alpha_g)*cos(pre_beta_g)*cos(pre_gamma_g)*z_l;
    return res;
}

double hapticRenderingQP::compute_phi_z_T_over_phi_qg5(double x_l, double y_l, double z_l)
{
    double res = -cos(pre_beta_g)*x_l - sin(pre_beta_g)*sin(pre_gamma_g)*y_l - sin(pre_beta_g)*cos(pre_gamma_g)*z_l;
    return res;
}


double hapticRenderingQP::compute_phi_x_T_over_phi_qg6(double x_l, double y_l, double z_l)
{
    double res = -sin(pre_alpha_g)*cos(pre_beta_g)*x_l - (sin(pre_alpha_g)*sin(pre_beta_g)*sin(pre_gamma_g) + cos(pre_alpha_g)*cos(pre_gamma_g))*y_l
                + (-sin(pre_alpha_g)*sin(pre_beta_g)*cos(pre_gamma_g) + cos(pre_alpha_g)*sin(pre_gamma_g))*z_l;
    return res;
}

double hapticRenderingQP::compute_phi_y_T_over_phi_qg6(double x_l, double y_l, double z_l)
{
    double res = cos(pre_alpha_g)*cos(pre_beta_g)*x_l + (cos(pre_alpha_g)*sin(pre_beta_g)*sin(pre_gamma_g) - sin(pre_alpha_g)*cos(pre_gamma_g))*y_l
                + (cos(pre_alpha_g)*sin(pre_beta_g)*cos(pre_gamma_g) + sin(pre_alpha_g)*sin(pre_gamma_g))*z_l;
    return res;
}

double hapticRenderingQP::compute_phi_z_T_over_phi_qg6(double x_l, double y_l, double z_l)
{
    return 0.;
}


void hapticRenderingQP::resetQPMatrices()
{
    G_.clear();
    h_.clear();
    P_ << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;
    q_ << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
}

void hapticRenderingQP::generateMotionHistoryConstraints()
{
    /* append G_ */
    Eigen::VectorXd g(6);
    g << 1.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 1.0, 0.0, 0.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 1.0, 0.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 0.0, 1.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 0.0, 0.0, 1.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 0.0, 0.0, 0.0, 1.0;
    G_.push_back(g);

    
    g << -1.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, -1.0, 0.0, 0.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, -1.0, 0.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 0.0, -1.0, 0.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 0.0, 0.0, -1.0, 0.0;
    G_.push_back(g);

    g << 0.0, 0.0, 0.0, 0.0, 0.0, -1.0;
    G_.push_back(g);

    /* append h_ */
    double delta = 0.01;
    h_.push_back(haptic_cfg_[0] + delta);
    h_.push_back(haptic_cfg_[1] + delta); 
    h_.push_back(haptic_cfg_[2] + delta);
    h_.push_back(haptic_cfg_[3] + delta);
    h_.push_back(haptic_cfg_[4] + delta);
    h_.push_back(haptic_cfg_[5] + delta);

    h_.push_back(-pre_x_g + delta);
    h_.push_back(-pre_y_g + delta); 
    h_.push_back(-pre_z_g + delta);
    h_.push_back(-pre_gamma_g + delta);
    h_.push_back(-pre_beta_g + delta);
    h_.push_back(-pre_alpha_g + delta);
}

std::vector<double> hapticRenderingQP::solveQP()
{
    /* Add constraints based on motion history */
    // generateMotionHistoryConstraints();
    
    /* formulate QP */
    ramp::qp qp_query;
    int ind = 0;
    for (const auto& g : G_)
    {
        std_msgs::Float64MultiArray tmp;
        
        /* 
            Renormalization:
            - Each row of A[i] and b[i] are renormalized by sum(abs(A[i])) -> not used for now
            - Each row of G[i] and h[i] are renormalized by sum(abs(G[i]))
            - P and q are renormalized with the spectral norm of P. -> not used for now
        */
        double cur_row_abs_sum = std::abs(g[0]) + std::abs(g[1]) + std::abs(g[2]) + std::abs(g[3]) + std::abs(g[4]) + std::abs(g[5]);
        tmp.data = std::vector<double>{ g[0]/cur_row_abs_sum, 
                                        g[1]/cur_row_abs_sum, 
                                        g[2]/cur_row_abs_sum, 
                                        g[3]/cur_row_abs_sum, 
                                        g[4]/cur_row_abs_sum, 
                                        g[5]/cur_row_abs_sum};
        qp_query.request.G_mat.push_back(tmp);

        h_[ind] = h_[ind]/cur_row_abs_sum;
        ++ind;
    }
    qp_query.request.h_vec.data = h_;
    // set stiffness coefficients
    double k_t = 1.0;
    double k_r = 1.0;
    qp_query.request.P_vec.data = std::vector<double>{k_t*P_[0], k_t*P_[1], k_t*P_[2], k_r*P_[3], k_r*P_[4], k_r*P_[5]};
    qp_query.request.q_vec.data = std::vector<double>{k_t*q_[0], k_t*q_[1], k_t*q_[2], k_r*q_[3], k_r*q_[4], k_r*q_[5]};

    
    if(qp_srv_client_.call(qp_query))
    {
        // ROS_INFO("QP solution is (graphic tool cfg)");
        // for (const auto& t : qp_query.response.graphic_tool_cfg.data)
        //     std::cout << t << " ";
        // std::cout << std::endl;
        return qp_query.response.graphic_tool_cfg.data;
    }
    else
    {
        ROS_ERROR("Failed to solve qp via service. Return previous graphic tool cfg.");
        return std::vector<double>{pre_x_g, pre_y_g, pre_z_g, pre_gamma_g, pre_beta_g, pre_alpha_g};
    }
}
