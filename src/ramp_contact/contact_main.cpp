#include <ros/ros.h>
#include "ramp_abb/ramp_abb_test.h"
#include "ramp_contact/ramp_contact.h"

/* 
 * \brief global variables: ramp parameters and experiment stats
 */
rampParameters ramp_params;
statistics stats;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_contact_node");
	ros::NodeHandle n;
	ROS_INFO("**********************************");
	ROS_INFO("RAMP-Contact node running.");
	ROS_INFO("**********************************");
 
	rampContact rc;
	tf::TransformListener tf_listener;

	/* 
		Initial estimation of hole pose
		tf_hat_fromt_hole_to_base
	 */
	Eigen::Affine3d tf_hat_from_hole_to_base;
	Eigen::Matrix3d rot;
	rot <<  0.0, 1.0, 0.0,
		   -1.0, 0.0, 0.0,
			0.0, 0.0, 1.0;
	tf_hat_from_hole_to_base = rot;
	tf_hat_from_hole_to_base.translation() = Eigen::Vector3d(0.55, 0.0, 0.45);

	/* 
		perturb tf_hat_fromt_hole_to_base a little bit to create
		some orientational uncertainty
		gt means gro                und truth
	 */
	Eigen::Matrix3d rot_uncert = (Eigen::AngleAxisd(rampUtil::convertDegToRad(0.0), Eigen::Vector3d::UnitX())
								* Eigen::AngleAxisd(rampUtil::convertDegToRad(0.0), Eigen::Vector3d::UnitY())
								* Eigen::AngleAxisd(rampUtil::convertDegToRad(0.0), Eigen::Vector3d::UnitZ())).matrix();
	Eigen::Affine3d tf_uncert;
	tf_uncert = rot_uncert;
	tf_uncert.translation() = Eigen::Vector3d(0.0, 0.0, 0.0);
	Eigen::Affine3d tf_gt_from_hole_to_base = tf_hat_from_hole_to_base * tf_uncert;
	rc.movePassiveObjectToPose(tf_gt_from_hole_to_base);
	
	/* 
		Insertion loop starts
	 */
	bool is_inserted = false;
	/* 
		fixed transforms
	 */
	// tf1
	tf::StampedTransform transform_passive_arm_base_to_active_arm_base;
	tf_listener.lookupTransform("/panda_link0", "/base_link", ros::Time(0), transform_passive_arm_base_to_active_arm_base);
	Eigen::Affine3d tf_from_passive_arm_base_to_active_arm_base;
	rampUtil::convertTfTransformtoAffine3d(transform_passive_arm_base_to_active_arm_base, tf_from_passive_arm_base_to_active_arm_base);

	// tf2
	Eigen::Affine3d tf_from_active_object_to_passive_object;
	rot <<  1.0, 0.0, 0.0,
			0.0, -1.0, 0.0,
			0.0, 0.0, -1.0;
	tf_from_active_object_to_passive_object = rot;
	tf_from_active_object_to_passive_object.translation() = Eigen::Vector3d(0.0, 0.0, 0.06);

	// initial tf_from_hole_to_world_base
	Eigen::Affine3d tf_hat_from_hole_to_active_arm_base = tf_from_passive_arm_base_to_active_arm_base * tf_hat_from_hole_to_base;
	// Eigen::Affine3d tf_hat_from_hole_to_active_arm_base = tf_from_passive_arm_base_to_active_arm_base * tf_gt_from_hole_to_base;
	
	rc.setTfHatHoleInWorldBase(tf_hat_from_hole_to_active_arm_base);

	/* *************************************
		Program mode 1
		Collect training data 
	 * ************************************/
	// Eigen::Affine3d tf_from_active_object_to_base;
	// tf_from_active_object_to_base = tf_hat_from_hole_to_active_arm_base * tf_from_active_object_to_passive_object;
	// rc.moveActiveObjectToPose(tf_from_active_object_to_base);
	// sleep(5.0);
	// rc.collectFmeaFsimData();
	// rc.collectFsimData();

	/* *************************
		Program mode 2
		Enter interaction loop -> not used
	 * *************************/
	// int cnt = 0;
	// while (ros::ok() && !is_inserted)
	// {
	// 	/* 
	// 		find peg corresponding pose to pre-insert
	// 		based on current tf_hat_from_hole_to_active_arm_base
	// 	*/
	// 	Eigen::Affine3d tf_from_active_object_to_base;
	// 	if (cnt == 0)
	// 	 	tf_from_active_object_to_base = tf_hat_from_hole_to_active_arm_base * tf_from_active_object_to_passive_object;
	// 	else
	// 		tf_from_active_object_to_base = rc.getCurBestEstPasObjInWorldBase() * tf_from_active_object_to_passive_object;
	// 	rc.moveActiveObjectToPose(tf_from_active_object_to_base);
		
	// 	/* 
	// 		panda robot insert once
	// 	*/
	// 	tf::Vector3 obj_moving_dir_in_obj_link(0., 0., 1.); // always along z-direction of object frame
	// 	trajectoryExecutionResult ter = rc.insertOnce(obj_moving_dir_in_obj_link, 110, 0.02, 0.02);

	// 	// !!!!!!!!!!!!!!!!!!!!!!!!!!!
	// 	ter.state = "ABORTED"; 
	// 	// rc.contactReasonOnceViaHapticRendering(ter);
	// 	// !!!!!!!!!!!!!!!!!!!!!!!!!!!

	// 	if(ter.state == "SUCCEEDED")
	// 	{
	// 		is_inserted = true;
	// 		ROS_INFO("Insertion succeeded!");
	// 	}
	// 	else if (ter.state == "ABORTED")
	// 	{
	// 		/* move back robot */
	// 		rc.recoverFromErrorOnce();
	// 		tf::Vector3 reversed_dir(-obj_moving_dir_in_obj_link.x(), -obj_moving_dir_in_obj_link.y(), -obj_moving_dir_in_obj_link.z());
	// 		rc.insertOnce(reversed_dir, 60);

	// 		/* 
	// 			panda robot contact reason once
	// 			update tf_hat_from_hole_to_active_arm_base
	// 		*/
	// 		rc.contactReasonOnceViaHapticRendering(ter);
	// 	}
	// 	else
	// 		ROS_WARN("Undefined trajectory execution state");
	// 	++cnt;

	// 	ROS_INFO("Press Enter to continue");
	// 	getchar();
	// 	getchar();
	// }

	/* *************************
		Program mode 3
		Compliant insertion via contact
		state transitions
	 * *************************/
	/* 
		move active arm to pre-insert nominal pose
	 */
	Eigen::Affine3d tf_from_active_object_to_base;
	Eigen::Vector3d gt_uncert_rpy, pred_uncert_rpy;

	/* 
		case 0: nominal pose -> direct insertion is possible if calibrated well 
	*/
	// tf_from_active_object_to_passive_object.translation() = Eigen::Vector3d(0.002, -0.0015, 0.06);
	// tf_from_active_object_to_base = tf_hat_from_hole_to_active_arm_base * tf_from_active_object_to_passive_object;
	// rc.moveActiveObjectToPose(tf_from_active_object_to_base);

	// gt_uncert_rpy <<   0.0, 0.0, 0.0;
	// pred_uncert_rpy << 0.0, 0.0, 0.0;
	// Eigen::Vector3d pos_offset;
	// pos_offset << 0.002, -0.0015, 0.0;
	// rc.directInsertion(gt_uncert_rpy, pred_uncert_rpy, pos_offset);

	/* 
		case 1: pred is very close to gt -> direct insertion is ok 
	*/
	// tf_from_active_object_to_passive_object.translation() = Eigen::Vector3d(0.002, -0.002, 0.06);
	// tf_from_active_object_to_base = tf_hat_from_hole_to_active_arm_base * tf_from_active_object_to_passive_object;
	// rc.moveActiveObjectToPose(tf_from_active_object_to_base);

	// gt_uncert_rpy <<   7.0, 1.0, 9.0;
	// pred_uncert_rpy << 3.0, 1.0, 9.0; // yaw corrected from 5.0 to 9.0
	// Eigen::Vector3d pos_offset;
	// pos_offset << 0.002, -0.002, 0.0;
	// rc.directInsertion(gt_uncert_rpy, pred_uncert_rpy, pos_offset);

	/* 
		case 2: pred is a bit far from gt -> insertion with contact state transition is ok 
	*/
	Eigen::Vector3d pos_offset;
	bool direct_insert = false;
	int test_case = 12;
	switch(test_case)
	{
		/* below for 2-pin */
		case 1:
			/* test 1 */
			pos_offset << -0.001, -0.003, 0.0;
			gt_uncert_rpy <<   9.0, -13.0, -1.0;
			pred_uncert_rpy << 7.0, -15.0, -1.0; 
			break;
		case 2:
			/* test 2 */
			pos_offset << 0.001, -0.003, 0.0;
			gt_uncert_rpy << 9.0, 3.0, -1.0;
			pred_uncert_rpy << 5.0, 5.0, -1.0; 
			break;
		case 3:
			/* test 3 */
			pos_offset << -0.001, -0.003, 0.0;
			gt_uncert_rpy << 11.0, -11.0, -1.0;
			pred_uncert_rpy << 13.0, -15.0, -1.0; 
			break;
		case 4:
			/* test 4 */
			pos_offset << -0.001, -0.003, 0.0;
			gt_uncert_rpy << 9.0, -9.0, -1.0;
			pred_uncert_rpy << 9.0, -7.0, -1.0; 
			break;
		case 5:
			/* test 5 */
			pos_offset << -0.001, -0.001, 0.0;
			gt_uncert_rpy << -11.0, -7.0, -1.0;
			pred_uncert_rpy << -15.0, -3.0, -1.0; 
			break;
		case 6:	
			/* test 6 */
			pos_offset << 0.001, -0.003, 0.0;
			gt_uncert_rpy << 9.0, 1.0, -1.0;
			pred_uncert_rpy << 5.0, 1.0, -1.0; 
			break;
		case 7:
			/* test 7 */
			pos_offset << 0.001, -0.00, 0.0;
			gt_uncert_rpy << -13.0, 7.0, -1.0;
			pred_uncert_rpy << -15.0, 1.0, -1.0; 
			break;
		case 8:
			/* test 8 */
			pos_offset << -0.00, -0.001, 0.0;
			gt_uncert_rpy << -7.0, -13.0, 11.0;
			pred_uncert_rpy << -13.0, -7.0, 11.0; 
			break;
		case 9:
			/* test 9 */
			pos_offset << 0.001, -0.004, 0.0;
			gt_uncert_rpy << 7.0, 3.0, -9.0;
			pred_uncert_rpy << 9.0, 11.0, -9.0; 
			break;
		case 10:
			/* test 10 */
			pos_offset << -0.001, -0.001, 0.0;
			gt_uncert_rpy << -5.0, -7.0, -15.0;
			pred_uncert_rpy << -15.0, -5.0, -15.0; 
			break;

		/* below for 3-pin */
		case 11:
			/* recorded */
			pos_offset << -0.00, -0.00, 0.0;
			gt_uncert_rpy << -13.0, 9.0, 0.0;
			pred_uncert_rpy << -15.0, 3.0, 0.0; 
			break;

		case 12: 
			/* recorded */
			pos_offset << -0.00, -0.002, 0.0;
			gt_uncert_rpy << 9.0, -7.0, 0.0;
			pred_uncert_rpy << 13.0, -3.0, 0.0; 
			break;
	}

	tf_from_active_object_to_passive_object.translation() = pos_offset; 
	tf_from_active_object_to_passive_object.translation()[2] = 0.06; // starting height
	tf_from_active_object_to_base = tf_hat_from_hole_to_active_arm_base * tf_from_active_object_to_passive_object;
	rc.moveActiveObjectToPose(tf_from_active_object_to_base);

	if (direct_insert)
		rc.directInsertion(gt_uncert_rpy, pred_uncert_rpy, pos_offset);
	else
	{
		// pos_offset << 0.003, -0.000, 0.025;
		pos_offset << 0.0, -0.0, 0.0;
		int compliant_option = 2;
		rc.compliantInsertionViaContactStateTransitions(gt_uncert_rpy, pred_uncert_rpy, pos_offset, compliant_option);
	}

	/* 
		case 3: pred is too off from gt -> future work 
	*/

	ros::waitForShutdown();
	return 0;
}

