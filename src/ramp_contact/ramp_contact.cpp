#include "ramp_contact/ramp_contact.h"
#include "ramp/ik.h"
#include <franka_control/SetLoad.h>
#include <franka_control/SetForceTorqueCollisionBehavior.h>

rampContact::rampContact()
: arm1("panda_arm", ""), tf_listener(), fol_jnt_traj_action_client("/position_joint_trajectory_controller/follow_joint_trajectory", true),
  abb_fol_jnt_traj_action_client_("/abb/joint_trajectory_action", true),
  on_hardware_(false), load_gripper_driver_(false), haptic_qp_(), gripper_move_action_client_("/franka_gripper/move/", true), 
  gripper_homing_action_client_("/franka_gripper/homing/", true), gripper_stop_action_client_("/franka_gripper/stop/",true),
  gripper_grasp_action_client_("/franka_gripper/grasp/", true)
{
    ros::NodeHandle nh;
    /* 
        update on_hardware_ parameter
     */
    if(nh.hasParam("/ramp/on_hardware")) 
	{
		nh.getParam("/ramp/on_hardware", on_hardware_);
		std::cout <<"\non_hardware: "<< on_hardware_;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/on_hardware");

    if(nh.hasParam("/ramp/gripper")) 
	{
		nh.getParam("/ramp/gripper", load_gripper_driver_);
		std::cout <<"\nload_gripper_driver: "<< load_gripper_driver_;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/gripper");

    /* 
        set up action interface to control hardware
     */
    if (on_hardware_)
    {
        while(!fol_jnt_traj_action_client.waitForServer(ros::Duration(5.0)))
            ROS_ERROR("Waiting for the joint_trajectory_action server on Franka panda hardware");
        
        while(!abb_fol_jnt_traj_action_client_.waitForServer(ros::Duration(5.0)))
            ROS_ERROR("Waiting for the joint_trajectory_action server on ABB hardware");

        if (load_gripper_driver_)
        {
            while(!gripper_move_action_client_.waitForServer(ros::Duration(5.0)))
                ROS_ERROR("Waiting for the gripper move action server on hardware");

            while(!gripper_homing_action_client_.waitForServer(ros::Duration(5.0)))
                ROS_ERROR("Waiting for the gripper homing action server on hardware");

            while(!gripper_stop_action_client_.waitForServer(ros::Duration(5.0)))
                ROS_ERROR("Waiting for the gripper stop action server on hardware");

            while(!gripper_grasp_action_client_.waitForServer(ros::Duration(5.0)))
                ROS_ERROR("Waiting for the gripper grasp action server on hardware");
            sleep(1.0); //sleep to set up the connection
        }
    }

    /* 
        publishers and subscribers
     */
    franka_error_recov_pub = nh.advertise<franka_control::ErrorRecoveryActionGoal>("/franka_control/error_recovery/goal", 100);
    cancel_traj_follow_pub = nh.advertise<actionlib_msgs::GoalID>("/position_joint_trajectory_controller/follow_joint_trajectory/cancel", 100);
    set_load_srv_client = nh.serviceClient<franka_control::SetLoad>("/franka_control/set_load");
    set_ft_col_srv_client = nh.serviceClient<franka_control::SetForceTorqueCollisionBehavior>("/franka_control/set_force_torque_collision_behavior");
    max_wrench_.max_wrench_ << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    wrench_state_sub = nh.subscribe("/franka_state_controller/F_ext", 1, &rampContact::wrenchStateCallback, this);
    franka_state_sub = nh.subscribe("/franka_state_controller/franka_states", 1, &rampContact::frankaStateCallback, this);
    if(on_hardware_) 
        joint_state_sub = nh.subscribe("/franka_state_controller/joint_states", 1, &rampContact::jointStateCallback, this);
    else
        joint_state_sub = nh.subscribe("/joint_states", 1, &rampContact::jointStateCallback, this);
    passive_robot_joint_cfg_pub = nh.advertise<sensor_msgs::JointState>("/abbArm/move_group/fake_controller_joint_states", 100); 
    ik_srv_client = nh.serviceClient<ramp::ik>("/abbArm/ik_query");
    visual_tools.reset(new moveit_visual_tools::MoveItVisualTools("panda_link0"));

    sleep(5.0); // wait for rviz to set up
    arm1.drawRobotArmInRviz();
    arm1.rvizVisualTrigger();

    // set panda robot load
    // setLoad(); // load can be set in DESK web interface
    setForceTorqueCollisionThresholds();

    /************************************************/
    /*****************COUPLINGS**********************/
    /************************************************/
    /* add abb arm coupling */
    /* 
        abb coupling has scaling problem to visualize 
        so I just use franka coupling to visually replace it 
        since it is modified from abb coupling 
    */
	// std::string object_mesh_path("package://ramp/mesh/couplings/franka-panda-coupling.stl"); 
    // std::string object_id("abb_coupling");
    // moveit_msgs::ObjectColor color;
	// color.id = object_id;
	// color.color.r = 1.0; color.color.g = 1.0;
	// color.color.b = 1.0; color.color.a = 1.0;
	// Eigen::Vector3d inch_to_m(1.0/39.37, 1.0/39.37, 1.0/39.37);
	// arm1.addSceneObject("link_6", "abb_coupling_link", false, object_mesh_path, inch_to_m, object_id, color);

    /* add panda arm coupling */
	std::string object_mesh_path = "package://ramp/mesh/couplings/franka-panda-coupling.stl"; 
    std::string object_id = "panda_coupling";
	moveit_msgs::ObjectColor color;
    color.id = object_id;
    color.color.r = 1.0; color.color.g = 1.0;
	color.color.b = 1.0; color.color.a = 1.0;
	Eigen::Vector3d inch_to_m(1.0/39.37, 1.0/39.37, 1.0/39.37);
	// arm1.addSceneObject("panda_link8", "panda_coupling_link", true, object_mesh_path, inch_to_m, object_id, color);

    /*************************************************/
    /*****************PASSIVE OBJECT******************/
    /*****************ABB OBJECT**********************/
    /*************************************************/
    std::vector<double> pose_vec_in_abb_eef(6, 0.0);
	pose_vec_in_abb_eef[0] = 0.05; pose_vec_in_abb_eef[1] = 0.0;  pose_vec_in_abb_eef[2] = 0.0;
	pose_vec_in_abb_eef[3] = 0.0; pose_vec_in_abb_eef[4] = 1.57; pose_vec_in_abb_eef[5] = -1.57;

    passive_obj.parent_link = "link_6";
    passive_obj.object_link = "passive_object_link";
    passive_obj.pose_vec = pose_vec_in_abb_eef;
    rampUtil::convertXYZRPYtoTfTransform(pose_vec_in_abb_eef, passive_obj.tf_from_object_link_to_parent_link);

    passive_obj.object_id = 1;
    passive_obj.mesh_scale = 1.0/39.37;
    passive_obj.color = rviz_visual_tools::colors::BROWN;
    std::string pkg_path = ros::package::getPath("ramp");
	
    /* Two pin */
    // passive_obj.mesh_path = "package://ramp/mesh/2_pin/female-2-pin-with-rail.stl"; 
	// std::string sp_tree_path = pkg_path+"/sphere_trees/2_pin/octree-depth6/female-2-pin-octree.sph";

    /* Three pin */
    passive_obj.mesh_path = "package://ramp/mesh/3_pin/female-3-pin-with-rail.stl"; 
	std::string sp_tree_path = pkg_path+"/sphere_trees/3_pin/octree-depth5/female-3-pin-octree.sph";
    
    std::vector<double> pose_vec_from_spt_link_to_mesh_link(6, 0.0);
    pose_vec_from_spt_link_to_mesh_link[2] = -0.03;
    passive_obj.sphere_tree = std::make_shared<sphereTree>(sp_tree_path, pose_vec_from_spt_link_to_mesh_link);

    std::vector<double> pose_vec_parent_link_to_eef_link(6, 0.0); // fixed identity transformation
    tf::Transform tf;
    rampUtil::convertXYZRPYtoTfTransform(pose_vec_parent_link_to_eef_link, tf);
    passive_obj.tf_from_parent_link_to_arm_eef_link = tf;

	arm1.drawObjectInRviz(passive_obj.parent_link, passive_obj.pose_vec, passive_obj.mesh_path, passive_obj.mesh_scale, passive_obj.object_id, passive_obj.color);
    // arm1.drawFrameInRviz(passive_obj.parent_link, passive_obj.pose_vec, passive_obj.object_link);

    /*************************************************/
    /*****************ACTIVE OBJECT*******************/
    /*****************Panda OBJECT********************/
    /*************************************************/
    std::vector<double> pose_vec_in_panda_eef(6, 0.0);
	pose_vec_in_panda_eef[0] = 0.0; pose_vec_in_panda_eef[1] = 0.0;  pose_vec_in_panda_eef[2] = 0.05;
	pose_vec_in_panda_eef[3] = 0.0; pose_vec_in_panda_eef[4] = 0.0; pose_vec_in_panda_eef[5] = 3.14;

    active_obj.parent_link = "panda_hand_dummy_link";
    active_obj.object_link = "active_object_link";
    active_obj.pose_vec = pose_vec_in_panda_eef;
    rampUtil::convertXYZRPYtoTfTransform(pose_vec_in_panda_eef, active_obj.tf_from_object_link_to_parent_link);
    active_obj.object_id = 2; 
    active_obj.mesh_scale = 1.0/39.37;
    active_obj.color = rviz_visual_tools::colors::WHITE;
    
    /* Two pin */
    // active_obj.mesh_path = "package://ramp/mesh/2_pin/male-2-pin-with-rail.stl"; 
    // sp_tree_path = pkg_path+"/sphere_trees/2_pin/octree-depth6/male-2-pin-octree.sph";

    /* Three pin */
    active_obj.mesh_path = "package://ramp/mesh/3_pin/male-3-pin-with-rail.stl"; 
    sp_tree_path = pkg_path+"/sphere_trees/3_pin/octree-depth5/male-3-pin-octree.sph";

    pose_vec_from_spt_link_to_mesh_link = std::vector<double>(6, 0.0);
    active_obj.sphere_tree = std::make_shared<sphereTree>(sp_tree_path, pose_vec_from_spt_link_to_mesh_link);

    pose_vec_parent_link_to_eef_link[5] = 0.785; // this is a fixed transformation
    rampUtil::convertXYZRPYtoTfTransform(pose_vec_parent_link_to_eef_link, tf);
    active_obj.tf_from_parent_link_to_arm_eef_link = tf;

	// arm1.drawObjectInRviz(active_obj.parent_link, active_obj.pose_vec, active_obj.mesh_path, active_obj.mesh_scale, active_obj.object_id, active_obj.color);
    // arm1.drawFrameInRviz(active_obj.parent_link, active_obj.pose_vec, active_obj.object_link);
    // arm1.rvizVisualTrigger();

    /* 
        Contact reasoning parameters
     */
    act_sph_rad_scale_ = 1.0;
    pas_sph_rad_scale_ = 1.0;
    step_limit_of_simulated_fvrt_ = 20; // maximum step_limit_of_simulated_fvrt_ * 1mm  penetration depth
	use_level_x_sphere_tree_ = 5; 
	rotation_res_ = 2.0;
    x_rot_range_ = {-15.0, 15.5};    
    y_rot_range_ = {-15.0, 15.5}; // {-20.0, 21.0};
    z_rot_range_ = {-0.0, 0.5};
    use_force_reasoing_only_  = true; // experience shows that only force works better
    use_ft_avg_reasoning_ = false; // experience shows that ft_max works better than ft_avg
    exe_timeout_ = 20.0; 
    sp_rad_delta_ = 0.005; // 5mm
    stop_contact_reason_at_each_pose_ = true;
}

std::string rampContact::moveActiveObjectToPose(const Eigen::Affine3d& tf_from_object_to_base, double acc_scale_factor, double vel_scale_factor)
{
    // find tf from from pand_link8 to panda_link0 given tf from object link to panda_link0
    tf::Transform transform_from_object_to_base;
    rampUtil::convertAffine3dtoTfTransform(tf_from_object_to_base, transform_from_object_to_base);

    tf::Transform tf_from_object_to_parent_link = active_obj.tf_from_object_link_to_parent_link;
    // rampUtil::convertXYZRPYtoTfTransform(active_obj.pose_vec, tf_from_object_to_parent_link);

    tf::Transform tf_from_parent_link_to_base = transform_from_object_to_base * 
                                                (active_obj.tf_from_parent_link_to_arm_eef_link * tf_from_object_to_parent_link).inverse();
    geometry_msgs::Pose pose_msg; // eef link pose in robot base link
    rampUtil::convertTfTransformtoMsg(tf_from_parent_link_to_base, pose_msg);
    
    /* plan arm motion */
    moveit_msgs::RobotTrajectory traj;
    armCfg cur_cfg = getRobotCurrentCfg();
    ROS_INFO("Moving panda eef to pose (set point):");
    rampUtil::printGeoMsgPose(pose_msg);
    arm1.moveFromCurrentJntCfgToGivenPose(cur_cfg, pose_msg, traj, acc_scale_factor, vel_scale_factor);
    
    /* move arm hardware */
    std::string exe_hw_state;
    if (on_hardware_)
    {
        exe_hw_state = executeTrajectoryOnHardware(traj); // blocking
        cur_cfg = getRobotCurrentCfg();
    }
    else
        cur_cfg = traj.joint_trajectory.points.back().positions;

    cur_cfg = getRobotCurrentCfg();
    Eigen::Affine3d tf_eef;
    arm1.getEefPose(cur_cfg, tf_eef);
    geometry_msgs::Pose pose_eef_msg;
    rampUtil::convertAffine3dtoQuaternion(tf_eef, pose_eef_msg);
    ROS_INFO("Moved panda eef to pose (actual reached point):");
    rampUtil::printGeoMsgPose(pose_eef_msg);

    /* move arm in simulation */
    arm1.renderJointConfiguration(cur_cfg);
	arm1.drawObjectInRviz(active_obj.parent_link, active_obj.pose_vec, active_obj.mesh_path, active_obj.mesh_scale, active_obj.object_id, active_obj.color);
    // arm1.drawFrameInRviz(active_obj.parent_link, active_obj.pose_vec, active_obj.object_link);
    arm1.rvizVisualTrigger();
    return exe_hw_state;
}

bool rampContact::movePassiveObjectToPose(const Eigen::Affine3d& tf_from_object_to_base, bool move_hardware)
{
    tf::Transform transform_from_object_to_base;
    rampUtil::convertAffine3dtoTfTransform(tf_from_object_to_base, transform_from_object_to_base);

    tf::Transform tf_from_object_to_parent_link;
    rampUtil::convertXYZRPYtoTfTransform(passive_obj.pose_vec, tf_from_object_to_parent_link);

    tf::Transform tf_from_parent_link_to_base = transform_from_object_to_base * 
                                                (passive_obj.tf_from_parent_link_to_arm_eef_link * tf_from_object_to_parent_link).inverse();
    geometry_msgs::Pose pose_msg; // eef link pose in robot base link
    rampUtil::convertTfTransformtoMsg(tf_from_parent_link_to_base, pose_msg);

    // find ik
    ramp::ik ik;
    ik.request.pose = pose_msg;
    if(ik_srv_client.call(ik))
    {
        ROS_INFO("Moving abb eef to pose:");
        rampUtil::printGeoMsgPose(pose_msg);
        ROS_INFO("Arm ik solution is");
        rampUtil::printArmCfg(ik.response.arm_cfg.data);
        
        // actual movement on harware
        if (move_hardware)
        {
            moveABBHardwareToCfg(ik.response.arm_cfg.data);
        }
        
        // actual movement in simulation
        sensor_msgs::JointState joint_state;
        joint_state.header.stamp = ros::Time::now();
        std::vector<std::string> jnt_names{"joint_1", "joint_2", "joint_3", "joint_4", "joint_5", "joint_6"};
        joint_state.name = jnt_names;
        joint_state.position = ik.response.arm_cfg.data;
        passive_robot_joint_cfg_pub.publish(joint_state);
        ros::spinOnce();
        sleep(1.0);

        // redraw abb arm and abb object
        arm1.drawRobotArmInRviz();
        arm1.drawObjectInRviz(passive_obj.parent_link, passive_obj.pose_vec, passive_obj.mesh_path, passive_obj.mesh_scale, passive_obj.object_id, passive_obj.color);
        // arm1.drawFrameInRviz(passive_obj.parent_link, passive_obj.pose_vec, passive_obj.object_link);
        arm1.rvizVisualTrigger();
        return true;
    }
    else
    {
        ROS_ERROR("Failed to get IK via service");
        return false;
    }
}

void rampContact::drawActiveObjectSphereTrees()
{
    tf::StampedTransform tf_from_parent_link_to_world_base;
	tf_listener.lookupTransform("/panda_link0", active_obj.parent_link, ros::Time(0), tf_from_parent_link_to_world_base);
    tf::Transform tf_from_spt_to_world_base = tf_from_parent_link_to_world_base * active_obj.tf_from_object_link_to_parent_link;
    active_obj.sphere_tree->visualizeSpheresAtLevelX(5, tf_from_spt_to_world_base, active_obj.object_id, visual_tools);
}

void rampContact::drawPassiveObjectSphereTrees()
{
    tf::StampedTransform tf_from_parent_link_to_world_base;
	tf_listener.lookupTransform("/panda_link0", passive_obj.parent_link, ros::Time(0), tf_from_parent_link_to_world_base);
    tf::Transform tf_from_spt_to_world_base = tf_from_parent_link_to_world_base * passive_obj.tf_from_object_link_to_parent_link;
    passive_obj.sphere_tree->visualizeSpheresAtLevelX(5, tf_from_spt_to_world_base, passive_obj.object_id, visual_tools);
}

proximityQueryResult rampContact::spheresProximityQuery(const tf::Transform& active_obj_tf_from_parent_link_to_world_base,
                                                        const tf::Transform& passive_obj_tf_from_obj_link_to_world_base)
{
    std::vector<sphere> active_spheres, passive_spheres;
    /* 
        get spheres at level x of active object
     */
    // tf::StampedTransform tf_from_parent_link_to_world_base;
	// tf_listener.lookupTransform("/panda_link0", active_obj.parent_link, ros::Time(0), tf_from_parent_link_to_world_base);
    tf::Transform tf_from_spt_to_world_base = active_obj_tf_from_parent_link_to_world_base * active_obj.tf_from_object_link_to_parent_link;
    active_obj.sphere_tree->getSpheresAtLevelXInWorldBaseFrame(5, tf_from_spt_to_world_base, active_spheres);
    
    /* 
        get spheres at level x of passive object
     */
    // tf::StampedTransform passive_obj_tf_from_parent_link_to_world_base;
	// tf_listener.lookupTransform("/panda_link0", passive_obj.parent_link, ros::Time(0), passive_obj_tf_from_parent_link_to_world_base);
    // tf_from_spt_to_world_base = passive_obj_tf_from_parent_link_to_world_base * passive_obj.tf_from_object_link_to_parent_link;
    tf_from_spt_to_world_base = passive_obj_tf_from_obj_link_to_world_base;
    passive_obj.sphere_tree->getSpheresAtLevelXInWorldBaseFrame(5, tf_from_spt_to_world_base, passive_spheres);

    /* 
        proximity query
     */
    proximityQueryResult pqr;
    int m = active_spheres.size();
    int n = passive_spheres.size();
    for (int i=0; i<m; ++i)
    {
        for (int j=0; j<n; ++j)
        {
            double dist = rampUtil::get3dDistance(active_spheres[i].center, passive_spheres[j].center);
            if (dist < (active_spheres[i].radius + passive_spheres[j].radius))
            {
                pqr.is_colliding = true;
                pqr.closest_dist = 0.0;
                pqr.colliding_pairs.emplace_back(i, j);
            }
            pqr.closest_dist = std::min(pqr.closest_dist, dist);
        }
    }
    return pqr;
}

trajectoryExecutionResult rampContact::insertOnce(const tf::Vector3& obj_moving_dir_in_obj_link, int steps, double acc_scale_factor, double vel_scale_factor)
{
    ROS_INFO("[Insertion process started]");
    /* 
        step 1
        find eef moving direction in base link
     */
    tf::StampedTransform tf_from_parent_link_to_world_base;
	tf_listener.lookupTransform("/panda_link0", active_obj.parent_link, ros::Time(0), tf_from_parent_link_to_world_base);
    tf::Transform tf_from_object_link_to_base_link = tf_from_parent_link_to_world_base * active_obj.tf_from_object_link_to_parent_link;
    tf_from_object_link_to_base_link.setOrigin(tf::Vector3(0., 0., 0.));
    tf::Vector3 obj_moving_dir_in_base_link = tf_from_object_link_to_base_link * obj_moving_dir_in_obj_link;
    tf::Vector3 eef_moving_dir_in_base_link = obj_moving_dir_in_base_link; // because of rigid connection
    eef_moving_dir_in_base_link.normalize();
    ROS_INFO("Eef moving dir in base link is %f, %f, %f", eef_moving_dir_in_base_link.x(), eef_moving_dir_in_base_link.y(), eef_moving_dir_in_base_link.z());
    
    /* 
        step 2
        move robot eef using Jacobian until contact
     */
    int error_code = 0;
    moveit_msgs::RobotTrajectory traj;
    armCfg arm_cur_cfg;
    if (on_hardware_)
        arm_cur_cfg = getRobotCurrentCfg();
    else
    {
        arm_cur_cfg.resize(7,0.0);
        arm_cur_cfg[0] = -0.012; arm_cur_cfg[1] = -0.010; arm_cur_cfg[2] = 0.014;
        arm_cur_cfg[3] = -1.825; arm_cur_cfg[4] = -0.000; arm_cur_cfg[5] = 1.815;
        arm_cur_cfg[6] = 0.791; 
    }
    arm1.jacobianBasedMotionGeneration(arm_cur_cfg, eef_moving_dir_in_base_link, steps, error_code, traj, acc_scale_factor, vel_scale_factor);

    std::string state;
    if (on_hardware_)
        state = executeTrajectoryOnHardware(traj);
    else
        arm_cur_cfg = traj.joint_trajectory.points.back().positions;

    // arm1.renderJointConfiguration(arm_cur_cfg);
	// arm1.drawObjectInRviz(active_obj.parent_link, active_obj.pose_vec, active_obj.mesh_path, active_obj.mesh_scale, active_obj.object_id, active_obj.color);
    // arm1.drawFrameInRviz(active_obj.parent_link, active_obj.pose_vec, active_obj.object_link);
    // arm1.rvizVisualTrigger();
    
    /* 
        step 3
        return contact result
     */
    Eigen::VectorXd F_measured(6); 
    F_measured << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    tf::StampedTransform active_obj_tf_in_contact, tf_from_k_to_world_base;
    if (state == "SUCCEEDED")
        ROS_INFO("[Insertion process completed]");
    else if (state == "ABORTED")
    {
        // note down contact time and contact tf from active object parent link to base link
        contact_time_ = ros::Time::now();
        tf_listener.lookupTransform("/panda_link0", active_obj.parent_link, ros::Time(0), active_obj_tf_in_contact);
        tf_listener.lookupTransform("/panda_link0", "/panda_K", ros::Time(0), tf_from_k_to_world_base);

        cancelCurrentTrajectoryFollowGoal();

        /* process wrench msgs to extract F_measured */
        F_measured = extractWrenchDirections();
        ROS_WARN("[Insertion process aborted]");

        recoverFromErrorOnce();
    }
    else
        ROS_WARN("[Undefined trajectory execution state]");

    trajectoryExecutionResult exe_res;
    exe_res.traj = traj;
    exe_res.f_ext = F_measured;
    exe_res.active_obj_tf_from_parent_to_base_in_contact = active_obj_tf_in_contact;
    exe_res.tf_from_k_frame_to_world_base_in_contact = tf_from_k_to_world_base;
    exe_res.contact_time = contact_time_;
    exe_res.state = state;

    resetMaxWrench();
    return exe_res;
}

Eigen::VectorXd rampContact::extractWrenchDirections() const 
{
    /* 
        find wrench with largest magnitude 
    */
    Eigen::VectorXd tmp(6);
    if ((contact_time_ - max_wrench_.max_wrench_time_).toSec() < 30.0)
    {
        Eigen::VectorXd max_w = -max_wrench_.max_wrench_; // [xyz-torque, xyz-force]
        /* 
            transform from k frame to world frame 
        */
        tf::StampedTransform tf_from_k_frame_to_world_base;
        tf_listener.lookupTransform("/panda_link0", "/panda_K", ros::Time(0), tf_from_k_frame_to_world_base);
        
        Eigen::Affine3d tf_from_k_to_w_affine;
        rampUtil::convertTfTransformtoAffine3d(tf_from_k_frame_to_world_base, tf_from_k_to_w_affine);
        
        Eigen::Affine3d T_kw = tf_from_k_to_w_affine.inverse();
        Eigen::Matrix3d rot = T_kw.rotation();
        Eigen::Vector3d trans = T_kw.translation();
        Eigen::Matrix3d p_bracket;
        p_bracket << 0.0,      -trans[2],  trans[1],
                     trans[2],  0.0,      -trans[0],
                    -trans[1],  trans[0],  0.0;
        Eigen::Matrix3d pr = p_bracket*rot;
        
        Eigen::Matrix3d zeros(3, 3);
        zeros << 0.0, 0.0, 0.0,
                 0.0, 0.0, 0.0,
                 0.0, 0.0, 0.0;
        Eigen::MatrixXd Ad_T_kw(6, 6);
        Ad_T_kw << rot, zeros, pr, rot;

        Eigen::VectorXd max_w_in_world_frame = Ad_T_kw.transpose()*max_w;
        /* 
            swap force and torque component 
        */
        tmp << max_w_in_world_frame[3], max_w_in_world_frame[4], max_w_in_world_frame[5], 
               max_w_in_world_frame[0], max_w_in_world_frame[1], max_w_in_world_frame[2];

        /* 
            transform from k frame to active object frame 
        */
       tf::Transform tf_from_obj_to_hand_dummy = active_obj.tf_from_object_link_to_parent_link;
       tf::StampedTransform tf_from_dummy_to_k;
       tf_listener.lookupTransform("/panda_K", "/panda_hand_dummy_link", ros::Time(0), tf_from_dummy_to_k);
       tf::Transform tf_from_obj_to_k = tf_from_dummy_to_k * tf_from_obj_to_hand_dummy;

       Eigen::Affine3d tf_from_obj_to_k_affine;
       rampUtil::convertTfTransformtoAffine3d(tf_from_obj_to_k, tf_from_obj_to_k_affine);

       Eigen::Affine3d T_k_obj = tf_from_obj_to_k_affine;
       rot = T_k_obj.rotation();
       trans = T_k_obj.translation();
       p_bracket << 0.0,      -trans[2],  trans[1],
                    trans[2],  0.0,      -trans[0],
                    -trans[1],  trans[0],  0.0;
       pr = p_bracket*rot;
       Eigen::MatrixXd Ad_T_k_obj(6, 6);
       Ad_T_k_obj << rot, zeros, pr, rot;
       Eigen::VectorXd max_w_in_obj_frame = Ad_T_k_obj.transpose()*max_w;


        /* 
            debugs
         */
        std::cout << std::fixed << std::setprecision(3) << "Computed F_ext in panda K frame is (xyz-torque xyz-force) [" << max_w.transpose() << "]" << std::endl;
        std::cout << std::fixed << std::setprecision(3) << "Computed F_ext in panda base frame is (xyz-torque xyz-force) [" << max_w_in_world_frame.transpose() << "]" << std::endl;
        std::cout << std::fixed << std::setprecision(3) << "Computed F_ext in active obj frame is (xyz-torque xyz-force) [" << max_w_in_obj_frame.transpose() << "]" << std::endl;
        // for (const auto& s : franka_states_)
        // {
        //     Eigen::VectorXd tmp_O(6);
        //     tmp_O << s.O_F_ext_hat_K[3], s.O_F_ext_hat_K[4], s.O_F_ext_hat_K[5],
        //              s.O_F_ext_hat_K[0], s.O_F_ext_hat_K[1], s.O_F_ext_hat_K[2];
            
        //     Eigen::VectorXd tmp_k(6);
        //     tmp_k << s.K_F_ext_hat_K[3], s.K_F_ext_hat_K[4], s.K_F_ext_hat_K[5],
        //              s.K_F_ext_hat_K[0], s.K_F_ext_hat_K[1], s.K_F_ext_hat_K[2];

        //     Eigen::VectorXd diff = max_w_in_world_frame - tmp_O;
        //     if (diff.norm() < 1e-2)
        //     {
        //         std::cout << "matching wrench found!" << std::endl;
        //         std::cout << std::fixed << std::setprecision(3) << "Controller reported F_ext in K frame is [" << tmp_k.transpose() << "]" << std::endl;
        //         std::cout << std::fixed << std::setprecision(3) << "Controller reported F_ext in panda base frame is [" << tmp_O.transpose() << "]" << std::endl;
        //     }
            
        //     // ROS_INFO("K_F_ext_hat_k received [%f, %f, %f, %f, %f, %f]", s.K_F_ext_hat_K[3], s.K_F_ext_hat_K[4], s.K_F_ext_hat_K[5],
        //     //                                                             s.K_F_ext_hat_K[0], s.K_F_ext_hat_K[1], s.K_F_ext_hat_K[2]);
        //     // ROS_INFO("O_F_ext_hat_k received [%f, %f, %f, %f, %f, %f]", s.O_F_ext_hat_K[3], s.O_F_ext_hat_K[4], s.O_F_ext_hat_K[5],
        //     //                                                             s.O_F_ext_hat_K[0], s.O_F_ext_hat_K[1], s.O_F_ext_hat_K[2]);
        // }
    }
    else
    {   
        ROS_WARN("Max wrenched occurred out of time window.");
        tmp.setZero();
    }
    return tmp;
}

void rampContact::setTfHatHoleInWorldBase(const Eigen::Affine3d& tf_hat)
{
    tf_hat_from_hole_to_world_base_ = tf_hat;
    generatePassiveObjectCandidatePoses();
    // viewPassiveObjectCandidatePoses();
}

void rampContact::transformSpheres(const std::vector<sphere>& src_spheres, std::vector<sphere>& tgt_spheres, const tf::Transform& tf)
{
    for (int i=0; i<src_spheres.size(); ++i)
    {
        tf::Vector3 pt(src_spheres[i].center[0], src_spheres[i].center[1], src_spheres[i].center[2]);
        tf::Vector3 transformed_pt =tf * pt;
        sphere s(Eigen::Vector3d(transformed_pt.x(), transformed_pt.y(), transformed_pt.z()), src_spheres[i].radius);
        tgt_spheres.push_back(s);
    }
}

hapticRenderingResult rampContact::generateVirtualHapticResponse(const moveit_msgs::RobotTrajectory& rob_traj, const tf::Transform& cur_tf_hat_from_passive_obj_to_world_base)
{
    /* results collector */
    std::vector<Eigen::VectorXd> f_vrt;
    Eigen::VectorXd f_vrt_max(6);
    f_vrt_max.setZero();

    /* get fixed transform */
    tf::StampedTransform tf_from_parent_link_to_eef_link;
	tf_listener.lookupTransform("/panda_link8", active_obj.parent_link, ros::Time(0), tf_from_parent_link_to_eef_link);

    std::vector<sphere> active_spheres_in_spt_frame, passive_spheres_in_spt_frame;
    active_obj.sphere_tree->getSpheresAtLevelXInSphereTreeFrame(use_level_x_sphere_tree_, active_spheres_in_spt_frame, act_sph_rad_scale_);
    passive_obj.sphere_tree->getSpheresAtLevelXInSphereTreeFrame(use_level_x_sphere_tree_, passive_spheres_in_spt_frame, pas_sph_rad_scale_);

    /* 
        get scene object(hole) configuration
     */
    std::vector<sphere> active_spheres_in_world_frame, passive_spheres_in_world_frame;
    tf::Transform pas_obj_tf_obj_link_to_world_base = cur_tf_hat_from_passive_obj_to_world_base;
    transformSpheres(passive_spheres_in_spt_frame, passive_spheres_in_world_frame, pas_obj_tf_obj_link_to_world_base);

    std::vector<double> graphic_act_obj_pose_vec_obj_link_to_world_base_at_t_minus_1(6, 0.0);
    int simulated_steps_cnt = 0;
    for (int i=0; i<rob_traj.joint_trajectory.points.size(); ++i)
    {
        // ROS_INFO("Haptic rendering way point %d", i);
        armCfg cur_cfg = rob_traj.joint_trajectory.points[i].positions;

        /* 
            get haptic tool configuration
         */
        Eigen::Affine3d tf_from_eef_link_to_arm_base;
        arm1.getEefPose(cur_cfg, tf_from_eef_link_to_arm_base);

        // std::cout << "EEF position:[" << tf_from_eef_link_to_arm_base.translation()[0] << " " 
        //           << tf_from_eef_link_to_arm_base.translation()[1] << " " << tf_from_eef_link_to_arm_base.translation()[2] << "]" << std::endl;

        tf::Transform tf_from_panda_eef_link_to_panda_base;
        rampUtil::convertAffine3dtoTfTransform(tf_from_eef_link_to_arm_base, tf_from_panda_eef_link_to_panda_base);
        tf::Transform act_obj_tf_parent_link_to_world_base = tf_from_panda_eef_link_to_panda_base * tf_from_parent_link_to_eef_link;
        tf::Transform act_obj_tf_obj_link_to_world_base = act_obj_tf_parent_link_to_world_base * active_obj.tf_from_object_link_to_parent_link;
        std::vector<double> haptic_act_obj_pose_vec_obj_link_to_world_base;
        rampUtil::convertTfTransformtoXYZRPY(act_obj_tf_obj_link_to_world_base, haptic_act_obj_pose_vec_obj_link_to_world_base);
        
        /* 
            transform active spheres to world frame
         */
        active_spheres_in_world_frame.clear();
        transformSpheres(active_spheres_in_spt_frame, active_spheres_in_world_frame, act_obj_tf_obj_link_to_world_base);

        /* 
            initial proximity query to quickly decide if haptic tool is colliding with passive object
         */
        int m = active_spheres_in_world_frame.size();
        int n = passive_spheres_in_world_frame.size();
        bool is_colliding = false;
        std::vector<std::pair<sphere, sphere>> colliding_pairs;
        for (int i=0; i<m; ++i)
        {
            for (int j=0; j<n; ++j)
            {
                double dist = rampUtil::get3dDistance(active_spheres_in_world_frame[i].center, passive_spheres_in_world_frame[j].center);
                if (dist < (active_spheres_in_world_frame[i].radius + passive_spheres_in_world_frame[j].radius))
                {
                    is_colliding = true;
                    break;
                }
            }
            if (is_colliding)
                break;
        }

        /* 
            haptic rendering
         */
        if (!is_colliding)
        {
            // std::cout << "Spheres are not Colliding" << std::endl;
            graphic_act_obj_pose_vec_obj_link_to_world_base_at_t_minus_1 = haptic_act_obj_pose_vec_obj_link_to_world_base;
            Eigen::VectorXd zero_f(6);
            zero_f.setZero();
            f_vrt.push_back(zero_f);
            // ROS_INFO("Not in contact. Zero haptic response.");
        }
        else
        {
            // ROS_INFO("Solving QP to compute haptic response.");
            /* 
                proximity query between graphic tool and passive object
                (contact constraint prediction algorithm)
             */
            std::vector<sphere> graphic_active_spheres_in_world_frame;
            tf::Transform graphic_act_obj_tf_obj_link_to_world_base;
            rampUtil::convertXYZRPYtoTfTransform(graphic_act_obj_pose_vec_obj_link_to_world_base_at_t_minus_1, graphic_act_obj_tf_obj_link_to_world_base);
            transformSpheres(active_spheres_in_spt_frame, graphic_active_spheres_in_world_frame, graphic_act_obj_tf_obj_link_to_world_base);
            double radius_delta = sp_rad_delta_;
            enlargeSpheres(graphic_active_spheres_in_world_frame, radius_delta);

            for (int i=0; i<graphic_active_spheres_in_world_frame.size(); ++i)
            {
                for (int j=0; j<passive_spheres_in_world_frame.size(); ++j)
                {
                    double dist = rampUtil::get3dDistance(graphic_active_spheres_in_world_frame[i].center, passive_spheres_in_world_frame[j].center);
                    if (dist < (graphic_active_spheres_in_world_frame[i].radius + passive_spheres_in_world_frame[j].radius))
                    {
                        // std::cout << std::endl;
                        // std::cout << "[Querying stage]" << std::endl;
                        // std::cout << "act sp center is " << graphic_active_spheres_in_world_frame[i].center.transpose() << " radius is " << graphic_active_spheres_in_world_frame[i].radius << std::endl;
                        // std::cout << "pas sp center is " << passive_spheres_in_world_frame[j].center.transpose() << " radius is " << passive_spheres_in_world_frame[j].radius << std::endl;
                        // std::cout << "3dist is " << dist << std::endl;
                        // std::cout << "radii sum is " << (graphic_active_spheres_in_world_frame[i].radius + passive_spheres_in_world_frame[j].radius) << std::endl;
                        
                        /* make a QP constraint */
                        std::pair<sphere, sphere> col_pair = std::make_pair(sphere(active_spheres_in_spt_frame[i].center, active_spheres_in_spt_frame[i].radius),
                                                                            sphere(passive_spheres_in_world_frame[j].center, passive_spheres_in_world_frame[j].radius));
                        colliding_pairs.push_back(col_pair);
                    }
                }
            }
            /* 
                already satisfy non-penetration cstrs 
            */
            if (colliding_pairs.empty())
            {   
                ROS_WARN("Empty collding pairs by CCP algorithm.");
                continue;
            }
            /* 
                formulate QP
            */
            std::vector<double> graphic_act_obj_pose_vec_obj_link_to_world_base_at_t(6, 0.0);
            haptic_qp_.resetQPMatrices();
            haptic_qp_.setHapticCfg(haptic_act_obj_pose_vec_obj_link_to_world_base);
            haptic_qp_.setGraphicCfgPrevious(graphic_act_obj_pose_vec_obj_link_to_world_base_at_t_minus_1);
            haptic_qp_.setCollidingSpherePairs(colliding_pairs);
            graphic_act_obj_pose_vec_obj_link_to_world_base_at_t = haptic_qp_.solveQP();
            /* 
                compute virtual feedback force and torque
             */
            Eigen::Vector3d force;
            force[0] = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[0] - haptic_act_obj_pose_vec_obj_link_to_world_base[0];
            force[1] = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[1] - haptic_act_obj_pose_vec_obj_link_to_world_base[1];
            force[2] = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[2] - haptic_act_obj_pose_vec_obj_link_to_world_base[2];

            Eigen::Vector3d torque;
            torque[0] = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[3] - haptic_act_obj_pose_vec_obj_link_to_world_base[3];
            torque[1] = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[4] - haptic_act_obj_pose_vec_obj_link_to_world_base[4];
            torque[2] = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[5] - haptic_act_obj_pose_vec_obj_link_to_world_base[5];

            Eigen::VectorXd feedback_obj_frame(6);
            feedback_obj_frame << force[0], force[1], force[2], torque[0], torque[1], torque[2];

            /* 
                transform feedback F/T to world base frame
             */
            Eigen::Affine3d tf_from_obj_to_world_affine;
            rampUtil::convertTfTransformtoAffine3d(act_obj_tf_obj_link_to_world_base, tf_from_obj_to_world_affine);
            
            Eigen::Vector3d r = tf_from_obj_to_world_affine.translation();

            Eigen::Vector3d force_world_frame = force; // since we already transform spheres to world frame
            Eigen::Vector3d torque_world_frame = r.cross(force_world_frame) + torque;
            Eigen::VectorXd feedback_world_frame(6);
            feedback_world_frame << force_world_frame[0], force_world_frame[1], force_world_frame[2],
                                    torque_world_frame[0], torque_world_frame[1], torque_world_frame[2];

            f_vrt.push_back(feedback_world_frame);
            if (feedback_world_frame.norm() > f_vrt_max.norm())
                f_vrt_max = feedback_world_frame;

            graphic_act_obj_pose_vec_obj_link_to_world_base_at_t_minus_1 = graphic_act_obj_pose_vec_obj_link_to_world_base_at_t;




            /* 
                Debug/Verify
             */
            // ROS_INFO("haptic cfg is [%f, %f, %f, %f, %f, %f]",  haptic_act_obj_pose_vec_obj_link_to_world_base[0],
            //                                                     haptic_act_obj_pose_vec_obj_link_to_world_base[1],
            //                                                     haptic_act_obj_pose_vec_obj_link_to_world_base[2],
            //                                                     haptic_act_obj_pose_vec_obj_link_to_world_base[3],
            //                                                     haptic_act_obj_pose_vec_obj_link_to_world_base[4],
            //                                                     haptic_act_obj_pose_vec_obj_link_to_world_base[5]);
            // ROS_INFO("graphic cfg is [%f, %f, %f, %f, %f, %f]", graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[0],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[1],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[2],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[3],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[4],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[5]);
            // ROS_INFO("graphic - haptic cfg is [%f, %f, %f, %f, %f, %f]", graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[0] - haptic_act_obj_pose_vec_obj_link_to_world_base[0],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[1] - haptic_act_obj_pose_vec_obj_link_to_world_base[1],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[2] - haptic_act_obj_pose_vec_obj_link_to_world_base[2],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[3] - haptic_act_obj_pose_vec_obj_link_to_world_base[3],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[4] - haptic_act_obj_pose_vec_obj_link_to_world_base[4],
            //                                                     graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[5] - haptic_act_obj_pose_vec_obj_link_to_world_base[5]);
            simulated_steps_cnt++;
            if (simulated_steps_cnt > step_limit_of_simulated_fvrt_)
            {
                // ROS_ERROR("Visualize to Verify");
                // std::cout << colliding_pairs.size() << " pairs of spheres colliding " << std::endl;
                // std::cout << "number of steps rendering virtual force/torque " << simulated_steps_cnt << std::endl;
                // std::cout << std::fixed << std::setprecision(3) << "F_vrt (xyz-force xyz-torque) in obj frame " << feedback_obj_frame.transpose() << std::endl;
                // std::cout << std::fixed << std::setprecision(3) << "F_vrt (xyz-force xyz-torque) in wor frame " << feedback_world_frame.transpose() << std::endl;
                // ROS_INFO("graphic - haptic cfg is [%.3f, %.3f, %.3f, %.3f, %.3f, %.3f]", graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[0] - haptic_act_obj_pose_vec_obj_link_to_world_base[0],
                //                                                 graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[1] - haptic_act_obj_pose_vec_obj_link_to_world_base[1],
                //                                                 graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[2] - haptic_act_obj_pose_vec_obj_link_to_world_base[2],
                //                                                 graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[3] - haptic_act_obj_pose_vec_obj_link_to_world_base[3],
                //                                                 graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[4] - haptic_act_obj_pose_vec_obj_link_to_world_base[4],
                //                                                 graphic_act_obj_pose_vec_obj_link_to_world_base_at_t[5] - haptic_act_obj_pose_vec_obj_link_to_world_base[5]);

                // visual_tools->deleteAllMarkers();
                // /* visualize hatpic active spheres in world base */
                // for (const auto& s : active_spheres_in_world_frame)
                // {
                //     geometry_msgs::Pose p;
                //     p.position.x = s.center[0];
                //     p.position.y = s.center[1];
                //     p.position.z = s.center[2];
                //     p.orientation.w = 1.0;
                //     visual_tools->publishSphere(p, rviz_visual_tools::colors::RED, 2.0*s.radius, "Sphere");
                // }
                
                // /* visualize graphic active spheres in world base */
                // tf::Transform graphic_act_obj_tf;
                // rampUtil::convertXYZRPYtoTfTransform(graphic_act_obj_pose_vec_obj_link_to_world_base_at_t, graphic_act_obj_tf);
                // std::vector<sphere> graphic_act_obj_sps;
                // transformSpheres(active_spheres_in_spt_frame, graphic_act_obj_sps, graphic_act_obj_tf);
                // for (const auto& s : graphic_act_obj_sps)
                // {
                //     geometry_msgs::Pose p;
                //     p.position.x = s.center[0];
                //     p.position.y = s.center[1];
                //     p.position.z = s.center[2];
                //     p.orientation.w = 1.0;
                //     visual_tools->publishSphere(p, rviz_visual_tools::colors::YELLOW, 2.0*s.radius, "Sphere");
                // }

                // /* visualize passive spheres in world base */
                // for (const auto& s : passive_spheres_in_world_frame)
                // {
                //     geometry_msgs::Pose p;
                //     p.position.x = s.center[0];
                //     p.position.y = s.center[1];
                //     p.position.z = s.center[2];
                //     p.orientation.w = 1.0;
                //     visual_tools->publishSphere(p, rviz_visual_tools::colors::GREY, 2.0*s.radius, "Sphere");
                // }
                // ROS_INFO("Act obj spheres %zd", active_spheres_in_world_frame.size());
                // ROS_INFO("Passive obj spheres %zd", passive_spheres_in_world_frame.size());

                // /* visualize colliding spheres */
                // for (const auto& pir : colliding_pairs)
                // {
                //     Eigen::Vector3d as_cen = pir.first.center;
                //     tf::Vector3 pt(as_cen[0], as_cen[1], as_cen[2]);
                //     tf::Vector3 transformed_pt = act_obj_tf_obj_link_to_world_base * pt;
                //     double as_rad = pir.first.radius;
                    
                //     Eigen::Vector3d ps_cen = pir.second.center;
                //     double ps_rad = pir.second.radius;

                //     geometry_msgs::Pose p;
                //     p.position.x = transformed_pt[0];
                //     p.position.y = transformed_pt[1];
                //     p.position.z = transformed_pt[2];
                //     p.orientation.w = 1.0;
                //     // visual_tools->publishSphere(p, rviz_visual_tools::colors::BLUE, 2.0*as_rad, "Sphere");

                //     p.position.x = ps_cen[0];
                //     p.position.y = ps_cen[1];
                //     p.position.z = ps_cen[2];
                //     p.orientation.w = 1.0;
                //     // visual_tools->publishSphere(p, rviz_visual_tools::colors::GREEN, 2.0*ps_rad, "Sphere");

                //     // std::cout << std::endl;
                //     // std::cout << "[Visualizing stage]" << std::endl;
                //     // std::cout << "act sp center is " << transformed_pt[0] << " " << transformed_pt[1] << " " << transformed_pt[2] << " radius is " << as_rad << std::endl;
                //     // std::cout << "pas sp center is " << ps_cen.transpose() << " radius is " << ps_rad << std::endl;
                // }
                // visual_tools->trigger();

                /* continue with next candidate pose */
                break;
                // ROS_WARN("Press Enter to compute haptic response of next waypoint on the trajectory");
                // getchar();
            }
        }

        // ROS_INFO("Visualize every motion step");
        // visual_tools->deleteAllMarkers();
        // /* visualize hatpic active spheres in world base */
        // for (const auto& s : active_spheres_in_world_frame)
        // {
        //     geometry_msgs::Pose p;
        //     p.position.x = s.center[0];
        //     p.position.y = s.center[1];
        //     p.position.z = s.center[2];
        //     p.orientation.w = 1.0;
        //     visual_tools->publishSphere(p, rviz_visual_tools::colors::RED, 2.0*s.radius, "Sphere");
        // }

        // /* visualize passive spheres in world base */
        // for (const auto& s : passive_spheres_in_world_frame)
        // {
        //     geometry_msgs::Pose p;
        //     p.position.x = s.center[0];
        //     p.position.y = s.center[1];
        //     p.position.z = s.center[2];
        //     p.orientation.w = 1.0;
        //     visual_tools->publishSphere(p, rviz_visual_tools::colors::GREY, 2.0*s.radius, "Sphere");
        // }
        // visual_tools->trigger();
        // ROS_WARN("Press Enter to compute haptic response of next waypoint on the trajectory");
        // getchar();
    }
    // saveFTtoFile(f_vrt);
    wrenchAnalysisResult haptic_war = analyzeWrenchData(f_vrt);
    hapticRenderingResult haptic_response;
    haptic_response.f_vrt = f_vrt;
    if (use_force_reasoing_only_)
    {
        haptic_response.f_vrt_max << haptic_war.f_max[0], haptic_war.f_max[1], haptic_war.f_max[2], 0.0, 0.0, 0.0; // force only for now
        haptic_response.f_vrt_avg << haptic_war.f_avg[0], haptic_war.f_avg[1], haptic_war.f_avg[2], 0.0, 0.0, 0.0;
    }
    else
    {
        haptic_response.f_vrt_max = haptic_war.ft_max;
        haptic_response.f_vrt_avg = haptic_war.ft_avg;
    }
    return haptic_response;
}

void rampContact::visualizeSpheresInWorldBase(const std::vector<sphere> sps, const rviz_visual_tools::colors& color, const std::size_t& name_id)
{
    for (const auto& s : sps)
    {
        geometry_msgs::Pose p;
        p.position.x = s.center[0];
        p.position.y = s.center[1];
        p.position.z = s.center[2];
        p.orientation.w = 1.0;
        visual_tools->publishSphere(p, color, s.radius, "Sphere");
    }
}

void rampContact::saveFTtoFile(const std::vector<Eigen::VectorXd>& ft)
{   
    const std::string file_path("/home/huitan/Dropbox/Development/abb_catkin_ws/catkin_ws/src/ramp-abb/ramp/data/virtual_feedback_force_torque/");
	std::string spec_path = file_path+"test.txt";
	ROS_INFO("Saving virtual feedback force torque to file %s", spec_path.c_str());
	if (!ft.empty())
	{
		int n_dofs = 6;
		std::ofstream save_file;
		save_file.open(spec_path);
		for(int i=0; i<ft.size(); ++i)
		{
			/* every n_dofs is one knot cfg */
            save_file << i << " ";
			for (int j=0; j<n_dofs; ++j)
				save_file << ft[i][j] << " ";
            save_file << "\n";
		}
		save_file.close();
        ROS_INFO("Finished saving virtual F/T to file.");
	}
	else
		ROS_ERROR("Empty feedback to save!");
}

void rampContact::contactReasonOnceViaHapticRendering(const trajectoryExecutionResult& ter)
{
    /* 
        step 1
        for each candidate tf_from_pas_obj_link_to_world_base
            f_haptic = generateVirtualFeedbackWrench()
            compare(f_haptic, f_measure)
            assign matching score/probability to this candidate
     */
    std::cout << std::endl;
    ROS_INFO("[Contact reasoning start]");
    int cnt = 1;
    int total = tf_hat_from_hole_to_world_base_candidates_.size();
    for (auto& cur_tf_cost : tf_hat_from_hole_to_world_base_candidates_)
    {
        std::cout << std::endl;
        ROS_INFO("Evaluating candidate pose %d/%d", cnt, total);

        tf::Transform cur_tf_hat;
        rampUtil::convertAffine3dtoTfTransform(cur_tf_cost.first.tf_mat_, cur_tf_hat);
        hapticRenderingResult hrr = generateVirtualHapticResponse(ter.traj, cur_tf_hat);

        ROS_INFO("Passive object pose rel tf vec is [%.3f, %.3f, %.3f, %.3f, %.3f, %.3f]", 
            cur_tf_cost.first.relative_tf_vec_[0], 
            cur_tf_cost.first.relative_tf_vec_[1], 
            cur_tf_cost.first.relative_tf_vec_[2],
            cur_tf_cost.first.relative_tf_vec_[3], 
            cur_tf_cost.first.relative_tf_vec_[4], 
            cur_tf_cost.first.relative_tf_vec_[5]);

        double matching_score = 0.;
        if (use_ft_avg_reasoning_)
            matching_score = compareForceTorque(hrr.f_vrt_avg, ter.f_ext);
        else
            matching_score = compareForceTorque(hrr.f_vrt_max, ter.f_ext);
        
        cur_tf_cost.second *= matching_score;
        ++cnt;

        if (stop_contact_reason_at_each_pose_)
        {
            ROS_INFO("Press Enter to continue evaluating next pose");
            getchar();
            getchar();
        }
    }

    /* 
        step 2
        sort candidate tf by probability
     */
    std::sort(tf_hat_from_hole_to_world_base_candidates_.begin(), tf_hat_from_hole_to_world_base_candidates_.end(),
             [](const std::pair<candidatePose, double>& left, const std::pair<candidatePose, double>& right) {return left.second > right.second;});
    
    ROS_INFO("Best passive object pose rel tf vec is [%.3f, %.3f, %.3f, %.3f, %.3f, %.3f] score is %.3f", 
            tf_hat_from_hole_to_world_base_candidates_.front().first.relative_tf_vec_[0], 
            tf_hat_from_hole_to_world_base_candidates_.front().first.relative_tf_vec_[1], 
            tf_hat_from_hole_to_world_base_candidates_.front().first.relative_tf_vec_[2],
            tf_hat_from_hole_to_world_base_candidates_.front().first.relative_tf_vec_[3], 
            tf_hat_from_hole_to_world_base_candidates_.front().first.relative_tf_vec_[4], 
            tf_hat_from_hole_to_world_base_candidates_.front().first.relative_tf_vec_[5], 
            tf_hat_from_hole_to_world_base_candidates_.front().second);

    ROS_INFO("Worst passive object pose rel tf vec is [%.3f, %.3f, %.3f, %.3f, %.3f, %.3f] score is %.3f", 
            tf_hat_from_hole_to_world_base_candidates_.back().first.relative_tf_vec_[0], 
            tf_hat_from_hole_to_world_base_candidates_.back().first.relative_tf_vec_[1], 
            tf_hat_from_hole_to_world_base_candidates_.back().first.relative_tf_vec_[2],
            tf_hat_from_hole_to_world_base_candidates_.back().first.relative_tf_vec_[3], 
            tf_hat_from_hole_to_world_base_candidates_.back().first.relative_tf_vec_[4], 
            tf_hat_from_hole_to_world_base_candidates_.back().first.relative_tf_vec_[5], 
            tf_hat_from_hole_to_world_base_candidates_.back().second);

    ROS_INFO("[Contact reasoning finished]");
}

Eigen::Affine3d rampContact::getCurBestEstPasObjInWorldBase()
{
    return tf_hat_from_hole_to_world_base_candidates_.front().first.tf_mat_;
}

double rampContact::compareForceTorque(Eigen::VectorXd f_vrt, Eigen::VectorXd f_mea)
{
    Eigen::Vector3d force_vrt, torque_vrt;
    force_vrt << f_vrt[0], f_vrt[1], f_vrt[2];
    torque_vrt << f_vrt[3], f_vrt[4], f_vrt[5];
    if (force_vrt.norm() > 1e-4) force_vrt.normalize();
    if (torque_vrt.norm() > 1e-4) torque_vrt.normalize();

    Eigen::Vector3d force_mea, torque_mea;
    force_mea << f_mea[0], f_mea[1], f_mea[2];
    torque_mea << f_mea[3], f_mea[4], f_mea[5];
    if (force_mea.norm() > 1e-4) force_mea.normalize();
    if (torque_mea.norm() > 1e-4) torque_mea.normalize();

    std::cout << std::fixed << std::setprecision(3) << "F_mea (xyz-force xyz-torque) = [" << force_mea.transpose() << " " << torque_mea.transpose() << "]" << std::endl;
    std::cout << std::fixed << std::setprecision(3) << "F_vrt (xyz-force xyz-torque) = [" << force_vrt.transpose() << " " << torque_vrt.transpose() << "]" << std::endl;

    double force_matching_score = force_vrt.dot(force_mea);
    double torque_matching_score = torque_vrt.dot(torque_mea);

    if (use_force_reasoing_only_)
    {
        std::cout << std::fixed << std::setprecision(3) << "Force matching score is " << force_matching_score << std::endl;
        std::cout << std::fixed << std::setprecision(3) << "Torque matching score is " << torque_matching_score << std::endl;
        return force_matching_score;
    }
    else 
    {
        // if (f_vrt.norm() > 1e-5) f_vrt.normalize();
        // if (f_mea.norm() > 1e-5) f_mea.normalize();
        // std::cout << std::fixed << std::setprecision(3) << "Force and torque matching score is " << f_vrt.dot(f_mea) << std::endl;
        // return f_vrt.dot(f_mea);
        std::cout << std::fixed << std::setprecision(3) << "Force and torque matching score is " << force_matching_score * torque_matching_score << std::endl;
        return force_matching_score * torque_matching_score;
    }
}

void rampContact::generatePassiveObjectCandidatePoses()
{
    tf::StampedTransform transform_passive_arm_base_to_active_arm_base;
	tf_listener.lookupTransform("/panda_link0", "/base_link", ros::Time(0), transform_passive_arm_base_to_active_arm_base);
	Eigen::Affine3d tf_from_passive_arm_base_to_active_arm_base;
	rampUtil::convertTfTransformtoAffine3d(transform_passive_arm_base_to_active_arm_base, tf_from_passive_arm_base_to_active_arm_base);
    
    double x_rot_min = x_rot_range_.first; double x_rot_max = x_rot_range_.second;
    double y_rot_min = y_rot_range_.first; double y_rot_max = y_rot_range_.second;
    double z_rot_min = z_rot_range_.first; double z_rot_max = z_rot_range_.second;
    double rot_res = rotation_res_;
    for (double cur_x = x_rot_min; cur_x < x_rot_max; cur_x += rot_res)
    {
        for (double cur_y = y_rot_min; cur_y < y_rot_max; cur_y += rot_res)
        {
            for (double cur_z = z_rot_min; cur_z < z_rot_max; cur_z += rot_res)
            {
                Eigen::Matrix3d rot_uncert = (Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_x), Eigen::Vector3d::UnitX()) 
				            				* Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_y), Eigen::Vector3d::UnitY())
							            	* Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_z), Eigen::Vector3d::UnitZ())).matrix();
                Eigen::Affine3d tf_uncert;
                tf_uncert = rot_uncert;
                tf_uncert.translation() = Eigen::Vector3d(0.0, 0.0, 0.0);
            	Eigen::Affine3d tmp_tf_from_hole_to_world_base = tf_hat_from_hole_to_world_base_ * tf_uncert;
                Eigen::Affine3d tmp_tf_from_hole_to_pass_arm_base = tf_from_passive_arm_base_to_active_arm_base.inverse() * tmp_tf_from_hole_to_world_base;
                
                Eigen::VectorXd rel_tf_vec(6);
                rel_tf_vec[0] = 0.; rel_tf_vec[1] = 0.; rel_tf_vec[2] = 0.; 
                rel_tf_vec[3] = cur_x; rel_tf_vec[4] = cur_y; rel_tf_vec[5] = cur_z; 

                Eigen::AngleAxisd ax_ang(rot_uncert);

                candidatePose tmp_pose_world_base(tmp_tf_from_hole_to_world_base, rel_tf_vec, ax_ang);
                candidatePose tmp_pose_pass_arm_base(tmp_tf_from_hole_to_pass_arm_base, rel_tf_vec, ax_ang);

                double matching_score = 1.;
                tf_hat_from_hole_to_world_base_candidates_.emplace_back(tmp_pose_world_base, matching_score);
                tf_hat_from_hole_to_pas_arm_base_candidates_.emplace_back(tmp_pose_pass_arm_base);
            }
        }
    }
    ROS_INFO("Generated %zd candidate poses for passive object", tf_hat_from_hole_to_world_base_candidates_.size());
}

void rampContact::viewPassiveObjectCandidatePoses()
{
    int failed_move = 0;
    for (int i=0; i<tf_hat_from_hole_to_pas_arm_base_candidates_.size(); ++i)
    {
        ROS_INFO("Visualizing candidate pose %d/%zd", i+1, tf_hat_from_hole_to_pas_arm_base_candidates_.size());
        if (!movePassiveObjectToPose(tf_hat_from_hole_to_pas_arm_base_candidates_[i].tf_mat_, true))
            failed_move++;
    }
    ROS_WARN("%d/%zd failed move", failed_move, tf_hat_from_hole_to_pas_arm_base_candidates_.size());
}

void rampContact::wrenchStateCallback(const geometry_msgs::WrenchStamped::ConstPtr &msg)
{
    wrenches_.push_back(*msg);
    // only save last 30 
    // if (wrenches_.size() > 30)
        // wrenches_.pop();
    
    if (use_force_reasoing_only_)
    {
        double this_norm = sqrt(pow(msg->wrench.force.x, 2) + pow(msg->wrench.force.y, 2) + pow(msg->wrench.force.z, 2));
        if (this_norm > max_wrench_.max_wrench_.norm())
        {
            max_wrench_.max_wrench_[0] = 0.0;
            max_wrench_.max_wrench_[1] = 0.0;
            max_wrench_.max_wrench_[2] = 0.0;
            max_wrench_.max_wrench_[3] = msg->wrench.force.x;
            max_wrench_.max_wrench_[4] = msg->wrench.force.y;
            max_wrench_.max_wrench_[5] = msg->wrench.force.z;
            max_wrench_.max_wrench_time_ = msg->header.stamp;
        }
    }
    else
    {
        double this_norm = sqrt(pow(msg->wrench.force.x, 2) + pow(msg->wrench.force.y, 2) + pow(msg->wrench.force.z, 2) 
                            + pow(msg->wrench.torque.x,2) + pow(msg->wrench.torque.y,2) + pow(msg->wrench.torque.z,2)); 
        if (this_norm > max_wrench_.max_wrench_.norm())
        {
            max_wrench_.max_wrench_[0] = msg->wrench.torque.x;
            max_wrench_.max_wrench_[1] = msg->wrench.torque.y;
            max_wrench_.max_wrench_[2] = msg->wrench.torque.z;
            max_wrench_.max_wrench_[3] = msg->wrench.force.x;
            max_wrench_.max_wrench_[4] = msg->wrench.force.y;
            max_wrench_.max_wrench_[5] = msg->wrench.force.z;
            max_wrench_.max_wrench_time_ = msg->header.stamp;
        }
    }
}

void rampContact::frankaStateCallback(const franka_msgs::FrankaState::ConstPtr &msg)
{
    franka_states_.push_back(*msg);
    // ROS_INFO("K_F_ext_hat_k received [%f, %f, %f, %f, %f, %f]", msg->K_F_ext_hat_K[0], msg->K_F_ext_hat_K[1], msg->K_F_ext_hat_K[2],
    //                                                             msg->K_F_ext_hat_K[3], msg->K_F_ext_hat_K[4], msg->K_F_ext_hat_K[5]);
    // ROS_INFO("O_F_ext_hat_k received [%f, %f, %f, %f, %f, %f]", msg->O_F_ext_hat_K[0], msg->O_F_ext_hat_K[1], msg->O_F_ext_hat_K[2],
    //                                                             msg->O_F_ext_hat_K[3], msg->O_F_ext_hat_K[4], msg->O_F_ext_hat_K[5]);
}

void rampContact::jointStateCallback(const sensor_msgs::JointState::ConstPtr &msg)
{
    // ROS_INFO("Joint positions received [%f, %f, %f, %f, %f, %f, %f]", msg->position[0], msg->position[1], msg->position[2],
    //                                                                   msg->position[3], msg->position[4], msg->position[5], msg->position[6]);
    joint_states_.push(*msg);
    // only save last 30 
    if (joint_states_.size() > 30)
        joint_states_.pop();
}

std::string rampContact::executeTrajectoryOnHardware(const moveit_msgs::RobotTrajectory& trajectory)
{
    control_msgs::FollowJointTrajectoryGoal traj_goal;
    traj_goal.trajectory = trajectory.joint_trajectory;
    fol_jnt_traj_action_client.sendGoal(traj_goal);
    bool finished_before_timeout = fol_jnt_traj_action_client.waitForResult(ros::Duration(exe_timeout_));

    if (finished_before_timeout)
    {
        actionlib::SimpleClientGoalState state = fol_jnt_traj_action_client.getState();
        ROS_INFO("Controller: Franka Follow trajectory action finished: %s",state.toString().c_str());
        return state.toString();
    }
    else
    {
        ROS_WARN("Controller: Franka Follow trajectory action did not finish before the time out.");
        return "";
    }
}

void rampContact::closeGripper()
{
    franka_gripper::GraspEpsilon eps;
    eps.inner = 0.05; // m
    eps.outer = 0.2;
    franka_gripper::GraspGoal grasp_goal;
    grasp_goal.width = 0.05; // m
    grasp_goal.speed = 0.02; // m/s
    grasp_goal.force = 10.0;
    grasp_goal.epsilon = eps;

    gripper_grasp_action_client_.sendGoal(grasp_goal);
    bool is_suc = gripper_grasp_action_client_.waitForResult(ros::Duration(5.0));
    if (is_suc)
    {
        actionlib::SimpleClientGoalState state = gripper_grasp_action_client_.getState();
        ROS_INFO("Controller: Closing gripper action finished: %s",state.toString().c_str());
        ROS_INFO("Closed gripper");
    }
    else
    {
        ROS_WARN("Close gripper timed out");
        // stopGripperAction();
    }
}

void rampContact::openGripper()
{
    franka_gripper::MoveGoal move_goal;
    move_goal.width = 0.07;
    move_goal.speed = 0.01;

    gripper_move_action_client_.sendGoal(move_goal);
    bool is_suc = gripper_move_action_client_.waitForResult(ros::Duration(10.0));
    if (is_suc)
    {
        actionlib::SimpleClientGoalState state = gripper_move_action_client_.getState();
        ROS_INFO("Controller: Moving gripper action finished: %s",state.toString().c_str());
        ROS_INFO("Opened gripper");
    }
    else
    {
        ROS_WARN("Open gripper action timed out");
        // stopGripperAction();
    }
}

void rampContact::homingGripper()
{
    franka_gripper::HomingGoal homing_goal;
    gripper_homing_action_client_.sendGoal(homing_goal);
    bool is_suc = gripper_homing_action_client_.waitForResult(ros::Duration(20.0));
    if (is_suc)
    {
        actionlib::SimpleClientGoalState state = gripper_homing_action_client_.getState();
        ROS_INFO("Controller: Homing gripper action finished: %s",state.toString().c_str());
        
        if (state.toString() == "SUCCEEDED")
            ROS_INFO("Homed gripper");
        else if (state.toString() == "ABORTED")
            ROS_WARN("Failed to home gripper");
        else
            ROS_WARN("[Undefined homing gripper state]");
    }
    else
        ROS_WARN("Homing gripper timed out");
}

void rampContact::stopGripperAction()
{
    franka_gripper::StopGoal stop_goal;

    gripper_stop_action_client_.sendGoal(stop_goal);
    bool is_suc = gripper_stop_action_client_.waitForResult(ros::Duration(5.0));
    if (is_suc)
    {
        actionlib::SimpleClientGoalState state = gripper_stop_action_client_.getState();
        ROS_INFO("Controller: Stopping gripper action finished: %s",state.toString().c_str());
        ROS_INFO("Stopped gripper action");
    }
    else
        ROS_WARN("Stopping gripper action timed out");
}

void rampContact::cancelCurrentTrajectoryFollowGoal()
{
    ROS_WARN("Cancel current trajectory follow goal");
    // fol_jnt_traj_action_client.cancelGoal();
     actionlib_msgs::GoalID cancel_goal;
     cancel_traj_follow_pub.publish(cancel_goal);
}

void rampContact::recoverFromErrorOnce()
{
    franka_control::ErrorRecoveryActionGoal error_recov;
    franka_error_recov_pub.publish(error_recov);
    ROS_WARN("Recover from contact error once");
    sleep(1.0);
}

armCfg rampContact::getRobotCurrentCfg()
{
    if (!joint_states_.empty())
        return joint_states_.back().position;
    else
    {
        ROS_ERROR("No joint states received so far from real/virtual robot!. Using zero arm cfg.");
        return armCfg(7, 0);
    }
}

void rampContact::setLoad()
{
    franka_control::SetLoad sl;
    sl.request.mass = 0.3;
    sl.request.F_x_center_load[0] = 0.0; sl.request.F_x_center_load[1] = 0.0; sl.request.F_x_center_load[2] = 0.02;
    sl.request.load_inertia[0] = 0.001; sl.request.load_inertia[1] = 0.0;   sl.request.load_inertia[2] = 0.0;
    sl.request.load_inertia[3] = 0.0;   sl.request.load_inertia[4] = 0.001; sl.request.load_inertia[5] = 0.0;
    sl.request.load_inertia[6] = 0.0;   sl.request.load_inertia[7] = 0.0;   sl.request.load_inertia[8] = 0.001;
    if (set_load_srv_client.call(sl))
    {
        if (sl.response.success)
            ROS_INFO("Set load success.");
        else
            ROS_INFO("Failed to set load due to %s", sl.response.error.c_str());
    }
    else
        ROS_WARN("Failed to set load for franka panda.");
}

void rampContact::setForceTorqueCollisionThresholds()
{
    franka_control::SetForceTorqueCollisionBehavior sft;
    /* default */
    // sft.request.lower_torque_thresholds_nominal = {20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0};
    // sft.request.upper_torque_thresholds_nominal = {20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0};
    // sft.request.lower_force_thresholds_nominal = {20.0, 20.0, 20.0, 25.0, 25.0, 25.0};
    // sft.request.upper_force_thresholds_nominal = {20.0, 20.0, 20.0, 25.0, 25.0, 25.0};

    // assembly experiments
    sft.request.lower_torque_thresholds_nominal = {20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0};
    sft.request.upper_torque_thresholds_nominal = {20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0};
    // sft.request.lower_force_thresholds_nominal = {15.0, 15.0, 15.0, 15.0, 15.0, 15.0};
    // sft.request.upper_force_thresholds_nominal = {15.0, 15.0, 15.0, 15.0, 15.0, 15.0};
    sft.request.lower_force_thresholds_nominal = {30.0, 30.0, 30.0, 30.0, 30.0, 30.0};
    sft.request.upper_force_thresholds_nominal = {30.0, 30.0, 30.0, 30.0, 30.0, 30.0};

    if(set_ft_col_srv_client.call(sft))
    {
        if (sft.response.success)
            ROS_INFO("Set force torque collision behavior success.");
        else
            ROS_INFO("Failed to set force torque collision behavior due to %s", sft.response.error.c_str());
    }
    else
        ROS_WARN("Failed to set force torque collision behavior.");
}

void rampContact::resetMaxWrench()
{
    max_wrench_.max_wrench_.setZero();
}

wrenchAnalysisResult rampContact::analyzeWrenchData(const std::vector<Eigen::VectorXd>& wrenches)
{
    wrenchAnalysisResult war;
    Eigen::Vector3d cur_f_sum;
    cur_f_sum.setZero();
    Eigen::VectorXd cur_ft_sum(6);
    cur_ft_sum.setZero();
    for (const auto& cur_ft : wrenches)
    {
        Eigen::Vector3d cur_f = Eigen::Vector3d(cur_ft[0], cur_ft[1], cur_ft[2]);
        if (cur_f.norm() > war.f_max.norm())
            war.f_max = cur_f;
        if (cur_ft.norm() > war.ft_max.norm())
            war.ft_max = cur_ft;
        cur_f_sum += cur_f;
        cur_ft_sum += cur_ft;
    }
    int size = wrenches.size();
    war.f_avg = cur_f_sum / size;
    war.ft_avg = cur_ft_sum / size;
    return war;
}

void rampContact::enlargeSpheres(std::vector<sphere>& spheres, double radius_delta)
{
    for (auto& sp : spheres)
        sp.radius += radius_delta;
}

rampContact::~rampContact()
{

}

bool rampContact::moveABBHardwareToCfg(const armCfg& cfg)
{
	std::vector<double> arm_jnt_vels(6, 1.0);
	trajectory_msgs::JointTrajectory trajectory;
	std::vector<std::string> jnt_names{"joint_1", "joint_2", "joint_3", "joint_4", "joint_5", "joint_6"};
	trajectory.joint_names = jnt_names;
	trajectory_msgs::JointTrajectoryPoint point;
	point.positions = cfg;
	point.velocities = arm_jnt_vels;
	point.time_from_start = ros::Duration(0.5);
	trajectory.points.push_back(point);

	control_msgs::FollowJointTrajectoryGoal traj_goal;
    traj_goal.trajectory = trajectory;
    abb_fol_jnt_traj_action_client_.sendGoal(traj_goal);
    bool finished_before_timeout = abb_fol_jnt_traj_action_client_.waitForResult(ros::Duration(exe_timeout_));

    if (finished_before_timeout)
    {
        actionlib::SimpleClientGoalState state = abb_fol_jnt_traj_action_client_.getState();
        ROS_INFO("Controller: ABB Follow trajectory action finished: %s",state.toString().c_str());
        return true;
    }
    else
    {
        ROS_WARN("Controller: ABB Follow trajectory action did not finish before the time out.");
        return false;
    }
}

void rampContact::collectFmeaFsimData()
{
    int failed_move = 0; 
    int num_of_poses = tf_hat_from_hole_to_pas_arm_base_candidates_.size();
    std::string pkg_path = ros::package::getPath("ramp");
    ROS_INFO("Start collecting F_mea and F_sim data:");
    int cnt = 0;
    for (int i=0; i<num_of_poses; ++i)
    {
        if (i <= 180)
            continue;
        /* 
            step 1
            move ABB to candidate pose 
        */
        ROS_INFO("Moving ABB to candidate pose %d/%d", i+1, num_of_poses);
        bool move_passive_arm_hardware = true;
	    if (!movePassiveObjectToPose(tf_hat_from_hole_to_pas_arm_base_candidates_[i].tf_mat_, move_passive_arm_hardware))
        {
            failed_move++;
            continue;
        }

        /* 
            step 2
            move Franka panda to get F_mea
         */
        // collect Fmeas from Franka panda sensors
        wrenches_.clear();
    	tf::Vector3 obj_moving_dir_in_obj_link(0., 0., 1.); // always along z-direction of object frame
		trajectoryExecutionResult ter = insertOnce(obj_moving_dir_in_obj_link, 110, 0.02, 0.02);
        
        // apply sensor transformation
        Eigen::Affine3d tf_from_k_to_w_affine;
        rampUtil::convertTfTransformtoAffine3d(ter.tf_from_k_frame_to_world_base_in_contact, tf_from_k_to_w_affine);
        
        Eigen::Affine3d T_kw = tf_from_k_to_w_affine.inverse();
        Eigen::Matrix3d rot = T_kw.rotation();
        Eigen::Vector3d trans = T_kw.translation();
        Eigen::Matrix3d p_bracket;
        p_bracket << 0.0,      -trans[2],  trans[1],
                     trans[2],  0.0,      -trans[0],
                    -trans[1],  trans[0],  0.0;
        Eigen::Matrix3d pr = p_bracket*rot;
        
        Eigen::Matrix3d zeros(3, 3);
        zeros << 0.0, 0.0, 0.0,
                 0.0, 0.0, 0.0,
                 0.0, 0.0, 0.0;
        Eigen::MatrixXd Ad_T_kw(6, 6);
        Ad_T_kw << rot, zeros, pr, rot;

        // save F_mea to file
        std::ofstream save_force_signals;
        std::string file_path = pkg_path+"/data/force_signals/f_sims_and_f_meas/force_signals_"+std::to_string(i)+".txt";
        save_force_signals.open(file_path);
        save_force_signals << "Candidate pose start:" << std::endl;
        save_force_signals << i << std::endl;
        save_force_signals << "Candidate pose end" << std::endl;

        save_force_signals << "Uncertainty xyz position and orientation vector start: " << std::endl;
        save_force_signals << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[0] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[1] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[2] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[3] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[4] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[5] << " "
                           << std::endl;
        save_force_signals << "Uncertainty xyz position and orientation vector end" << std::endl;

        save_force_signals << "F_measure start:" << std::endl;
        double contact_time_window = 10.0; // in sec
        for (const auto& w : wrenches_)
        {
            // only save wrenches received in contact time window
            // ROS_INFO("measured wrench passed %f seconds", (ter.contact_time-w.header.stamp).toSec());
            if ( std::abs((ter.contact_time-w.header.stamp).toSec()) < contact_time_window )
            {
                // transform to world frame
                Eigen::VectorXd ft_in_k_frame(6); // [xyz-torque, xyz-force]
                ft_in_k_frame << w.wrench.torque.x, w.wrench.torque.y, w.wrench.torque.z, w.wrench.force.x, w.wrench.force.y, w.wrench.force.z;
                ft_in_k_frame = -ft_in_k_frame;
                Eigen::VectorXd ft_in_world_base_frame = Ad_T_kw.transpose()*ft_in_k_frame;
                
                save_force_signals << ft_in_world_base_frame[3] << " " << ft_in_world_base_frame[4] << " " << ft_in_world_base_frame[5] << std::endl;   
            }
            else
            {
                // ROS_WARN("Wrench time stamp out of contact window. Continue with next measurement.");
                continue;
            }
        }
        save_force_signals << "F_measure end" << std::endl;
        save_force_signals << std::endl;

        /* 
            step 3
            move Franka panda up to retract from contact
         */
        if(ter.state == "SUCCEEDED")
		{
			ROS_INFO("Insertion succeeded!");
		}
		else if (ter.state == "ABORTED")
		{
			/* first move back robot by moving along negative direction*/
			recoverFromErrorOnce();
			tf::Vector3 reversed_dir(-obj_moving_dir_in_obj_link.x(), -obj_moving_dir_in_obj_link.y(), -obj_moving_dir_in_obj_link.z());
			insertOnce(reversed_dir, 60);
            
            /* adjust eef orientiton to be original since contacts will deform the eef! */
            armCfg start_cfg = getRobotCurrentCfg();
            armCfg end_cfg = ter.traj.joint_trajectory.points.front().positions;
            moveit_msgs::RobotTrajectory retract_traj;
            arm1.moveFromJntCfgToJntCfgByJointMotion(start_cfg, end_cfg, retract_traj, 0.05, 0.05);
            executeTrajectoryOnHardware(retract_traj);
		}
		else
			ROS_WARN("Undefined trajectory execution state");

        /* 
            step 4
            get F_sim from haptic rendering
         */
        // collect Fsim from haptic rendering in simulation
        tf::Transform cur_tf_hat;
        rampUtil::convertAffine3dtoTfTransform(tf_hat_from_hole_to_world_base_candidates_[i].first.tf_mat_, cur_tf_hat);
        hapticRenderingResult hrr = generateVirtualHapticResponse(ter.traj, cur_tf_hat);

        // save F_sim to file
        save_force_signals << "F_simulation start:" << std::endl;
        for (const auto& f_sim : hrr.f_vrt)
        {
            if (f_sim.norm() > 1e-4)
            {   
                Eigen::Vector3d f_normalized(f_sim[0], f_sim[1], f_sim[2]);
                f_normalized.normalize();
                save_force_signals << f_normalized[0] << " " << f_normalized[1] << " " << f_normalized[2] << std::endl;
            }
            else
                save_force_signals << 0.0 << " " << 0.0 << " " << 0.0 << std::endl;;
        }
        save_force_signals << "F_simulation end" << std::endl;
        save_force_signals << std::endl;

        /* 
            step 5
            save groundtruth axis angle of rotation uncertainty
         */
        save_force_signals << "Ground truth axis angle of rotation uncertainty start:" << std::endl;
        save_force_signals << tf_hat_from_hole_to_world_base_candidates_[i].first.axis_ang_.axis().transpose() << " "
                           << tf_hat_from_hole_to_world_base_candidates_[i].first.axis_ang_.angle() << std::endl;
        save_force_signals << "Ground truth axis angle of rotation uncertainty end" << std::endl;
        save_force_signals.close();

        ROS_INFO("Finished writing data of pose %d to file %s", i, file_path.c_str());
        std::cout << std::endl;

        if ( ++cnt > 100)
        {
            ROS_WARN("Restart to reduce accumulative error");
            break;
        }
    }
    ROS_WARN("Finished with all candidate poses. Passive arm failed to move to %d/%d poses", failed_move, num_of_poses);
    ROS_INFO("Finished collecting F_mea and F_sim data.");
}

void rampContact::collectFsimData()
{
    int failed_move = 0; 
    int num_of_poses = tf_hat_from_hole_to_pas_arm_base_candidates_.size();
    std::string pkg_path = ros::package::getPath("ramp");
    ROS_INFO("Start collecting F_mea and F_sim data:");
    int cnt = 0;
    for (int i=0; i<num_of_poses; i+=350)
    {
        // if (i <= 13456)
            // continue;
        /* 
            step 1
            move ABB to candidate pose 
        */
        ROS_INFO("Moving ABB to candidate pose %d/%d", i+1, num_of_poses);
        bool move_passive_arm_hardware = false;
	    if (!movePassiveObjectToPose(tf_hat_from_hole_to_pas_arm_base_candidates_[i].tf_mat_, move_passive_arm_hardware))
        {
            failed_move++;
            continue;
        }

        /* 
            step 2
            move Franka panda to get F_mea
         */
        // collect Fmeas from Franka panda sensors
        wrenches_.clear();
    	tf::Vector3 obj_moving_dir_in_obj_link(0., 0., 1.); // always along z-direction of object frame
		trajectoryExecutionResult ter = insertOnce(obj_moving_dir_in_obj_link, 110, 0.02, 0.02);

        // save F_mea to file
        std::ofstream save_force_signals;
        std::string file_path = pkg_path+"/data/force_signals/f_sims_only/force_signals_"+std::to_string(i)+".txt";
        save_force_signals.open(file_path);
        save_force_signals << "Candidate pose start:" << std::endl;
        save_force_signals << i << std::endl;
        save_force_signals << "Candidate pose end" << std::endl;

        save_force_signals << "Uncertainty xyz position and orientation vector start: " << std::endl;
        save_force_signals << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[0] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[1] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[2] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[3] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[4] << " "
                           << tf_hat_from_hole_to_pas_arm_base_candidates_[i].relative_tf_vec_[5] << " "
                           << std::endl;
        save_force_signals << "Uncertainty xyz position and orientation vector end" << std::endl;

        save_force_signals << "F_measure start:" << std::endl;
        save_force_signals << "F_measure end" << std::endl;
        save_force_signals << std::endl;

        /* 
            step 4
            get F_sim from haptic rendering
         */
        // collect Fsim from haptic rendering in simulation
        tf::Transform cur_tf_hat;
        rampUtil::convertAffine3dtoTfTransform(tf_hat_from_hole_to_world_base_candidates_[i].first.tf_mat_, cur_tf_hat);
        hapticRenderingResult hrr = generateVirtualHapticResponse(ter.traj, cur_tf_hat);

        // save F_sim to file
        save_force_signals << "F_simulation start:" << std::endl;
        for (const auto& f_sim : hrr.f_vrt)
        {
            if (f_sim.norm() > 1e-4)
            {   
                Eigen::Vector3d f_normalized(f_sim[0], f_sim[1], f_sim[2]);
                f_normalized.normalize();
                save_force_signals << f_normalized[0] << " " << f_normalized[1] << " " << f_normalized[2] << std::endl;
            }
            else
                save_force_signals << 0.0 << " " << 0.0 << " " << 0.0 << std::endl;;
        }
        save_force_signals << "F_simulation end" << std::endl;
        save_force_signals << std::endl;

        /* 
            step 5
            save groundtruth axis angle of rotation uncertainty
         */
        save_force_signals << "Ground truth axis angle of rotation uncertainty start:" << std::endl;
        save_force_signals << tf_hat_from_hole_to_world_base_candidates_[i].first.axis_ang_.axis().transpose() << " "
                           << tf_hat_from_hole_to_world_base_candidates_[i].first.axis_ang_.angle() << std::endl;
        save_force_signals << "Ground truth axis angle of rotation uncertainty end" << std::endl;
        save_force_signals.close();

        ROS_INFO("Finished writing data of pose %d to file %s", i, file_path.c_str());
        std::cout << std::endl;

        // if ( ++cnt > 100)
        // {
        //     ROS_WARN("Restart to reduce accumulative error");
        //     break;
        // }
    }
    ROS_WARN("Finished with all candidate poses. Passive arm failed to move to %d/%d poses", failed_move, num_of_poses);
    ROS_INFO("Finished collecting F_mea and F_sim data.");
}

void rampContact::directInsertion(const Eigen::Vector3d& gt_uncert_rpy, const Eigen::Vector3d& pred_uncert_rpy, const Eigen::Vector3d& pos_offset)
{
    /* 
		step 1
		move passive object to the uncertain pose
	 */
    ROS_INFO("[1] Moving passive object to the uncertain pose...");
	bool move_hardware = true;
    // move passive object to ground truth uncertain pose
    tf::StampedTransform transform_passive_arm_base_to_active_arm_base;
	tf_listener.lookupTransform("/panda_link0", "/base_link", ros::Time(0), transform_passive_arm_base_to_active_arm_base);
	Eigen::Affine3d tf_from_passive_arm_base_to_active_arm_base;
	rampUtil::convertTfTransformtoAffine3d(transform_passive_arm_base_to_active_arm_base, tf_from_passive_arm_base_to_active_arm_base);

    double cur_x = gt_uncert_rpy[0];
    double cur_y = gt_uncert_rpy[1];
    double cur_z = gt_uncert_rpy[2];
    Eigen::Matrix3d rot_uncert = (Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_x), Eigen::Vector3d::UnitX()) 
                                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_y), Eigen::Vector3d::UnitY())
                                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_z), Eigen::Vector3d::UnitZ())).matrix();
    Eigen::Affine3d tf_uncert;
    tf_uncert = rot_uncert;
    tf_uncert.translation() = Eigen::Vector3d(0.0, 0.0, 0.0);
    Eigen::Affine3d tmp_tf_from_hole_to_world_base = tf_hat_from_hole_to_world_base_ * tf_uncert;
    Eigen::Affine3d tmp_tf_from_hole_to_pass_arm_base = tf_from_passive_arm_base_to_active_arm_base.inverse() * tmp_tf_from_hole_to_world_base;
    
    movePassiveObjectToPose(tmp_tf_from_hole_to_pass_arm_base, move_hardware);

	/* 
		step 2
		Guarded motion 1
		execute nominal insertion 
		trajectory and stop upon contacts
	 */
    ROS_INFO("[2] Guarded motion 1...");
	tf::Vector3 obj_moving_dir_in_obj_link(0., 0., 1.); // always along z-direction of object frame
	trajectoryExecutionResult ter = insertOnce(obj_moving_dir_in_obj_link, 110, 0.02, 0.02);

	/* 
		step 3
		retract from contact
        re-orient to align peg and hole
	 */
    ROS_INFO("[3] Retract and re-orient to align peg and hole...");
    if(ter.state == "SUCCEEDED")
        ROS_INFO("Insertion succeeded!");
    else if (ter.state == "ABORTED")
    {
        /* move back robot */
        recoverFromErrorOnce();
        tf::Vector3 reversed_dir(-obj_moving_dir_in_obj_link.x(), -obj_moving_dir_in_obj_link.y(), -obj_moving_dir_in_obj_link.z());
        insertOnce(reversed_dir, 50);
    }
    else
        ROS_WARN("Undefined trajectory execution state");

    // reorient
    cur_x = pred_uncert_rpy[0];
    cur_y = pred_uncert_rpy[1];
    cur_z = pred_uncert_rpy[2];
    rot_uncert = (Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_x), Eigen::Vector3d::UnitX()) 
                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_y), Eigen::Vector3d::UnitY())
                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_z), Eigen::Vector3d::UnitZ())).matrix();
    tf_uncert = rot_uncert;
    tf_uncert.translation() = Eigen::Vector3d(0.0, 0.0, 0.0);
    Eigen::Affine3d pred_tf_from_hole_to_world_base = tf_hat_from_hole_to_world_base_ * tf_uncert;

    // new orientation goal to align peg with hole
    Eigen::Affine3d tf_from_active_object_to_passive_object;
    Eigen::Matrix3d rot;
	rot <<  1.0, 0.0, 0.0,
			0.0, -1.0, 0.0,
			0.0, 0.0, -1.0;
	tf_from_active_object_to_passive_object = rot;
	tf_from_active_object_to_passive_object.translation() = pos_offset; // offset for position since we are focusing on orientation uncertainty

    Eigen::Affine3d tgt_tf_from_active_object_to_base = pred_tf_from_hole_to_world_base * tf_from_active_object_to_passive_object;
    // moveActiveObjectToOreintationGoal(tgt_tf_from_active_object_to_base, 0.01, 0.01);
    moveActiveObjectToPose(tgt_tf_from_active_object_to_base, 0.01, 0.01);

	/* 
		step 4
		direct insert
	 */
    ROS_INFO("[4] Cartesian compliant insertion...");
    ter = insertOnce(obj_moving_dir_in_obj_link, 110, 0.02, 0.02);

    if(ter.state == "SUCCEEDED")
        ROS_INFO("Insertion succeeded!");
    else if (ter.state == "ABORTED")
    {
        /* move back robot */
        recoverFromErrorOnce();
        tf::Vector3 reversed_dir(-obj_moving_dir_in_obj_link.x(), -obj_moving_dir_in_obj_link.y(), -obj_moving_dir_in_obj_link.z());
        insertOnce(reversed_dir, 50);
    }
    else
        ROS_WARN("Undefined trajectory execution state");
}

void rampContact::compliantInsertionViaContactStateTransitions(const Eigen::Vector3d& gt_uncert_rpy, const Eigen::Vector3d& pred_uncert_rpy, 
                                                                const Eigen::Vector3d& pos_offset, int compliant_option)
{
    /* 
		step 1
		move passive object to the uncertain pose
	 */
    ROS_INFO("[1] Moving passive object to the uncertain pose...");
	bool move_hardware = true;
    // move passive object to ground truth uncertain pose
    tf::StampedTransform transform_passive_arm_base_to_active_arm_base;
	tf_listener.lookupTransform("/panda_link0", "/base_link", ros::Time(0), transform_passive_arm_base_to_active_arm_base);
	Eigen::Affine3d tf_from_passive_arm_base_to_active_arm_base;
	rampUtil::convertTfTransformtoAffine3d(transform_passive_arm_base_to_active_arm_base, tf_from_passive_arm_base_to_active_arm_base);

    double cur_x = gt_uncert_rpy[0];
    double cur_y = gt_uncert_rpy[1];
    double cur_z = gt_uncert_rpy[2];
    Eigen::Matrix3d rot_uncert = (Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_x), Eigen::Vector3d::UnitX()) 
                                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_y), Eigen::Vector3d::UnitY())
                                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_z), Eigen::Vector3d::UnitZ())).matrix();
    Eigen::Affine3d tf_uncert;
    tf_uncert = rot_uncert;
    tf_uncert.translation() = Eigen::Vector3d(0.0, 0.0, 0.0);
    Eigen::Affine3d tmp_tf_from_hole_to_world_base = tf_hat_from_hole_to_world_base_ * tf_uncert;
    Eigen::Affine3d tmp_tf_from_hole_to_pass_arm_base = tf_from_passive_arm_base_to_active_arm_base.inverse() * tmp_tf_from_hole_to_world_base;
    
    movePassiveObjectToPose(tmp_tf_from_hole_to_pass_arm_base, move_hardware);

	/* 
		step 2
		Guarded motion 1
		execute nominal insertion 
		trajectory and stop upon contacts
	 */
    ROS_INFO("[2] Guarded motion 1...");
	tf::Vector3 obj_moving_dir_in_obj_link(0., 0., 1.); // always along z-direction of object frame
	trajectoryExecutionResult ter = insertOnce(obj_moving_dir_in_obj_link, 110, 0.02, 0.02);

	/* 
		step 3
		Guarded motion 2
		rotate active object to the refined pose
		and stop upon contacts
	 */
    ROS_INFO("[3] Guarded motion 2...");
    cur_x = pred_uncert_rpy[0];
    cur_y = pred_uncert_rpy[1];
    cur_z = pred_uncert_rpy[2];
    rot_uncert = (Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_x), Eigen::Vector3d::UnitX()) 
                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_y), Eigen::Vector3d::UnitY())
                * Eigen::AngleAxisd(rampUtil::convertDegToRad(cur_z), Eigen::Vector3d::UnitZ())).matrix();
    tf_uncert = rot_uncert;
    tf_uncert.translation() = Eigen::Vector3d(0.0, 0.0, 0.0);
    Eigen::Affine3d pred_tf_from_hole_to_world_base = tf_hat_from_hole_to_world_base_ * tf_uncert;

    // new orientation goal to align peg with hole
    Eigen::Affine3d tf_from_active_object_to_passive_object;
    Eigen::Matrix3d rot;
    rot <<  1.0, 0.0, 0.0,
            0.0, -1.0, 0.0,
            0.0, 0.0, -1.0;
    tf_from_active_object_to_passive_object = rot;
    tf_from_active_object_to_passive_object.translation() = pos_offset;

    Eigen::Affine3d tgt_tf_from_active_object_to_base = pred_tf_from_hole_to_world_base * tf_from_active_object_to_passive_object;

    int option = compliant_option;
    if (option == 1)
    {
        /* 
            step 3 option 1 
            old way but it worked
        */
        std::string exe_hw_state = moveActiveObjectToPose(tgt_tf_from_active_object_to_base, 0.01, 0.01);
        
        if(exe_hw_state == "SUCCEEDED")
            ROS_INFO("Motion execution succeeded!");
        else if (exe_hw_state == "ABORTED")
        {
            /* move back robot */
            recoverFromErrorOnce();
            // tf::Vector3 reversed_dir(-obj_moving_dir_in_obj_link.x(), -obj_moving_dir_in_obj_link.y(), -obj_moving_dir_in_obj_link.z());
            // insertOnce(reversed_dir, 50);
        }
        else
            ROS_WARN("Undefined trajectory execution state");
    }
    else
    {
        /* 
            step 3 option 2
            In-place only orientation change
        */
        // get panda current fk
        tf::StampedTransform tf_from_panda_eef_link_to_world_base;
        tf_listener.lookupTransform("/panda_link0", "/panda_link8", ros::Time(0), tf_from_panda_eef_link_to_world_base);

        tf::Transform tf_from_obj_link_to_world_base = tf_from_panda_eef_link_to_world_base * active_obj.tf_from_parent_link_to_arm_eef_link * active_obj.tf_from_object_link_to_parent_link;
        
        
        // maintain position and change orientation
        std::cout << "originial xyz position " << tgt_tf_from_active_object_to_base.translation()[0] << " "
                    << tgt_tf_from_active_object_to_base.translation()[1] << " " << tgt_tf_from_active_object_to_base.translation()[2] << std::endl;

        tgt_tf_from_active_object_to_base.translation()[0] = tf_from_obj_link_to_world_base.getOrigin().getX();
        tgt_tf_from_active_object_to_base.translation()[1] = tf_from_obj_link_to_world_base.getOrigin().getY();
        tgt_tf_from_active_object_to_base.translation()[2] = tf_from_obj_link_to_world_base.getOrigin().getZ();

        std::cout << "modified xyz position " << tgt_tf_from_active_object_to_base.translation()[0] << " "
                    << tgt_tf_from_active_object_to_base.translation()[1] << " " << tgt_tf_from_active_object_to_base.translation()[2] << std::endl;

        std::string exe_hw_state = moveActiveObjectToPose(tgt_tf_from_active_object_to_base, 0.01, 0.01);
        
        if(exe_hw_state == "SUCCEEDED")
            ROS_INFO("Motion execution succeeded!");
        else if (exe_hw_state == "ABORTED")
        {
            /* move back robot */
            recoverFromErrorOnce();
            // tf::Vector3 reversed_dir(-obj_moving_dir_in_obj_link.x(), -obj_moving_dir_in_obj_link.y(), -obj_moving_dir_in_obj_link.z());
            // insertOnce(reversed_dir, 50);
        }
        else
            ROS_WARN("Undefined trajectory execution state");
    }
    
	/* 
		step 4
		Cartesian compliant motion
		for straight insertion
	 */
    ROS_INFO("[4] Cartesian compliant insertion...");
    insertOnce(obj_moving_dir_in_obj_link, 50, 0.02, 0.02);
}

