#include "ramp_contact/sphere_tree.h"

sphere::sphere(const Eigen::Vector3d& cen, const double rad)
{
    center = cen;
    radius = rad;
}

sphereTree::sphereTree(const std::string& path_to_sph, const std::vector<double>& pose_vec_from_spt_link_to_mesh_link)
{
    std::ifstream file(path_to_sph);
    if (!file.is_open()) 
        ROS_ERROR("Couldn't open file %s", path_to_sph.c_str());
    else
    {
        int num_of_lines = 0;
        std::string line;
        while (std::getline(file, line)) 
        {
            num_of_lines++;
            std::istringstream iss(line);
            if (!iss.str().empty())
            {
                if (num_of_lines==1)
                {
                    iss >> depth >> branching_factor;
                    ROS_INFO("Sphere tree depth %d branching factor %d", depth, branching_factor);
                }
                else
                {
                    double x, y, z, rad, impt;
                    iss >> x >> y >> z >> rad >> impt;
                    x /= 39.37; // convert from inch to m
                    y /= 39.37; 
                    z /= 39.37; 
                    rad /= 39.37; 
                    spheres.push_back(sphere(Eigen::Vector3d(x,y,z), rad));
                    // printf("Added sphere at (%f, %f, %f) radius %f\n", x, y, z, rad);
                }
            }
            else
                break;
        }
        rampUtil::convertXYZRPYtoTfTransform(pose_vec_from_spt_link_to_mesh_link, tf_from_spt_link_to_mesh_link);
        ROS_INFO("Sphere tree loaded from file %s", path_to_sph.c_str());
    }
}

void sphereTree::visualizeSpheresAtLevelX(const int level, const tf::Transform& tf_from_object_link_to_world_base_link, 
                                          const std::size_t& id, const moveit_visual_tools::MoveItVisualToolsPtr& visual_tools)
{  
    int node_ind_min = (1-pow(branching_factor, level-1))/(1-branching_factor);
    int node_ind_max = (1-pow(branching_factor, level))/(1-branching_factor);
    ROS_INFO("Level %d node from [%d, %d)", level, node_ind_min, node_ind_max);
    for (int i=node_ind_min; i<node_ind_max; ++i)
    {
        tf::Vector3 pt(spheres[i].center[0], spheres[i].center[1], spheres[i].center[2]);
        tf::Vector3 transformed_pt = tf_from_object_link_to_world_base_link * tf_from_spt_link_to_mesh_link * pt;

        geometry_msgs::Pose p;
        p.position.x = transformed_pt.x();
        p.position.y = transformed_pt.y();
        p.position.z = transformed_pt.z();
        p.orientation.w = 1.0;
        if (id == 1)
		    visual_tools->publishSphere(p, rviz_visual_tools::colors::RED, spheres[i].radius, "Sphere");
        else
            visual_tools->publishSphere(p, rviz_visual_tools::colors::YELLOW, spheres[i].radius, "Sphere");
    }
	visual_tools->trigger();
}

sphereTree::~sphereTree()
{

}

void sphereTree::getSpheresAtLevelXInWorldBaseFrame(const int level, 
                                                    const tf::Transform& tf_from_object_link_to_world_base_link,
                                                    std::vector<sphere>& sphere_container)
{
    int node_ind_min = (1-pow(branching_factor, level-1))/(1-branching_factor);
    int node_ind_max = (1-pow(branching_factor, level))/(1-branching_factor);
    // ROS_INFO("Level %d node from [%d, %d)", level, node_ind_min, node_ind_max);
    for (int i=node_ind_min; i<node_ind_max; ++i)
    {
        tf::Vector3 pt(spheres[i].center[0], spheres[i].center[1], spheres[i].center[2]);
        tf::Vector3 transformed_pt = tf_from_object_link_to_world_base_link * tf_from_spt_link_to_mesh_link * pt;
        if (spheres[i].radius > 1e-3)
        {
            sphere s(Eigen::Vector3d(transformed_pt.x(), transformed_pt.y(), transformed_pt.z()), spheres[i].radius);
            sphere_container.push_back(s);
        }
        // else
            // ROS_WARN("Ignoring sphere %d since its radius is too small", i);
    }
}

void sphereTree::getSpheresAtLevelXInSphereTreeFrame(const int level, std::vector<sphere>& sphere_container, double radius_scale)
{
    int node_ind_min = (1-pow(branching_factor, level-1))/(1-branching_factor);
    int node_ind_max = (1-pow(branching_factor, level))/(1-branching_factor);
    // ROS_INFO("Level %d node from [%d, %d)", level, node_ind_min, node_ind_max);
    for (int i=node_ind_min; i<node_ind_max; ++i)
    {
        tf::Vector3 pt(spheres[i].center[0], spheres[i].center[1], spheres[i].center[2]);
        tf::Vector3 transformed_pt = tf_from_spt_link_to_mesh_link * pt;
        if (spheres[i].radius > 1e-3)
        {
            sphere s(Eigen::Vector3d(transformed_pt.x(), transformed_pt.y(), transformed_pt.z()), radius_scale*spheres[i].radius);
            sphere_container.push_back(s);
        }
    }
    // ROS_INFO("%zd spheres at level %d", sphere_container.size(), level);
}

