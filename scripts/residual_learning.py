#!/usr/bin/env python

import numpy as np
import os
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
import keras.backend as K
from scipy import spatial
from scipy import stats
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
import matplotlib as mpl 
## agg backend is used to create plot as a .png file
mpl.use('agg')
import matplotlib.pyplot as plt 

def parseForceFile(file_name):
    f_mea = np.empty((0, 3))
    f_sim = np.empty((0, 3))
    axis_ang_label = np.empty((0, 4))
    rpy_label = np.empty((0, 3))
    data_indicator = -1
    f_mea_max = np.zeros((1, 3))
    f_sim_max = np.zeros((1, 3))

    with open(file_name) as f:
        for line in f:
            if line.strip() == 'F_measure start:':
                data_indicator = 1
                continue
            elif line.strip() == 'F_measure end':
                data_indicator = 0
            elif line.strip() == 'F_simulation start:':
                data_indicator = 2
                continue
            elif line.strip() == 'F_simulation end':
                data_indicator = 0
            elif line.strip() == 'Ground truth axis angle of rotation uncertainty start:':
                data_indicator = 3
                continue
            elif line.strip() == 'Ground truth axis angle of rotation uncertainty end':
                data_indicator = 0
            elif line.strip() == 'Uncertainty xyz position and orientation vector start:':
                data_indicator = 4
                continue
            elif line.strip() == 'Uncertainty xyz position and orientation vector end':
                data_indicator = 0
            else:
                pass
            
            if data_indicator == 1:
                cur_f_mea = np.fromstring(line.strip(), dtype=float, sep=' ')
                if np.linalg.norm(f_mea_max) < np.linalg.norm(cur_f_mea):
                    f_mea_max = cur_f_mea
                f_mea = np.vstack((f_mea, cur_f_mea))
            
            if data_indicator == 2:
                cur_f_sim = np.fromstring(line.strip(), dtype=float, sep=' ')
                if np.linalg.norm(f_sim_max) < np.linalg.norm(cur_f_sim):
                    f_sim_max = cur_f_sim
                f_sim = np.vstack((f_sim, cur_f_sim))
            
            if data_indicator == 3:
                axis_ang_label = np.vstack((axis_ang_label, np.fromstring(line.strip(), dtype=float, sep=' ')))
            
            if data_indicator == 4:
                cur_rpy = np.fromstring(line.strip(), dtype=float, sep=' ')
                rpy_label = np.vstack((rpy_label, cur_rpy[3:]))

    return f_mea_max, f_sim_max, axis_ang_label, rpy_label

def loadData(data_path, uncert_range):
    print "Loading data..."
    f_meas = np.empty((0, 3))
    f_sims = np.empty((0, 3))
    axis_ang_labels = np.empty((0, 4))
    rpy_labels = np.empty((0, 3))

    for (dirpath, dirnames, filenames) in os.walk(data_path):
        for file in filenames:
            fm_max, fs_max, axis_ang_label, rpy_label = parseForceFile(data_path + file)

            if np.any(np.absolute(rpy_label) > uncert_range):
                continue
            
            # weight of fx fy fz is 1 1 0.5
            # fm_max[2] *= 0.1

            # only use first two dimension
            # fm_max = fm_max[:2]
            # fs_max = fs_max[:2]
            
            # normalize each force vector
            # fm_max = fm_max / np.linalg.norm(fm_max)
            # fs_max = fs_max / np.linalg.norm(fs_max)

            # normalize rpy label using its ranges
            rpy_label = (rpy_label+15.0)/30.0

            # append to data matrix
            f_meas = np.vstack((f_meas, fm_max))
            f_sims = np.vstack((f_sims, fs_max))
            axis_ang_labels = np.vstack((axis_ang_labels, axis_ang_label))
            rpy_labels = np.vstack((rpy_labels, rpy_label))

            # if np.linalg.norm(fs_max) < 1e-4:
            #     print "zero force!"
            #     print file
                # input("Press Enter to continue...")

    print "Loading data finished"

    # normalize each column by minus zero mean and dividing by std
    f_meas_mean = np.mean(f_meas, axis=0)
    f_meas_std = np.std(f_meas, axis=0, ddof=1)

    f_meas = f_meas - f_meas_mean
    if np.allclose(f_meas_std, [1e-8,1e-8,1e-8]) == False:
        f_meas = f_meas / f_meas_std

    # f_sims = f_sims - np.mean(f_sims, axis=0)
    # f_sims = f_sims / np.std(f_sims, axis=0, ddof=1)

    return f_meas, f_sims, axis_ang_labels, rpy_labels, f_meas_mean, f_meas_std

def trainValidateReasonOnce(f_meas, f_sims, rpy_labels, f_meas_mean, f_meas_std, f_sims_auxilary, rpy_labels_auxilary, split_size = 0.5, plot_history = False, epoch_num = 300, verbose=False, shuffle_data=True, use_auxilary_data=True):
    """ Shuffle data """
    if shuffle_data:
        inds = np.arange(f_meas.shape[0])
        np.random.shuffle(inds)
        f_meas = f_meas[inds]
        f_sims = f_sims[inds]
        rpy_labels = rpy_labels[inds]

    """ Split training and testing data """
    n_train = int(split_size * f_meas.shape[0])
    f_meas_train, f_meas_test = f_meas[:n_train, :], f_meas[n_train:, :]
    f_sims_train, f_sims_test = f_sims[:n_train, :], f_sims[n_train:, :]
    # axis_ang_labels_train, axis_ang_labels_test = axis_ang_labels[:n_train], axis_ang_labels[n_train:]
    rpy_labels_train, rpy_labels_test = rpy_labels[:n_train], rpy_labels[n_train:]

    """ actual training """
    f_org_train = np.hstack((f_sims_train, rpy_labels_train))
    f_org_test = np.hstack((f_sims_test, rpy_labels_test))
    label_f_meas_train = f_meas_train
    label_f_meas_test = f_meas_test
    regression_model_ind = 1

    if regression_model_ind == 1:
        """ neural network model """
        model = Sequential()
        adam = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
        sgd = keras.optimizers.SGD(lr=0.1, momentum=0.001, decay=0.001, nesterov=False)
        rmsprop = keras.optimizers.RMSprop(lr=0.01, rho=0.9, epsilon=None, decay=0.0)

        # parameter set 1
        # model.add(Dense(3, activation=None, input_dim=6))

        # parameter set 2
        model.add(Dense(16, activation='relu', input_dim=6))
        # model.add(Dense(16, activation='relu'))
        model.add(Dense(3, activation=None))
        
        model.compile(optimizer=sgd, loss='mae') # loss='mse'
        history = model.fit(f_org_train, label_f_meas_train, validation_data=(f_org_test, label_f_meas_test), epochs=epoch_num, batch_size=16, verbose=1)
        predicted_force = model.predict(f_org_test)
        # print "Learned model summary:"
        # model.summary()

        if plot_history:
            """ Plot trainning history """
            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('Model loss')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Test'], loc='upper left')
            plt.show()
    elif regression_model_ind == 2:
        """ linear regression """
        regr = linear_model.LinearRegression()

        # Train the model using the training sets
        regr.fit(f_org_train, label_f_meas_train)
        # Make predictions using the testing set
        predicted_force = regr.predict(f_org_test)

        # The coefficients
        print('Coefficients: \n', regr.coef_)
        print('Intercepts: \n', regr.intercept_)
        # The mean squared error
        print("Training Mean abosulte error: %.2f"
            % mean_absolute_error(label_f_meas_train, regr.predict(f_org_train)))
        print("Testing Mean abosulte error: %.2f"
            % mean_absolute_error(label_f_meas_test, predicted_force))
        # Explained variance score: 1 is perfect prediction
        print('Variance score: %.2f' % r2_score(label_f_meas_test, predicted_force))
        print "Linear regression parameters:"
        print regr.get_params()
    else:
        print "Not recoginzed regression model selected"

    """ restore original values of validation data """
    f_sims = f_sims_test
    rpy_gt = rpy_labels_test*30.0 - 15.0
    f_meas_gt = label_f_meas_test*f_meas_std + f_meas_mean
    f_sims_data_aug = predicted_force*f_meas_std + f_meas_mean

    """ load additional simulated feedback forces for finer resolution during nearest neighbor search """
    if use_auxilary_data:
        f_sims = np.vstack((f_sims, f_sims_auxilary))
        print "f_sims:"
        print f_sims
        
        f_sim_aug_test = np.hstack((f_sims_auxilary, rpy_labels_auxilary))
        f_sim_auxilary_data_augmented = model.predict(f_sim_aug_test)
        f_sim_auxilary_data_augmented = f_sim_auxilary_data_augmented*f_meas_std + f_meas_mean
        f_sims_data_aug = np.vstack((f_sims_data_aug, f_sim_auxilary_data_augmented))
        print "f_sims_data_aug:"
        print f_sims_data_aug

        rpy_labels_auxilary = rpy_labels_auxilary*30.0 - 15.0
        rpy_gt_expanded = np.vstack((rpy_gt, rpy_labels_auxilary))
        print "rpy_gt_expanded:"
        print rpy_gt_expanded

    """ RPY inference 1 """
    """ compare f_meas_gt with f_sims generated from different rpys """
    f_meas_normalized = f_meas_gt / np.linalg.norm(f_meas_gt, ord=2, axis=1, keepdims=True)
    f_sims_normalized = f_sims / np.linalg.norm(f_sims, ord=2, axis=1, keepdims=True)
    # np.savetxt('../data/residual_learning/f_sims.csv', f_sims, delimiter=',')
    # np.savetxt('../data/residual_learning/f_sims_nrmlized.csv', f_sims_normalized, delimiter=',')
    tree1 = spatial.KDTree(f_sims_normalized)

    def query(f_measured, tree, rpy_table):
        return rpy_table[tree.query(f_measured)[1]]

    if use_auxilary_data:
        inferred_rpy1 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree1, rpy_table=rpy_gt_expanded)
    else:
        inferred_rpy1 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree1, rpy_table=rpy_gt)

    diff_rpy1 = np.abs(rpy_gt - inferred_rpy1)
    diff_force1 = np.abs(f_meas_gt - f_sims)

    if verbose:
        print "\nInference 1 rpy diff mean:"
        print np.mean(diff_rpy1, axis=0)
        print "Inference 1 rpy diff std:"
        print np.std(diff_rpy1, axis=0, ddof=1)
        print "\nInference 1 force diff mean:"
        print np.mean(diff_force1, axis=0)
        print "Inference 1 force diff std:"
        print np.std(diff_force1, axis=0, ddof=1)


    """ RPY inference 2 """
    """ compare f_meas_gt with f_sims_data_aug generated from different rpys """
    # f_sims_data_aug_normalized = f_sims_data_aug / np.linalg.norm(f_sims_data_aug, ord=2, axis=1, keepdims=True)
    # tree2 = spatial.KDTree(f_sims_data_aug_normalized)
    # inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree2, rpy_table=rpy_gt)
    tree2 = spatial.KDTree(f_sims_data_aug)
    if use_auxilary_data:
        inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_gt, tree=tree2, rpy_table=rpy_gt_expanded)
    else:
        inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_gt, tree=tree2, rpy_table=rpy_gt)

    diff_rpy2 = np.abs(rpy_gt - inferred_rpy2)
    diff_force2 = np.abs(f_meas_gt - f_sims_data_aug)

    """ Paired t test """
    x_t_score, x_p_val = stats.ttest_rel(diff_rpy1[:,0],diff_rpy2[:,0])
    y_t_score, y_p_val = stats.ttest_rel(diff_rpy1[:,1],diff_rpy2[:,1])
    z_t_score, z_p_val = stats.ttest_rel(diff_rpy1[:,2],diff_rpy2[:,2])

    """ box plot """
    fig = plt.figure(1, figsize=(9, 6))
    ax = fig.add_subplot(111)
    # ax.set_xlim([xmin,xmax])
    ax.set_ylim([-1, 31])
    data_to_plot = np.hstack((diff_rpy1, diff_rpy2))
    permutation = [0,2,4,1,3,5]
    idx = np.argsort(permutation)

    bp = ax.boxplot(data_to_plot[:,idx], patch_artist=True)
    ax.set_xticklabels(['x1', 'x2', 'y1', 'y2', 'z1', 'z2'])
    fig.savefig('../data/residual_learning/box_plot.png', bbox_inches='tight')

    if verbose:
        # print "f_sims_data_aug:"
        # print f_sims_data_aug
        # print "true rpy:"
        # print rpy_gt
        # print "predicted rpy:"
        # print inferred_rpy2
        print "\nInference 2 rpy diff mean:"
        print np.mean(diff_rpy2, axis=0)
        print "Inference 2 rpy diff std:"
        print np.std(diff_rpy2, axis=0, ddof=1)
        print "\nInference 2 force diff mean:"
        print np.mean(diff_force2, axis=0)
        print "Inference 2 force diff std:"
        print np.std(diff_force2, axis=0, ddof=1)
        print ("delta x t-score and p-value: ", x_t_score, x_p_val)
        print ("delta y t-score and p-value: ", y_t_score, y_p_val)
        print ("delta z t-score and p-value: ", z_t_score, z_p_val)
        rpy_gt_pred = np.hstack((rpy_gt, inferred_rpy2))
        np.savetxt('../data/residual_learning/rpy_gt_pred.csv', rpy_gt_pred, delimiter=',')
        # np.savetxt('../data/residual_learning/rpy_gt.csv', rpy_gt, delimiter=',')
        # np.savetxt('../data/residual_learning/inferred_rpy2.csv', inferred_rpy2, delimiter=',')

    res = np.hstack((history.history['loss'][-1], history.history['val_loss'][-1]))
    res = np.hstack((res, np.mean(diff_rpy2, axis=0)))
    res = np.hstack((res, np.std(diff_rpy2, axis=0, ddof=1)))
    res = np.hstack((res, np.mean(diff_rpy1, axis=0)))
    res = np.hstack((res, np.std(diff_rpy1, axis=0, ddof=1)))
    return res, model

def applyModelToNewTask(model, data_path, retrain_size = 0.0, use_auxilary_data = False, verbose = True):
    """ load data """
    f_meas, f_sims, axis_ang_labels, rpy_labels, f_meas_mean, f_meas_std = loadData(data_path, uncert_range)
    
    """ generate data augnmented forces """
    f_org_test = np.hstack((f_sims, rpy_labels))
    predicted_force = model.predict(f_org_test)

    """ restore orignial scale """
    rpy_gt = rpy_labels*30.0 - 15.0
    f_meas_gt = f_meas*f_meas_std + f_meas_mean
    f_sims_data_aug = predicted_force*f_meas_std + f_meas_mean

    """ RPY inference 1 """
    """ compare f_meas_gt with f_sims generated from different rpys """
    f_meas_normalized = f_meas_gt / np.linalg.norm(f_meas_gt, ord=2, axis=1, keepdims=True)
    f_sims_normalized = f_sims / np.linalg.norm(f_sims, ord=2, axis=1, keepdims=True)
    # np.savetxt('../data/residual_learning/f_sims.csv', f_sims, delimiter=',')
    # np.savetxt('../data/residual_learning/f_sims_nrmlized.csv', f_sims_normalized, delimiter=',')
    tree1 = spatial.KDTree(f_sims_normalized)

    def query(f_measured, tree, rpy_table):
        return rpy_table[tree.query(f_measured)[1]]
    
    if use_auxilary_data:
        inferred_rpy1 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree1, rpy_table=rpy_gt_expanded)
    else:
        inferred_rpy1 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree1, rpy_table=rpy_gt)

    diff_rpy1 = np.abs(rpy_gt - inferred_rpy1)
    diff_force1 = np.abs(f_meas_gt - f_sims)

    if verbose:
        print "\nInference 1 rpy diff mean:"
        print np.mean(diff_rpy1, axis=0)
        print "Inference 1 rpy diff std:"
        print np.std(diff_rpy1, axis=0, ddof=1)
        print "\nInference 1 force diff mean:"
        print np.mean(diff_force1, axis=0)
        print "Inference 1 force diff std:"
        print np.std(diff_force1, axis=0, ddof=1)


    """ RPY inference 2 """
    """ compare f_meas_gt with f_sims_data_aug generated from different rpys """
    tree2 = spatial.KDTree(f_sims_data_aug)
    if use_auxilary_data:
        inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_gt, tree=tree2, rpy_table=rpy_gt_expanded)
    else:
        inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_gt, tree=tree2, rpy_table=rpy_gt)

    diff_rpy2 = np.abs(rpy_gt - inferred_rpy2)
    diff_force2 = np.abs(f_meas_gt - f_sims_data_aug)

    """ Paired t test """
    x_t_score, x_p_val = stats.ttest_rel(diff_rpy1[:,0],diff_rpy2[:,0])
    y_t_score, y_p_val = stats.ttest_rel(diff_rpy1[:,1],diff_rpy2[:,1])
    z_t_score, z_p_val = stats.ttest_rel(diff_rpy1[:,2],diff_rpy2[:,2])

    """ box plot """
    fig = plt.figure(1, figsize=(9, 6))
    ax = fig.add_subplot(111)
    # ax.set_xlim([xmin,xmax])
    ax.set_ylim([-1, 31])
    data_to_plot = np.hstack((diff_rpy1, diff_rpy2))
    permutation = [0,2,4,1,3,5]
    idx = np.argsort(permutation)

    bp = ax.boxplot(data_to_plot[:,idx], patch_artist=True)
    ax.set_xticklabels(['x1', 'x2', 'y1', 'y2', 'z1', 'z2'])
    fig.savefig('../data/residual_learning/box_plot.png', bbox_inches='tight')

    if verbose:
        # print "f_sims_data_aug:"
        # print f_sims_data_aug
        # print "true rpy:"
        # print rpy_gt
        # print "f_meas_gt"
        # print f_meas_gt
        # print "predicted rpy:"
        # print inferred_rpy2
        print "\nInference 2 rpy diff mean:"
        print np.mean(diff_rpy2, axis=0)
        print "Inference 2 rpy diff std:"
        print np.std(diff_rpy2, axis=0, ddof=1)
        print "\nInference 2 force diff mean:"
        print np.mean(diff_force2, axis=0)
        print "Inference 2 force diff std:"
        print np.std(diff_force2, axis=0, ddof=1)
        print ("delta x t-score and p-value: ", x_t_score, x_p_val)
        print ("delta y t-score and p-value: ", y_t_score, y_p_val)
        print ("delta z t-score and p-value: ", z_t_score, z_p_val)
        rpy_gt_pred = np.hstack((rpy_gt, inferred_rpy2))
        np.savetxt('../data/residual_learning/rpy_gt_pred.csv', rpy_gt_pred, delimiter=',')
        rpy_gt_f_meas = np.hstack((rpy_gt, f_meas_gt))
        np.savetxt('../data/residual_learning/rpy_gt_f_meas_gt.csv', rpy_gt_f_meas, delimiter=',')
        # np.savetxt('../data/residual_learning/rpy_gt.csv', rpy_gt, delimiter=',')
        # np.savetxt('../data/residual_learning/inferred_rpy2.csv', inferred_rpy2, delimiter=',')

def mixLearning(epoch_num = 1000, verbose=True, shuffle_data=False, use_auxilary_data=False, plot_history = False):
    uncert_range = np.array([15.5, 15.5, 15.5])

    """ Load training and validation data """
    data_path = '../data/force_signals/original_data/two-pin/f_sims_and_f_meas/'
    f_meas_2pin, f_sims_2pin, axis_ang_labels_2pin, rpy_labels_2pin, f_meas_mean_2pin, f_meas_std_2pin = loadData(data_path, uncert_range)
    
    """ restore original values of validation data """
    # rpy_gt_2pin = rpy_labels_2pin*30.0 - 15.0
    f_meas_2pin = f_meas_2pin*f_meas_std_2pin + f_meas_mean_2pin

    data_path = '../data/force_signals/original_data/three-pin/f_sims_and_f_meas/'
    f_meas_3pin, f_sims_3pin, axis_ang_labels_3pin, rpy_labels_3pin, f_meas_mean_3pin, f_meas_std_3pin = loadData(data_path, uncert_range)

    # rpy_gt_3pin = rpy_labels_3pin*30.0 - 15.0
    f_meas_3pin = f_meas_3pin*f_meas_std_3pin + f_meas_mean_3pin

    """ split data """
    split_size_2pin = 0.01
    split_size_3pin = 0.2
    n_train = int(split_size_2pin * f_meas_2pin.shape[0])
    f_meas_2pin_train, f_meas_2pin_test = f_meas_2pin[:n_train, :], f_meas_2pin[n_train:, :]
    f_sims_2pin_train, f_sims_2pin_test = f_sims_2pin[:n_train, :], f_sims_2pin[n_train:, :]
    rpy_labels_2pin_train, rpy_labels_2pin_test = rpy_labels_2pin[:n_train], rpy_labels_2pin[n_train:]

    n_train = int(split_size_3pin * f_meas_3pin.shape[0])
    f_meas_3pin_train, f_meas_3pin_test = f_meas_3pin[:n_train, :], f_meas_3pin[n_train:, :]
    f_sims_3pin_train, f_sims_3pin_test = f_sims_3pin[:n_train, :], f_sims_3pin[n_train:, :]
    rpy_labels_3pin_train, rpy_labels_3pin_test = rpy_labels_3pin[:n_train], rpy_labels_3pin[n_train:]

    """ group data """
    f_meas_train = np.empty((0, 3))
    f_sims_train = np.empty((0, 3))
    rpy_labels_train = np.empty((0, 3))

    f_meas_test = np.empty((0, 3))
    f_sims_test = np.empty((0, 3))
    rpy_labels_test = np.empty((0, 3))

    f_meas_train = np.vstack((f_meas_2pin_train, f_meas_3pin_train))
    f_sims_train = np.vstack((f_sims_2pin_train, f_sims_3pin_train))
    rpy_labels_train = np.vstack((rpy_labels_2pin_train, rpy_labels_3pin_train))

    """ test 2pin """
    # f_meas_test = f_meas_2pin_test
    # f_sims_test = f_sims_2pin_test
    # rpy_labels_test = rpy_labels_2pin_test

    """ test 3pin """
    f_meas_test = f_meas_3pin_test
    f_sims_test = f_sims_3pin_test
    rpy_labels_test = rpy_labels_3pin_test

    """ test 2pin + 3pin """
    # f_meas_test = np.vstack((f_meas_2pin_test, f_meas_3pin_test))
    # f_sims_test = np.vstack((f_sims_2pin_test, f_sims_3pin_test))
    # rpy_labels_test = np.vstack((rpy_labels_2pin_test, rpy_labels_3pin_test))

    # f_meas_mean = np.mean(f_meas, axis=0)
    # f_meas_std = np.std(f_meas, axis=0, ddof=1)

    # f_meas = f_meas - f_meas_mean
    # if np.allclose(f_meas_std, [1e-8,1e-8,1e-8]) == False:
    #     f_meas = f_meas / f_meas_std

    # f_sims = np.vstack((f_sims, f_sims_3pin))
    # f_sims = np.vstack((f_sims, f_sims_3pin))

    f_org_train = np.hstack((f_sims_train, rpy_labels_train))
    f_org_test = np.hstack((f_sims_test, rpy_labels_test))
    label_f_meas_train = f_meas_train
    label_f_meas_test = f_meas_test

    regression_model_ind = 1
    if regression_model_ind == 1:
        """ neural network model """
        model = Sequential()
        adam = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
        sgd = keras.optimizers.SGD(lr=0.1, momentum=0.001, decay=0.001, nesterov=False)
        rmsprop = keras.optimizers.RMSprop(lr=0.01, rho=0.9, epsilon=None, decay=0.0)

        # parameter set 1
        # model.add(Dense(3, activation=None, input_dim=6))

        # parameter set 2
        model.add(Dense(16, activation='relu', input_dim=6))
        model.add(Dense(16, activation='relu'))
        model.add(Dense(3, activation=None))
        
        model.compile(optimizer=sgd, loss='mae') # loss='mse'
        history = model.fit(f_org_train, label_f_meas_train, validation_data=(f_org_test, label_f_meas_test), epochs=epoch_num, batch_size=16, verbose=1)
        predicted_force = model.predict(f_org_test)
        # print "Learned model summary:"
        # model.summary()

        if plot_history:
            """ Plot trainning history """
            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('Model loss')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Test'], loc='upper left')
            plt.show()
    elif regression_model_ind == 2:
        """ linear regression """
        regr = linear_model.LinearRegression()

        # Train the model using the training sets
        regr.fit(f_org_train, label_f_meas_train)
        # Make predictions using the testing set
        predicted_force = regr.predict(f_org_test)

        # The coefficients
        print('Coefficients: \n', regr.coef_)
        print('Intercepts: \n', regr.intercept_)
        # The mean squared error
        print("Training Mean abosulte error: %.2f"
            % mean_absolute_error(label_f_meas_train, regr.predict(f_org_train)))
        print("Testing Mean abosulte error: %.2f"
            % mean_absolute_error(label_f_meas_test, predicted_force))
        # Explained variance score: 1 is perfect prediction
        print('Variance score: %.2f' % r2_score(label_f_meas_test, predicted_force))
        print "Linear regression parameters:"
        print regr.get_params()
    else:
        print "Not recoginzed regression model selected"

    """ restore original values of validation data """
    f_sims = f_sims_test
    rpy_gt = rpy_labels_test*30.0 - 15.0
    # f_meas_gt = label_f_meas_test*f_meas_std + f_meas_mean
    # f_sims_data_aug = predicted_force*f_meas_std + f_meas_mean
    f_meas_gt = label_f_meas_test
    f_sims_data_aug = predicted_force

    """ load additional simulated feedback forces for finer resolution during nearest neighbor search """
    if use_auxilary_data:
        f_sims = np.vstack((f_sims, f_sims_auxilary))
        print "f_sims:"
        print f_sims
        
        f_sim_aug_test = np.hstack((f_sims_auxilary, rpy_labels_auxilary))
        f_sim_auxilary_data_augmented = model.predict(f_sim_aug_test)
        f_sim_auxilary_data_augmented = f_sim_auxilary_data_augmented*f_meas_std + f_meas_mean
        f_sims_data_aug = np.vstack((f_sims_data_aug, f_sim_auxilary_data_augmented))
        print "f_sims_data_aug:"
        print f_sims_data_aug

        rpy_labels_auxilary = rpy_labels_auxilary*30.0 - 15.0
        rpy_gt_expanded = np.vstack((rpy_gt, rpy_labels_auxilary))
        print "rpy_gt_expanded:"
        print rpy_gt_expanded

    """ RPY inference 1 """
    """ compare f_meas_gt with f_sims generated from different rpys """
    f_meas_normalized = f_meas_gt / np.linalg.norm(f_meas_gt, ord=2, axis=1, keepdims=True)
    f_sims_normalized = f_sims / np.linalg.norm(f_sims, ord=2, axis=1, keepdims=True)
    # np.savetxt('../data/residual_learning/f_sims.csv', f_sims, delimiter=',')
    # np.savetxt('../data/residual_learning/f_sims_nrmlized.csv', f_sims_normalized, delimiter=',')
    tree1 = spatial.KDTree(f_sims_normalized)

    def query(f_measured, tree, rpy_table):
        return rpy_table[tree.query(f_measured)[1]]

    if use_auxilary_data:
        inferred_rpy1 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree1, rpy_table=rpy_gt_expanded)
    else:
        inferred_rpy1 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree1, rpy_table=rpy_gt)

    diff_rpy1 = np.abs(rpy_gt - inferred_rpy1)
    diff_force1 = np.abs(f_meas_gt - f_sims)

    if verbose:
        print "\nInference 1 rpy diff mean:"
        print np.mean(diff_rpy1, axis=0)
        print "Inference 1 rpy diff std:"
        print np.std(diff_rpy1, axis=0, ddof=1)
        print "\nInference 1 force diff mean:"
        print np.mean(diff_force1, axis=0)
        print "Inference 1 force diff std:"
        print np.std(diff_force1, axis=0, ddof=1)


    """ RPY inference 2 """
    """ compare f_meas_gt with f_sims_data_aug generated from different rpys """
    # f_sims_data_aug_normalized = f_sims_data_aug / np.linalg.norm(f_sims_data_aug, ord=2, axis=1, keepdims=True)
    # tree2 = spatial.KDTree(f_sims_data_aug_normalized)
    # inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_normalized, tree=tree2, rpy_table=rpy_gt)
    tree2 = spatial.KDTree(f_sims_data_aug)
    if use_auxilary_data:
        inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_gt, tree=tree2, rpy_table=rpy_gt_expanded)
    else:
        inferred_rpy2 = np.apply_along_axis(query, axis=1, arr=f_meas_gt, tree=tree2, rpy_table=rpy_gt)

    diff_rpy2 = np.abs(rpy_gt - inferred_rpy2)
    diff_force2 = np.abs(f_meas_gt - f_sims_data_aug)

    """ Paired t test """
    x_t_score, x_p_val = stats.ttest_rel(diff_rpy1[:,0],diff_rpy2[:,0])
    y_t_score, y_p_val = stats.ttest_rel(diff_rpy1[:,1],diff_rpy2[:,1])
    z_t_score, z_p_val = stats.ttest_rel(diff_rpy1[:,2],diff_rpy2[:,2])

    """ box plot """
    fig = plt.figure(1, figsize=(9, 6))
    ax = fig.add_subplot(111)
    # ax.set_xlim([xmin,xmax])
    ax.set_ylim([-1, 31])
    data_to_plot = np.hstack((diff_rpy1, diff_rpy2))
    permutation = [0,2,4,1,3,5]
    idx = np.argsort(permutation)

    bp = ax.boxplot(data_to_plot[:,idx], patch_artist=True)
    ax.set_xticklabels(['x1', 'x2', 'y1', 'y2', 'z1', 'z2'])
    fig.savefig('../data/residual_learning/box_plot.png', bbox_inches='tight')

    if verbose:
        # print "f_sims_data_aug:"
        # print f_sims_data_aug
        # print "true rpy:"
        # print rpy_gt
        # print "predicted rpy:"
        # print inferred_rpy2
        print "\nInference 2 rpy diff mean:"
        print np.mean(diff_rpy2, axis=0)
        print "Inference 2 rpy diff std:"
        print np.std(diff_rpy2, axis=0, ddof=1)
        print "\nInference 2 force diff mean:"
        print np.mean(diff_force2, axis=0)
        print "Inference 2 force diff std:"
        print np.std(diff_force2, axis=0, ddof=1)
        print ("delta x t-score and p-value: ", x_t_score, x_p_val)
        print ("delta y t-score and p-value: ", y_t_score, y_p_val)
        print ("delta z t-score and p-value: ", z_t_score, z_p_val)
        rpy_gt_pred = np.hstack((rpy_gt, inferred_rpy2))
        np.savetxt('../data/residual_learning/rpy_gt_pred.csv', rpy_gt_pred, delimiter=',')
        # np.savetxt('../data/residual_learning/rpy_gt.csv', rpy_gt, delimiter=',')
        # np.savetxt('../data/residual_learning/inferred_rpy2.csv', inferred_rpy2, delimiter=',')

    res = np.hstack((history.history['loss'][-1], history.history['val_loss'][-1]))
    res = np.hstack((res, np.mean(diff_rpy2, axis=0)))
    res = np.hstack((res, np.std(diff_rpy2, axis=0, ddof=1)))
    res = np.hstack((res, np.mean(diff_rpy1, axis=0)))
    res = np.hstack((res, np.std(diff_rpy1, axis=0, ddof=1)))
    return res, model

def standaloneLearning():
    """ program mode 1 """
    uncert_range = np.array([15.5, 15.5, 15.5])
    load_aux_data = False

    """ Load training and validation data """
    data_path = '../data/force_signals/original_data/two-pin/f_sims_and_f_meas/'
    # data_path = '../data/force_signals/original_data/three-pin/f_sims_and_f_meas/'
    f_meas, f_sims, axis_ang_labels, rpy_labels, f_meas_mean, f_meas_std = loadData(data_path, uncert_range)
    
    if load_aux_data:
        """ Load auxilary data only has f_sims """
        data_path = '../data/force_signals/original_data/two_pin/f_sims_only/'
        _, f_sims_auxilary, _, rpy_labels_auxilary, _, _,  = loadData(data_path, uncert_range)
    else:
        f_sims_auxilary = np.empty((0, 3))
        rpy_labels_auxilary = np.empty((0, 3))

    # size = [0.1, 0.2, 0.5, 0.7]
    size = [0.2]
    for s in size:
        res_all = np.empty((0, 14))
        for i in range(1):
            print('split size ', s, ' run ', i)
            res, model = trainValidateReasonOnce(f_meas, f_sims, rpy_labels, f_meas_mean, f_meas_std,
                f_sims_auxilary, rpy_labels_auxilary,
                split_size = s, epoch_num = 300, verbose=True, shuffle_data=False, use_auxilary_data=load_aux_data)
            res_all = np.vstack((res_all, res))
        
        path = '../data/residual_learning/learning_res_avg_50_runs_size_' + str(s) + '.csv'
        np.savetxt(path, np.mean(res_all, axis=0), delimiter=',')
        # print '\nall results mean:'
        # print np.mean(res_all, axis=0)
        # print 'all results std:'
        # print np.std(res_all, axis=0, ddof=1)

    """ program mode 2 """
    # apply model trained on two-pin data to three-pin
    # data_path = '../data/force_signals/original_data/three-pin/f_sims_and_f_meas/'
    # applyModelToNewTask(model, data_path)

    """ program mode 3 """
    # apply model trained on three-pin data to two-pin
    # data_path = '../data/force_signals/original_data/two-pin/f_sims_and_f_meas/'
    # applyModelToNewTask(model, data_path)


if __name__ == "__main__":
    # standaloneLearning()

    mixLearning(epoch_num = 1000, verbose=True, shuffle_data=False, use_auxilary_data=False)





