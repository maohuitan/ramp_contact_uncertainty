#!/usr/bin/python

from pylab import *
from matplotlib import rc, rcParams
import seaborn as sns
import matplotlib.pylab as plt
import pandas as pd
from sets import Set

rc('text',usetex=True)
rc('font',**{'family':'serif','serif':['Computer Modern']})

def plotForceTorque():
    #  Import the data from a text file and save as a 2D matrix.
    #dataMatrix1 = genfromtxt('winavrg.dat')
    dataMatrix1 = genfromtxt('../data/virtual_feedback_force_torque/test.txt')

    # Slice out required abscissae and ordinate vectors.

    x = dataMatrix1[:,0]
    force_x = dataMatrix1[:,1]
    force_y = dataMatrix1[:,2]
    force_z = dataMatrix1[:,3]
    torque_x = dataMatrix1[:,4]
    torque_y = dataMatrix1[:,5]
    torque_z = dataMatrix1[:,6]


    plot(x,force_x,label=r'forceX')
    plot(x,force_y,label=r'forceY')
    plot(x,force_z,label=r'forceZ')
    plot(x,torque_x,label=r'torqueX')
    plot(x,torque_y,label=r'torqueY')
    plot(x,torque_z,label=r'torqueZ')


    #legend(loc='lower right',prop={'size':23})
    legend(loc='upper left')
    #xlabel(r'time',fontsize=15,fontweight="bold")
    xlabel(r'time step',fontsize=17,fontweight='bold')
    #plt.xlim([0,6500])
    plt.ylabel(r'force/torque',fontsize=17,fontweight='bold')
    #plt.xticks([500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500],fontsize=23,fontweight="bold")
    #plt.yticks([-0.006,-0.005,-0.004,-0.003,-0.002,-0.001,0.000,0.001],fontsize=23,fontweight="bold")
    ax = plt.gca()
    ax.grid(True)

    show()

def plotErrorHeatMap():
    org_data = genfromtxt('../data/residual_learning/3-pin/rpy_gt_pred_heatmap_data.csv', delimiter=',')
    exist = Set()
    gama = 0.0
    exist.add(str(gama))
    uniq_data = np.empty((0, 4))
    for row in org_data:
        sig = str(row[2])
        if sig in exist:
            # print sig
            uniq_data = np.vstack((uniq_data, row))

    # manually add missing ones
    if (abs(gama - 7.0) < 0.0001):
        uniq_data = np.vstack((uniq_data, np.array([-3, -11, 7, 12])))
    elif (abs(gama - 1.0) < 0.0001):
        uniq_data = np.vstack((uniq_data, np.array([-11, -5, 1, 18])))
        uniq_data = np.vstack((uniq_data, np.array([11, -9, 1, 12])))
        uniq_data = np.vstack((uniq_data, np.array([3, 13, 1, 14])))
    elif (abs(gama - 15.0) < 0.0001):
        uniq_data = np.vstack((uniq_data, np.array([-13, -5, 15, 9])))
        uniq_data = np.vstack((uniq_data, np.array([-1, 9, 15, 7])))

    print uniq_data.shape
    cnt = 0
    for row in uniq_data:
        if row[3] > 15:
            cnt = cnt + 1
    print 1.0 - cnt / 256.0

    x = uniq_data[:, 0]
    y = uniq_data[:, 1]
    z = uniq_data[:, 3]
    data = pd.DataFrame(data={'alpha':x, 'beta':y, 'error':z})
    data = data.pivot(index='alpha', columns='beta', values='error')
    # ax = sns.heatmap(data, annot=True, cmap='viridis', vmin=0, vmax=60)
    ax = sns.heatmap(data, annot=False, cmap='viridis')
    # ax = sns.heatmap(data, annot=True, cmap='YlGnBu')

    # plt.imshow(data, cmap=plt.cm.gray_r, interpolation='nearest')
    # plt.imshow(data, cmap='hot', interpolation='nearest')
    # plt.imshow(data, cmap='viridis', interpolation='nearest')
    # plt.imshow(data, cmap='viridis', interpolation='nearest', vmin=0, vmax=60)
    # plt.colorbar()
    plt.show()

plotErrorHeatMap()

