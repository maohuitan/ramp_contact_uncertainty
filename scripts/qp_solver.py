#!/usr/bin/env python

# Import the necessary packages
import numpy
from cvxopt import matrix
from cvxopt import solvers

from ramp.srv import *
from std_msgs.msg import Float64MultiArray
import rospy

pre_sol = Float64MultiArray()

def solveQP(req):
    print ("Solving QP...")
    # Define QP parameters (with NumPy)
    P = matrix(numpy.diag(req.P_vec.data), tc='d')
    q = matrix(numpy.array(req.q_vec.data), tc='d')

    G_mat = []
    for arr in req.G_mat:
        G_mat.append(arr.data)
    G = matrix(numpy.array(G_mat), tc='d')
    
    h = matrix(numpy.array(req.h_vec.data), tc='d')
    # print(P)
    # print(q)
    # print(G)
    # print(h)

    # Construct the QP, invoke solver
    sol = solvers.qp(P,q,G,h)

    if 'optimal' not in sol['status']:
        return pre_sol
    print(sol['x'])
    print("Optimal objective cost is:")
    print(str(sol['primal objective']))
    # tmp = numpy.array(req.q_vec.data)
    # org_cost = sol['primal objective'] + 0.5*numpy.linalg.norm(tmp)**2
    # print("original cost is " + str(org_cost))
    res = Float64MultiArray()
    res.data = sol['x']
    pre_sol.data = sol['x']
    return res


def solveQPServer():
    rospy.init_node('solve_qp_server')
    s = rospy.Service('qp_query', qp, solveQP)
    # solvers.options['refinement'] = 2
    print ("Ready to solve QP.")
    rospy.spin()

if __name__ == "__main__":
    solveQPServer()