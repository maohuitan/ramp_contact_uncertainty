#!/usr/bin/env python

import rospy
from franka_control import SetLoad

if __name__ == '__main__':
    rospy.wait_for_service('/franka_control/set_load')
    try:
        set_load = rospy.ServiceProxy('franka_control/set_load', SetLoad)

        SetLoad sl
        sl.request.mass = 0.1
        sl.request.F_x_center_load[0] = 0.0
        sl.request.F_x_center_load[1] = 0.0
        sl.request.F_x_center_load[2] = 0.02
        sl.request.load_inertia[0] = 0.001
        sl.request.load_inertia[1] = 0.0
        sl.request.load_inertia[2] = 0.0
        sl.request.load_inertia[3] = 0.0
        sl.request.load_inertia[4] = 0.001
        sl.request.load_inertia[5] = 0.0
        sl.request.load_inertia[6] = 0.0
        sl.request.load_inertia[7] = 0.0
        sl.request.load_inertia[8] = 0.001

        set_load_srv_client.call(sl);
        res = set_load(sl)
        if(res.success)
            print "Set load success"
        else:
            print "Set load failed"
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
