
#ifndef GRAPH_H
#define GRAPH_H

#include <queue>
#include <unordered_map>
#include <boost/shared_ptr.hpp>
#include <moveit_msgs/RobotTrajectory.h>
#include "ramp_util/ramp_util.h"

/* 
	for priority queue used in Dijkstra's algorithm implementation 
	<weight, node_id>
*/
typedef std::pair<double, int> iPair;
enum motion_type{FREE, LINEAR, ARC, NA};

/*
	graph data structure
*/
class node
{
	public:
		node();
		/** 
		 * \brief node constructor
		 * \param node id, corresponding motion point id, and corresponding joint cfg
		 * \return void
		 */
		node(const int &index, const int &motion_pnt_id, const armCfg &jnt_cfg);
		friend bool operator < (const node &a, const node &b)
		{
			return a.id < b.id;
		}

		node& operator = (const node &n)
		{
			this->id = n.id;
			this->motion_point_id = n.motion_point_id;
			this->joint_cfg = n.joint_cfg;
		}
		
		/* node bookeeping id */
		int id;
		/* the id of the motion point it is associated with */
		int motion_point_id; 
		/* the joint cfg associated with this node */
		armCfg joint_cfg;
};

class edge
{
	public:
		/** 
		 * \brief edge constructor
		 * \param destination node, edge weight(joint path length), and corresponding joint path
		 * \return void
		 */
		edge(const node &des_node, const double &weight_cost, const moveit_msgs::RobotTrajectory &trajectory, const motion_type type);
		/* destination node */
		node target_node;
		/* edge weight(joint path length measured in joint space) */
		double weight;
		/* edge corresponding joint path */
		moveit_msgs::RobotTrajectory traj;
		/* edge trajecoty motion type */
		motion_type edge_type;
};

class graph
{
	public:
		graph();
		void addNode(const node &a);
		void addEdge(const node &a, const edge &e);
		void removeEdge(const node &a, const edge &e);
		/** 
		 * \brief get the trajectory associated with a certain edge
		 * \param src node id, des node id, and joint path container
		 * \return void
		 */
		void getEdgeTrajectory(const int a_id, const int b_id, moveit_msgs::RobotTrajectory &traj, bool only_key_cfgs);
		/** 
		 * \brief check if there exists another node having same joint cfg 
		 * \param joint cfg being checked, and exist_node
		 * \return true if such node exists, false otherwise
		 */
		bool hasNodeWithJntCfg(const armCfg &cfg, node &exist_node);
		/** 
		 * \brief find shortest path on the graph
		 * \param src node id, des node id, and joint path container
		 * \return void
		 */
		void shortestPath(const int &src_id, const int &des_id, std::vector<int> &path);
		void reset();
		
		void printPath(const std::vector<int> &parent, const int &des_node_id, std::vector<int> &path);
		void printPath(const std::vector<int> &parent, const int &des_node_id);
		void printSolution(const std::vector<double> &dist, const int &n, const std::vector<int> &parent);
		void printAdjList();
		void printNodesInfo();

	private:
		/* number of nodes on the graph */
		int num_of_nodes;
		/* adjacent list */
		std::unordered_map<int, std::vector<edge> > adj_list;
		/* nodes set */
		std::set<node> nodes;
};

#endif
