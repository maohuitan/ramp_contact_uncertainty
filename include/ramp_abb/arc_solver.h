
#ifndef ARC_SOLVER_H
#define ARC_SOLVER_H

#include <cmath>
#include "ramp_util/ramp_util.h"

class arcSolver
{
	public:
		arcSolver();

		Eigen::Vector3f arc(double t);
		bool solveArc(const Eigen::Vector3f &P1, const Eigen::Vector3f &P2, const Eigen::Vector3f &P3);

	private:
		Eigen::Vector3f Center, FDirP1, FDirP2;;
		double Radius;
		double Angle;
};

#endif
