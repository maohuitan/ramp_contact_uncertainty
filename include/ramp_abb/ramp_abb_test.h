#ifndef RAMP_ABB_TEST_H
#define RAMP_ABB_TEST_H

#include "ramp_abb/ramp_abb_motion_planning.h" 
#include "ramp_abb/graph.h"
#include "ramp_contact/sphere_tree.h"

/*
	class rampABBtest 
	tests of rampAbbMotionPlanning and graph
*/

class rampABBtest
{
	public:
		rampABBtest();
		void testCfgConversion();
		void testPointCloudLoading();
		void testGraph();
		void testArcMotion();
		void testContactMotion();
		void testSphereTree();
	
	private:
		rampABBmotionPlanning ramp_abb_mp;
		graph g;
};

#endif
