#ifndef RAMP_ABB_MOTION_PLANNING_H
#define RAMP_ABB_MOTION_PLANNING_H

#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <sensor_msgs/JointState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/GetStateValidity.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/ApplyPlanningScene.h>
/* tf */
#include <tf/transform_listener.h>

/* pcl */
#include <pcl/io/pcd_io.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>

/* ramp */
#include "ramp_setting/ramp_setting.h"
#include "ramp_util/ramp_util.h"
#include "ramp_abb/graph.h"
#include "ramp_abb/arc_solver.h"

/* services */
#include <ramp/addTool.h>
#include <ramp/removeTool.h>
#include <ramp/addAttachedObject.h>
#include <ramp/removeAttachedObject.h>
#include <ramp/addPointCloudToScene.h>
#include <ramp/generateMotionPlan.h>

/* mesh */
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <shape_msgs/Mesh.h>
#include <shape_msgs/MeshTriangle.h>

typedef std::vector<int> abbCfg;
enum via_type{STOP, FLY_BY};
class motionViaPoint
{
	public:
		motionViaPoint(const geometry_msgs::Pose &eef_pose, const via_type &point_via_type, const motion_type &point_motion_type, bool point_collison_check);
		geometry_msgs::Pose pose;
		via_type v_type;
		motion_type m_type;
		bool collision_check;
};

class rampABBmotionPlanning
{
	public:
		rampABBmotionPlanning(const std::string move_group_name, const std::string node_space);
		void initialize();
		
		/* 
		 * \brief set up environments
		 */
		void setFlagContinueOnUnreachable(bool cont_unreach);
		void setOutputKeyCfgsFlag(bool output_key_flag);
		void setViaPoints(const std::vector<motionViaPoint> &via_points_specs);
		void clearViaPoints();
		void addTool(const Eigen::Affine3d &tf, const std::string &path_to_mesh);
		void removeTool();
		void addAttachedObject(const Eigen::Affine3d &tf, const std::string &path_to_mesh);
		void removeAttachedObject();
		/* 
		 * \brief add object to planning scene
		 */
		void addSceneObject(const std::string& parent_link_name, const std::string& link_name, bool is_attached,
							const std::string &path_to_mesh, const Eigen::Vector3d &mesh_scale, 
							const std::string &object_id, const moveit_msgs::ObjectColor& color);
		/* 
		 * \brief remove object from planning scene
		 */
		void removeSceneObject(const std::string &object_id);
		/* 
		 * \brief add another robot arm to planning scnene
		 */
		void addSceneRobotArm(const std::string &object_id);
		/* 
		 * \brief evaluate different ik solutions for generating straight line motion
		 * return true if motion is generated and target eef pose is reached
		 */
		bool linearPathEvaluation(const geometry_msgs::Pose &start_eef_pose, const geometry_msgs::Pose &target_eef_pose,
								  std::vector<moveit_msgs::RobotTrajectory> &trajectories, bool include_j6_ik);
		bool arcPathEvaluation(const geometry_msgs::Pose &start_eef_pose,  const geometry_msgs::Pose &mid_eef_pose, const geometry_msgs::Pose &target_eef_pose,
							   std::vector<moveit_msgs::RobotTrajectory> &trajectories);
		/* 
		 * \brief generate cartesian constrained motion based on Jacobian pseudo inverse
		 * 		  error_code: 0 is success, 1 is singularity, 2 is joint limit, 3 is collision
		 * return true if motion is generated and target eef pose is reached
		 */
		bool jacobianBasedMotionGeneration(const armCfg &start_cfg, const tf::Vector3& eef_moving_dir_in_base_link, const int &num_of_steps, int &error_code, moveit_msgs::RobotTrajectory &trajectory, double acc_scale_factor = 0.05, double vel_scale_factor = 0.05);
		bool jacobianBasedMotionGeneration(const armCfg &start_cfg, const geometry_msgs::Pose &target_eef_pose, const int &num_of_steps , int &error_code, moveit_msgs::RobotTrajectory &trajectory); 
		bool moveAlongArc(const armCfg &start_cfg, const geometry_msgs::Pose &mid_pnt, const geometry_msgs::Pose &final_pnt, int &error_code, moveit_msgs::RobotTrajectory &trajectory);
		/* 
		 * \brief plan to a pose/joint goal
		 */
		bool moveFromCurrentJntCfgToGivenPose(const armCfg& start_cfg, const geometry_msgs::Pose &goal_pose, moveit_msgs::RobotTrajectory &trajectory, double acc_scale_factor = 0.05, double vel_scale_factor = 0.05);
		bool moveFromJntCfgToJntCfg(const armCfg &start_cfg, const armCfg &end_cfg, moveit_msgs::RobotTrajectory &trajectory);
		bool moveFromJntCfgToJntCfgByInterpolation(const armCfg &start_cfg, const armCfg &end_cfg, moveit_msgs::RobotTrajectory &trajectory,
													 double vel_scale_factor = 0.05, double acc_scale_factor = 0.05, int interpolation_steps = 1);
		bool moveFromJntCfgToJntCfgByJointMotion(const armCfg &start_cfg, const armCfg &end_cfg, moveit_msgs::RobotTrajectory &trajectory,
												 double vel_scale_factor = 0.05, double acc_scale_factor = 0.05);
		/* 
		 * \brief generate motion plan from default cfg to default cfg via graph search
		 *		  graph node: a point in joint space
		 *		  graph edge: a path connecting two points in joint space
		 *        Also filters out non-reachable via points (linear and free path segments)
		 */
		void generateMotionPlanViaGraphSearch(moveit_msgs::RobotTrajectory &trajectory, std::vector<int> &reachable_via_pnt_inds,
											  std::vector<moveit_msgs::RobotTrajectory> &trajectory_segments);
		/* 
		 * \brief get a random valid eef poes by sampling in joint space
		 */
		void getRandomValidEefPose(geometry_msgs::Pose &eef_pose);
		/* 
		 * \brief convert robot joint vector cfg to abb 4-digit cfg 
		 * 		  for 6-joint robot, the last digit is dummy now and left for external axis
		 */
		void convertRobotCfgToABBCfg(const armCfg &robot_cfg, abbCfg &abb_cfg);
		/* 
		 * \brief read in a point cloud from file
		 */
		void loadPointCloud(const Eigen::Affine3d &tf_from_camera_to_robot_base, const std::string &path);
		/* 
		 * \brief test to see if trajectory has at least one joint position
		 */
		bool isTrajectoryEmpty(const moveit_msgs::RobotTrajectory &trajectory);
		int findClosestJntCfg(const armCfg &src_cfg, const std::vector<armCfg> &cfgs);
		/* 
		 * \brief append trajectories to trajectory1
		 */
		void appendTrajectories(moveit_msgs::RobotTrajectory &trajectory1, const std::vector<moveit_msgs::RobotTrajectory> &trajectories);
		/* 
		 * \brief append trajectories to trajectory1
		 */
		void clearContactCfgContainer();
		std::vector<armCfg> getContactCfgs();
		void visualizeContactCfgs();

		/* 
		 * \brief visualization function in rviz
		 */
		void visualizeMotionViaPoints(const std::vector<motionViaPoint> &motion_via_points);
		void renderJointConfiguration(const armCfg &cfg);
		void renderJointTrajectoryMsg(const trajectory_msgs::JointTrajectory &trajectory);
		void renderJointTrajectoryMsg(const trajectory_msgs::JointTrajectory &trajectory,const std::vector<std::string> &jnt_names);
		/* 
		 * \brief only draw object in rviz
		 */
		void drawObjectInRviz(const std::string& parent_link_name, const std::vector<double>& pose_vec,
							 const std::string &path_to_mesh, const double mesh_scale,
							 const std::size_t object_id, const rviz_visual_tools::colors& color);

		void drawFrameInRviz(const std::string& parent_link_name, const std::vector<double>& pose_vec, const std::string& label);
		/* 
		 * \brief only draw another robot arm in rviz (not adding it to planning scene)
		 */
		void drawRobotArmInRviz();
		/* 
		 * \brief Trigger rviz visualization once
		 */
		void rvizVisualTrigger();

		/* 
		 * \brief ros service callback functions
		 */
		bool addToolSrv(ramp::addTool::Request &req, ramp::addTool::Response &res);
		bool removeToolSrv(ramp::removeTool::Request &req, ramp::removeTool::Response &res);
		bool addAttachedObjectSrv(ramp::addAttachedObject::Request &req, ramp::addAttachedObject::Response &res);
		bool removeAttachedObjectSrv(ramp::removeAttachedObject::Request &req, ramp::removeAttachedObject::Response &res);
		bool addPointCloudToSceneSrv(ramp::addPointCloudToScene::Request &req, ramp::addPointCloudToScene::Response &res);
		bool generateMotionPlanSrv(ramp::generateMotionPlan::Request &req, ramp::generateMotionPlan::Response &res);

		void getEefPose(const armCfg& cfg, Eigen::Affine3d& tf_from_eef_link_to_arm_base);

	private:
		/* 
		 * \brief ramp node handle
		 */
		ros::NodeHandle nh;

		const std::string move_group_name_;
		const std::string node_space_;
		/* 
		 * \brief kinematic setting of the robot running RAMP
		 */
		boost::shared_ptr<rampSetting> ramp_setting_ptr;
		/* 
		 * \brief ompl motion planning interface
		 */
		moveit::planning_interface::MoveGroupInterface move_group;
		const robot_state::JointModelGroup* joint_model_group;
		/* 
		 * \brief planning scene monitor 
		 */
		ros::Publisher planning_scene_diff_publisher;
		planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor;
		/* 
		 * \brief publish point cloud and convert to octomap
		 */
		ros::Publisher object_octo_publisher, display_pub, joint_cfg_pub;
		/* 
		 * \brief for visualizing things in rviz
		 */
		moveit_visual_tools::MoveItVisualToolsPtr visual_tools;
		ros::AsyncSpinner spinner;
		/* 
		 * \brief motion planning graph
		 */
		graph planning_graph;
		/* 
		 * \brief graph node id counter
		 */
		int node_id;
		/* 
		 * \brief solve for arc center and radius given 3 spatial points
		 */
		arcSolver arc_solver;
		/* 
		 * \brief flag for controlling whether planning should continue or not if
		 * 		  one not reachable viapoint is found
		 */
		bool continue_on_unreachble;
		/* 
		 * \brief specified via points
		 */
		std::vector<motionViaPoint> via_pnts_specified;
		/* 
		 * \brief tf from toolx to tool0
		 */
		Eigen::Affine3d tf_from_toolx_to_tool0;
		/* 
		 * \brief attached tool geometry
		 */
		moveit_msgs::AttachedCollisionObject attached_tool_geometry;
		/* 
		 * \brief flag for if tool is attached
		 */
		bool is_tool_attached;
		/* 
		 * \brief tf from toolx to tool0
		 */
		Eigen::Affine3d tf_from_attached_object_to_toolx;
		/* 
		 * \brief attached tool geometry
		 */
		moveit_msgs::AttachedCollisionObject attached_object_geometry;
		/* 
		 * \brief scene object geometry
		 */
		moveit_msgs::AttachedCollisionObject scene_object_geometry;
		/* 
		 * \brief flag for if object is attached to toolx
		 */
		bool is_object_attached;
		/* 
		 * \brief flag for controlling the generated trajectory via graph search
		 * 		  if true only start and final cfg will be included for linear and arc segments
		 */
		bool only_output_key_cfgs;
		/* 
		 * \brief in-contact or collision armCfg collector
		 */
		std::vector<armCfg> contact_cfgs;
		/* 
		 * \brief in-contact or collision armCfg collector
		 */
		tf::TransformListener tf_listener;
		/* 
		 * \brief load planning parameters from yaml file
		 */
		void loadParameters();
		/* 
		 * \brief convert between trajectory types
		 */
		void convertRobotTrajectoryToMsg(const robot_trajectory::RobotTrajectory &rob_traj, trajectory_msgs::JointTrajectory &traj_msg);
		/* 
		 * \brief tool and attached object add/remove
		 */
		void setTfFromToolXToTool0(const Eigen::Affine3d &tf);
		void addToolMesh(const std::string &path_to_mesh);
		void setTfFromAttachedObjectToToolx(const Eigen::Affine3d &tf);
		void addAttachedObjectMesh(const std::string &path_to_mesh);
		void setToolBoundingSphere(const float radius);
		void createSceneObject(const std::string& link_name, const std::string& path_to_mesh);
		/* 
		 * \brief percetion based ros services
		 */
		ros::ServiceServer add_tool_srv, remove_tool_srv, add_attached_object_srv, remove_attached_object_srv;
		ros::ServiceServer load_point_cloud_srv, generate_mp_srv;
};

#endif
