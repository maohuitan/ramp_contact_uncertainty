#ifndef RAMP_CONTACT_H
#define RAMP_CONTACT_H

#include <iomanip>

#include "ramp_abb/ramp_abb_motion_planning.h" 
#include "ramp_contact/sphere_tree.h"
#include "ramp_contact/haptic_rendering_qp.h"
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/WrenchStamped.h>

// action for FollowJointTrajectoryAction
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <franka_control/ErrorRecoveryActionGoal.h>
#include <franka_gripper/MoveAction.h>
#include <franka_gripper/HomingAction.h>
#include <franka_gripper/StopAction.h>
#include <franka_gripper/GraspAction.h>
#include <franka_msgs/FrankaState.h>
#include <actionlib_msgs/GoalID.h>

struct robotObject
{
	std::string parent_link, object_link;
	std::vector<double> pose_vec; // tf from obj link to parent link
	tf::Transform tf_from_object_link_to_parent_link; // converted from pose_vec 
	tf::Transform tf_from_parent_link_to_arm_eef_link; // fixed tf from parent link to arm eef link
	std::string mesh_path;
	double mesh_scale;
	std::size_t object_id;
	rviz_visual_tools::colors color;
	std::shared_ptr<sphereTree> sphere_tree;
};

struct proximityQueryResult
{
	bool is_colliding;
	double closest_dist;
	std::vector<std::pair<int, int>> colliding_pairs; // if is_colliding, this container will save colliding index pairs
	proximityQueryResult()
	{
		is_colliding = false;
		closest_dist = 1e6;
	}
};

struct trajectoryExecutionResult
{
	Eigen::VectorXd f_ext;
	std::string state;
	tf::Transform active_obj_tf_from_parent_to_base_in_contact;
	tf::Transform tf_from_k_frame_to_world_base_in_contact;
	moveit_msgs::RobotTrajectory traj;
	ros::Time contact_time;
	trajectoryExecutionResult() : f_ext(6) {};
};

struct maxWrench
{
	Eigen::VectorXd max_wrench_;
	ros::Time max_wrench_time_;
	maxWrench() : max_wrench_(6) {};
};

struct hapticRenderingResult
{
	std::vector<Eigen::VectorXd> f_vrt;
	Eigen::VectorXd f_vrt_max;
	Eigen::VectorXd f_vrt_avg;
	hapticRenderingResult() : f_vrt_max(6), f_vrt_avg(6) {};
};

struct wrenchAnalysisResult
{
	Eigen::Vector3d f_max, f_avg;
	Eigen::VectorXd ft_max, ft_avg;
	wrenchAnalysisResult() : ft_max(6), ft_avg(6) 
	{
		f_max.setZero();
		f_avg.setZero();
		ft_max.setZero();
		ft_avg.setZero();
	};
};

class rampContact
{
	public:
		rampContact();
		~rampContact();
		std::string moveActiveObjectToPose(const Eigen::Affine3d& tf_from_object_to_base, double acc_scale_factor = 0.05, double vel_scale_factor = 0.05);
		bool movePassiveObjectToPose(const Eigen::Affine3d& tf_from_object_to_base, bool move_hardware = false);

		/* 
			move active object along positive z direction to insert once 
		*/
		trajectoryExecutionResult insertOnce(const tf::Vector3& obj_moving_dir_in_obj_link, int steps = 75, double acc_scale_factor = 0.05, double vel_scale_factor = 0.05);
		/* 
			Estimate tf_from_passive_object_in_world_base
			Input: 
				measured force and torque
				eef_moving_dir
			Output:
				group of tf_from_passive_object_in_world_base, sorted by likelihood
		*/
		void setTfHatHoleInWorldBase(const Eigen::Affine3d& tf_hat);
		void contactReasonOnceViaHapticRendering(const trajectoryExecutionResult& ter);
		void recoverFromErrorOnce();

		void drawActiveObjectSphereTrees();
		void drawPassiveObjectSphereTrees();

		proximityQueryResult spheresProximityQuery(const tf::Transform& active_obj_tf_from_parent_link_to_world_base,
                                                   const tf::Transform& passive_obj_tf_from_obj_link_to_world_base);

		Eigen::Affine3d getCurBestEstPasObjInWorldBase();

		void closeGripper();
		void openGripper();
		void stopGripperAction();
		void homingGripper();

		/* 
			Contact forces residual learning
		 */
		void collectFmeaFsimData(); // harware and simulation both
		void collectFsimData(); // only needs simulation

		/* 
			Direct insertion
		 */
		void directInsertion(const Eigen::Vector3d& gt_uncert_rpy, const Eigen::Vector3d& pred_uncert_rpy, const Eigen::Vector3d& pos_offset);
		/* 
			Compliant insertion via contact state transitions
		 */
		void compliantInsertionViaContactStateTransitions(const Eigen::Vector3d& gt_uncert_rpy, const Eigen::Vector3d& pred_uncert_rpy,
														 const Eigen::Vector3d& pos_offset,  int compliant_option = 2);

	private:
		tf::TransformListener tf_listener;
		ros::Publisher passive_robot_joint_cfg_pub;
		ros::ServiceClient ik_srv_client;
        moveit_visual_tools::MoveItVisualToolsPtr visual_tools;

		rampABBmotionPlanning arm1;
		robotObject passive_obj, active_obj;
		bool on_hardware_;
		bool load_gripper_driver_;
		
		/* 
			robot and contact states from sensors
		 */
		std::vector<geometry_msgs::WrenchStamped> wrenches_;
		maxWrench max_wrench_;
		ros::Subscriber wrench_state_sub, franka_state_sub;
		std::queue<sensor_msgs::JointState> joint_states_;
		std::vector<franka_msgs::FrankaState> franka_states_;
		ros::Subscriber joint_state_sub;
		ros::Time contact_time_;

		void resetMaxWrench();
		void wrenchStateCallback(const geometry_msgs::WrenchStamped::ConstPtr &msg);
		void jointStateCallback(const sensor_msgs::JointState::ConstPtr &msg);
		void frankaStateCallback(const franka_msgs::FrankaState::ConstPtr &msg);
		Eigen::VectorXd extractWrenchDirections() const;

		/* 
			control panda arm hardware
			FollowJointTrajectoryAction provided by ros-industrial.
		 */
		ros::ServiceClient set_load_srv_client, set_ft_col_srv_client;
		actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> fol_jnt_traj_action_client;
		ros::Publisher franka_error_recov_pub, cancel_traj_follow_pub;
		
		void setLoad();
		void setForceTorqueCollisionThresholds();
		std::string executeTrajectoryOnHardware(const moveit_msgs::RobotTrajectory& trajectory);
		void cancelCurrentTrajectoryFollowGoal();
		armCfg getRobotCurrentCfg();

		/* 
			control panda gripper
		 */
		actionlib::SimpleActionClient<franka_gripper::MoveAction> gripper_move_action_client_;
		actionlib::SimpleActionClient<franka_gripper::HomingAction> gripper_homing_action_client_;
		actionlib::SimpleActionClient<franka_gripper::StopAction> gripper_stop_action_client_;
		actionlib::SimpleActionClient<franka_gripper::GraspAction> gripper_grasp_action_client_;

		/* 
			control ABB arm
		 */
		actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> abb_fol_jnt_traj_action_client_;
		bool moveABBHardwareToCfg(const armCfg& cfg);

		/* 
			contact reasoing
		 */
		Eigen::Affine3d tf_hat_from_hole_to_world_base_;

		struct candidatePose
		{
			Eigen::Affine3d tf_mat_;
			Eigen::VectorXd relative_tf_vec_;
			Eigen::AngleAxisd axis_ang_;
			candidatePose() : relative_tf_vec_(6) {};
			candidatePose(const Eigen::Affine3d& tf, const Eigen::VectorXd& rel_tf_vec, const Eigen::AngleAxisd& axis_ang)
			{
				tf_mat_ = tf;
				relative_tf_vec_ = rel_tf_vec;
				axis_ang_ = axis_ang;
			};
		};

		std::vector<std::pair<candidatePose, double>> tf_hat_from_hole_to_world_base_candidates_;
		std::vector<candidatePose> tf_hat_from_hole_to_pas_arm_base_candidates_;
		hapticRenderingQP haptic_qp_;

		void generatePassiveObjectCandidatePoses(); // generate candidate tf_hat_from_hole_to_world_base_ based on current estimation
		void viewPassiveObjectCandidatePoses();
		hapticRenderingResult generateVirtualHapticResponse(const moveit_msgs::RobotTrajectory& rob_traj,
															const tf::Transform& cur_tf_hat_from_passive_obj_to_world_base);
		double compareForceTorque(Eigen::VectorXd f_vrt, Eigen::VectorXd f_mea);
		void transformSpheres(const std::vector<sphere>& src_spheres, std::vector<sphere>& tgt_spheres, const tf::Transform& tf);
		void enlargeSpheres(std::vector<sphere>& spheres, double radius_delta);
		void saveFTtoFile(const std::vector<Eigen::VectorXd>& ft);
		void visualizeSpheresInWorldBase(const std::vector<sphere> sps, const rviz_visual_tools::colors& color, const std::size_t& name_id);

		wrenchAnalysisResult analyzeWrenchData(const std::vector<Eigen::VectorXd>& wrenches);

		int step_limit_of_simulated_fvrt_;
		int use_level_x_sphere_tree_;
		double rotation_res_;
		double act_sph_rad_scale_;
		double pas_sph_rad_scale_;
		std::pair<double, double> x_rot_range_, y_rot_range_, z_rot_range_;
		bool use_force_reasoing_only_;
		bool use_ft_avg_reasoning_;
		double exe_timeout_;
		double sp_rad_delta_;
		bool stop_contact_reason_at_each_pose_;

};

#endif
