#ifndef HAPTIC_RENDERING_QP_H
#define HAPTIC_RENDERING_QP_H

#include "ramp_contact/sphere_tree.h"
#include "ramp/qp.h"
#include <Eigen/Dense>
#include <vector>

class hapticRenderingQP
{
    public:
        hapticRenderingQP();
        ~hapticRenderingQP();
        void setGraphicCfgPrevious(const std::vector<double>& graphic_cfg_pre);
        void setHapticCfg(const std::vector<double>& haptic_cfg);
        void setCollidingSpherePairs(const std::vector<std::pair<sphere, sphere>>& colliding_pairs);
        void resetQPMatrices();
        std::vector<double> solveQP();
    private:
        // previous graphic tool configuration
        double pre_x_g, pre_y_g, pre_z_g, pre_gamma_g, pre_beta_g, pre_alpha_g;
        std::vector<double> haptic_cfg_;

        std::vector<Eigen::VectorXd> G_;
        std::vector<double> h_;
        Eigen::VectorXd P_; // diagonal matrix 6X6 with diagonal ones
        Eigen::VectorXd q_;

        void makeOneConstraint(double x_l, double y_l, double z_l, double x_o, double y_o, double z_o, double act_sp_rad, double pas_sp_rad);

        double computeX_T(double x_l, double y_l, double z_l);
        double computeY_T(double x_l, double y_l, double z_l);
        double computeZ_T(double x_l, double y_l, double z_l);

        double compute_phi_x_T_over_phi_qg4(double x_l, double y_l, double z_l);
        double compute_phi_y_T_over_phi_qg4(double x_l, double y_l, double z_l);
        double compute_phi_z_T_over_phi_qg4(double x_l, double y_l, double z_l);
        
        double compute_phi_x_T_over_phi_qg5(double x_l, double y_l, double z_l);
        double compute_phi_y_T_over_phi_qg5(double x_l, double y_l, double z_l);
        double compute_phi_z_T_over_phi_qg5(double x_l, double y_l, double z_l);

        double compute_phi_x_T_over_phi_qg6(double x_l, double y_l, double z_l);
        double compute_phi_y_T_over_phi_qg6(double x_l, double y_l, double z_l);
        double compute_phi_z_T_over_phi_qg6(double x_l, double y_l, double z_l);

        void generateMotionHistoryConstraints();

		ros::ServiceClient qp_srv_client_;
};

#endif