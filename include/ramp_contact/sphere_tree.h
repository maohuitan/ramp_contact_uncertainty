#ifndef SPHERE_TREE_H
#define SPHERE_TREE_H

#include <ros/ros.h>
#include <Eigen/Dense>
#include <fstream>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include "ramp_util/ramp_util.h"

class sphere
{
    public:
        sphere(const Eigen::Vector3d& cen, const double rad);
        Eigen::Vector3d center;
        double radius;
};

class sphereTree
{
	public:
		sphereTree(const std::string& path_to_sph, const std::vector<double>& pose_vec_from_spt_link_to_mesh_link);
                void visualizeSpheresAtLevelX(const int level, const tf::Transform& tf_from_object_link_to_world_base_link,
                                              const std::size_t& id, const moveit_visual_tools::MoveItVisualToolsPtr& visual_tools);
		~sphereTree();
                void getSpheresAtLevelXInWorldBaseFrame(const int level, const tf::Transform& tf_from_object_link_to_world_base_link, std::vector<sphere>& sphere_container);
                void getSpheresAtLevelXInSphereTreeFrame(const int level, std::vector<sphere>& sphere_container, double radius_scale = 1.0);
	private:
                std::vector<sphere> spheres;
                /* 
                        [tf from sphere tree link to mesh link]
                        we need this transform because there is a new frame 
                        build specifically for representing sphere trees when
                        we use Bradshaw's construction toolkit
                 */
                tf::Transform tf_from_spt_link_to_mesh_link;
                int depth, branching_factor;
};

#endif
