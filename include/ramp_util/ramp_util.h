
#ifndef RAMP_UTIL_H
#define RAMP_UTIL_H

#include <ros/ros.h>
#include "ramp_util/ramp_common.h"
#include <Eigen/Dense>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <fstream>
#include <tf/transform_listener.h>

namespace rampUtil
{
	void convertXYZRPYtoAffine3d(const std::vector<double> &pose_vec, Eigen::Affine3d &pose_tf);
	void convertAffine3dtoQuaternion(const Eigen::Affine3d &pose_tf, geometry_msgs::Pose &pose_quat);
	void convertAffine3dtoTfTransform(const Eigen::Affine3d &pose_tf, tf::Transform& transform);
	void convertAffine3dtoXYZRPY(const Eigen::Affine3d &pose_tf, std::vector<double> &pose_vec);
	void convertXYZRPYtoQuaternion(const std::vector<double> &pose_vec, geometry_msgs::Pose &pose_quat);
	void convertXYZRPYtoTfTransform(const std::vector<double> &pose_vec, tf::Transform& transform);
	void convertQuaterniontoXYZRPY(const geometry_msgs::Pose &pose_quat, std::vector<double> &pose_vec);
	void convertQuaterniontoAffine3d(const geometry_msgs::Pose &pose_quat, Eigen::Affine3d &pose_tf);
	void convertTfTransformtoMsg(const tf::Transform& transform, geometry_msgs::Pose &pose_msg);
	void convertTfTransformtoAffine3d(const tf::Transform& transform, Eigen::Affine3d &pose_tf);
	void convertTfTransformtoXYZRPY(const tf::Transform& transform, std::vector<double> &pose_vec);
	double convertDegToRad(double deg);
	double convertMtoMM(double len_in_meter);

	void printGeoMsgPose(const geometry_msgs::Pose &p);
	void printArmCfg(const armCfg &jv);
	void printArmCfgs(const std::vector<armCfg> &jvs);
	void printGeoMsgTwist(const geometry_msgs::Twist &t);
	void printTfTransform(const tf::Transform& tf);
	void printTrajectory(const moveit_msgs::RobotTrajectory &traj);

	bool areSamePoses(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2);
	bool areSamePositions(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2);
	bool areSameJntCfgs(const armCfg &c1, const armCfg &c2);
	double getJntCfgDistance(const armCfg &c1, const armCfg &c2);
	double getJointPathLength(const moveit_msgs::RobotTrajectory &traj);

	double get3dDistance(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2);
	/* 
	* \brief generate a random float between a and b
	*/
	float randomFloat(float a, float b);
	void saveCfgsToFile(std::vector<armCfg> &cfgs);
}

#endif
