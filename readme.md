## Pose uncertainty reduction via force forecasting

## dependencies
* Install ROS 
http://www.ros.org/
* Install Openmesh: 
https://www.openmesh.org/download/ 
* Install MoveIt
https://moveit.ros.org/
* Franka Panda documentation
https://frankaemika.github.io/docs/

## run
1. $ roslaunch ramp ramp_contact_hardware.launch
2. $ rosrun ramp ramp_contact_node

